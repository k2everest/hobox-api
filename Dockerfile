FROM node:8.2

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

#RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

# install node packages
RUN npm install && \
    npm install -g nodemon && \
    npm install amazon-mws -g && \
    npm install mercadolibre-new 

COPY . ./

EXPOSE 3000

CMD ["npm","start"]
