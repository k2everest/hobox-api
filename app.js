var compression = require("compression");
var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
const morgan = require("morgan");
var fs = require("fs");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
const passport = require("passport");
const swaggerSpec = require("./service/swagger");
const mercadolivreCredentials = require("./service/mercadolivre").credentials;
var events = require("events");

process.on("unhandledRejection", (reason, promise) => {
  console.warn(
    "Unhandled promise rejection:",
    promise,
    "reason:",
    reason.stack || reason
  );
});

//routes
const auth = require("./api/auth");
const product_mercadolivre = require("./api/product_mercadolivre");
const amazon = require("./api/amazon");
const products = require("./api/product");
const payments = require("./api/payment");

const platforms = require("./api/platform");
const orders = require("./api/order");
const categories = require("./api/category");
const users = require("./api/user");
const index = require("./api/");
const attributes = require("./api/attribute");
const sync = require("./api/sync");
const order_mercadolivre = require("./api/order_mecadolivre");
const platform_mercadolivre = require("./api/platform_mercadolivre");
const attribute_mercadolivre = require("./api/attribute_mercadolivre");
const category_mercadolivre = require("./api/category_mercadolivre");
const bling = require("./api/bling");

// Database
var db = require("./service/db");

var helmet = require("helmet");

var app = express();
app.set("view engine", "jade");

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//create log for all requests
app.use(
  morgan("combined", {
    stream: fs.createWriteStream("./logs/access.log", { flags: "a" }),
  })
);
app.use(compression());
app.use(helmet());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());

// DB torna-se acessível
app.use(function (req, res, next) {
  req.db = db;
  req.mercadolivreCredentials = mercadolivreCredentials;
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST, GET, DELETE, PUT, PATCH"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "x-access-token, X-Requested-With,content-type, authorization"
  );
  next();
});

//define routes
app.use("/", index);
app.use("/login/", auth);
app.use("/auth/", auth);
app.use("/users/", users.userOpenRouter);

app.get("/swagger.json", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

//check user is authenticated
require("./middleware/authenticated")(app);

app.use("/users/", users);
app.use("/payments/", payments);

//check user is deactivated
require("./middleware/checkIsDeactivated")(app);

app.use("/amazon/", amazon);
app.use("/bling/", bling);
app.use("/products/", product_mercadolivre);
app.use("/products/", products);
app.use("/plataforms/", platforms);
app.use("/plataforms/", platform_mercadolivre);
app.use("/orders/", order_mercadolivre);
app.use("/orders/", orders);
app.use("/categories/mercadolivre/", category_mercadolivre);
app.use("/categories/", categories);
app.use("/mercadolivre/attributes", attribute_mercadolivre);
app.use("/attributes/", attributes);
app.use("/sync-logs/", sync);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
