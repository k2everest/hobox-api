/* Copyright 2015 EBANX */
"use strict";

var Config = function (integrationKey, testMode) {
  this.integrationKey = integrationKey;
  this.testMode = true;
  this.usingHttp = true;
};

Config.prototype = {
  getTestMode: function () {
    return this.testMode;
  },
  getIntegrationKey: function () {
    return this.integrationKey;
  },

  getEndPoint: function () {
    if (this.getTestMode()) {
      return "https://staging.ebanx.com.br/";
    } else {
      return "https://api.ebanx.com.br/";
    }
  },
};

module.exports = new Config();
