const events = require("../../service/event");

//update status connection magento
module.exports = function (ownerId, statusConnCurrent) {
  events.emit("updateConnMagento", {
    owner: ownerId,
    statusConn: statusConnCurrent,
  });
};
