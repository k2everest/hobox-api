const updateConnMl = require("../controller/platform.update.connMl");
const events = require("../../../service/event");

module.exports.run = function () {
  console.log("entrou no run check status de conexão");
  events.on("updateConnMl", action);
};

function action(params) {
  console.log("o evento updateConnMl disparou");
  updateConnMl(params.owner, params.statusConn);
}
