module.exports = [
  {
    hobox: "pending_payment",
    status: "payment_required",
  },
  {
    hobox: "payment_review",
    status: "payment_in_process",
  },
  {
    hobox: "pending_payment",
    status: "confirmed",
  },
  {
    hobox: "processing",
    status: "paid",
  },
  {
    hobox: "canceled",
    status: "cancelled",
  },
  {
    hobox: "complete",
    status: "shipped",
  },
  {
    hobox: "complete",
    status: "delivered",
  },
  {
    hobox: "complete",
    status: "closed",
  },
];
