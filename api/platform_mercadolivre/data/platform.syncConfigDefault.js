module.exports = {
  order: {
    send: {
      value: false,
      changeable: false,
    },
    receive: {
      value: true,
      changeable: true,
    },
  },
  product: {
    send: {
      value: false,
      changeable: true,
    },
    receive: {
      value: false,
      changeable: false,
    },
  },
  stock: {
    send: {
      value: false,
      changeable: true,
    },
    receive: {
      value: false,
      changeable: false,
    },
  },
};
