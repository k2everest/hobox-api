module.exports.mappedAttributesDefault = require("./platform.mappedAttributesDefault");
module.exports.mercadolivreStatus = require("./platform.status");
module.exports.syncConfigDefault = require("./platform.syncConfigDefault");
module.exports.rules = require("./platform.rulesDefault");
