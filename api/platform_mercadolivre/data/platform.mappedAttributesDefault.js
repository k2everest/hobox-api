module.exports = [
  {
    marketplaceName: "title",
    hoboxName: "name",
    options: null,
  },
  {
    marketplaceName: "seller_custom_field",
    hoboxName: "sku",
    options: null,
  },
  {
    marketplaceName: "description",
    hoboxName: "description",
    options: null,
  },
  {
    marketplaceName: "available_quantity",
    hoboxName: "qty",
    options: null,
  },
  {
    marketplaceName: "price",
    hoboxName: "price",
    options: null,
  },
  {
    marketplaceName: "promotional_price",
    hoboxName: "special_price",
    options: null,
  },
  {
    marketplaceName: "weight",
    hoboxName: "weight",
    options: null,
  },
  {
    marketplaceName: "status",
    hoboxName: "status",
    options: [
      {
        hobox: "1",
        ml: "active",
      },
      {
        hobox: "0",
        ml: "paused",
      },
    ],
  },
  /* {
          "marketplaceName" : "height",
          "hoboxName" : "volume_altura",
          "options" : null
      },
      {
          "marketplaceName" : "width",
          "hoboxName" : "volume_largura",
          "options" : null
      },
      {
          "marketplaceName" : "length",
          "hoboxName" : "volume_comprimento",
          "options" : null
      },
      {
          "marketplaceName" : "shipping_mode",
          "hoboxName" : "shipment_type",
          "options" : null
      },
      {
          "marketplaceName" : "express_free_shipping",
          "hoboxName" : "frete_gratis",
          "options" : null
      },
      {
          "marketplaceName" : "normal_free_shipping",
          "hoboxName" : "frete_gratis",
          "options" : null
      },
      {
          "marketplaceName" : "custom_free_shipping",
          "hoboxName" : "frete_gratis",
          "options" : null
      },
      {
          "marketplaceName" : "local_pick_up",
          "hoboxName" : "shipment_type",
          "options" : null
      },
      {
          "marketplaceName" : "listing_type_id",
          "hoboxName" : "tipo_anuncioml",
          "options" : [
              {
                  "hobox" : "160",
                  "ml" : "Premium"
              },
              {
                  "hobox" : "157",
                  "ml" : "Diamante"
              },
              {
                  "hobox" : "171",
                  "ml" : "Clássico"
              },
              {
                  "hobox" : "159",
                  "ml" : "Prata"
              }
          ]
      } */
];
