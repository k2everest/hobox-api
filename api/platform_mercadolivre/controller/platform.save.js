const data = require("../data");
const mercadolivre = require("../../../service/mercadolivre");
const events = require("../../../service/event");

module.exports = async function (req, res) {
  let db = req.db;
  const ownerId = req.decoded._id;
  const idClient = JSON.parse(req.body.plataform._id);
  const syncOrdersFrom = req.body.plataform.settings.syncOrdersFrom;

  try {
    let platform = await db.collection("plataform").findOneAsync({
      _id: idClient,
      $or: [{ owner: ownerId }, { owner: { $exists: false } }],
    });
    platform.owner = ownerId;
    platform.settings.syncOrdersFrom = syncOrdersFrom
      ? syncOrdersFrom
      : new Date();
    await setPlatform(platform);
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }

  async function setPlatform(platform) {
    try {
      let result = await db.collection("plataform").updateAsync(
        { _id: idClient },
        {
          $set: {
            owner: platform.owner,
            "settings.syncOrdersFrom": platform.settings.syncOrdersFrom,
          },
        }
      );
      if (result.result.upserted || !platform.syncConfig)
        setDefaultConfig(idClient);
      (await testConnection(platform)) == true
        ? (events.emit("updateConnMl", { owner: ownerId, statusConn: true }),
          res.json({ message: "updated and conected" }))
        : (events.emit("updateConnMl", { owner: ownerId, statusConn: false }),
          res.json({ message: "updated but not connected" }));
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  }

  async function setDefaultConfig(idPlatform) {
    const mappingOrderStatus = data.mercadolivreStatus;
    const mappedAttributesDefault = data.mappedAttributesDefault;
    try {
      await db.collection("plataform").updateAsync(
        { _id: idPlatform },
        {
          $set: {
            mappedOrderStatus: mappingOrderStatus,
            mappedAttributes: mappedAttributesDefault,
            syncConfig: data.syncConfigDefault,
            rules: data.rules,
          },
        },
        { upsert: false }
      );

      return { message: "updated" };
    } catch (err) {
      return err;
    }
  }
};

const testConnection = async function (platform) {
  let meli = await mercadolivre.meli(
    platform.settings.accessToken,
    platform.settings.refreshToken
  );
  let userId = platform.settings.seller;
  let result = await meli.getAsync(`/users/${userId}`);
  return result.message ? false : true;
};
