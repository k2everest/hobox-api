const Model = require("../model");

module.exports = async (req, res) => {
  const platformId =
    req.params.platformId.length === 24
      ? ObjectId(req.params.platformId)
      : JSON.parse(req.params.platformId);
  const ownerId = req.decoded._id;
  const settings = req.body.settings;
  console.log("settings", settings);
  let result = null;

  switch (settings.rule) {
    case "offsetFee":
      result = await offsetFee();
      break;
    default:
      result = def();
  }

  return result.error
    ? res.status(400).send(result.message)
    : res.json(result.message);

  async function offsetFee() {
    let currentSettings = { products: settings.products, isActived: true };
    let result = await Model.offsetFee({
      ownerId: ownerId,
      idPlatform: platformId,
      settings: currentSettings,
    });
    return result;
  }

  async function def() {
    return res.status(400).send("Rule not defined");
  }
};
