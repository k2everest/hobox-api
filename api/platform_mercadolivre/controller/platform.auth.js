module.exports = function (req, res) {
  res.json(
    "http://auth.mercadolivre.com.br/authorization?response_type=code&client_id=" +
      process.env.MERCADOLIVRE_CLIENT_ID
  );
};
