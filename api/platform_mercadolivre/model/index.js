const ObjectId = require("mongodb").ObjectId;
const db = require("../../../service/db");
const schemas = require("./schemas");

function validate(obj, schema) {
  const result = schema.validate(obj);
  return result.error;
}

exports.offsetFee = async ({ ownerId, idPlatform, settings }) => {
  let validElements = validate(settings, schemas.ruleSettings);
  if (validElements != null) {
    return {
      error: true,
      message: validElements,
    };
  }
  try {
    await db
      .collection("plataform")
      .updateAsync(
        { _id: idPlatform, owner: ownerId, plataform: "mercadolivre" },
        { $set: { rules: { offsetFee: settings } } },
        { upsert: false }
      );
    return { message: "updated" };
  } catch (err) {
    return { error: true, message: err.message };
  }
};
