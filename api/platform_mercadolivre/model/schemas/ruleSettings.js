const Joi = require("joi");

module.exports = Joi.object().keys({
  isActived: Joi.boolean(),
  products: Joi.array(),
});
