var express = require("express");
var router = express.Router();
const auth = require("./controller/platform.auth");
const save = require("./controller/platform.save");
const updateRules = require("./controller/platform.save.rules");

//create and update mercado livre plataform
router.post("/mercadolivre", save);

//auth mercado livre
router.get("/mercadolivre/auth", auth);

router.put("/:platformId/mercadolivre/rules", updateRules);

module.exports = router;
