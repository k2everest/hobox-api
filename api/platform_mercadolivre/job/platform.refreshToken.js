const meli = require("mercadolibre-new");
const cron = require("cron");
const logger = require("../../../service/winston");
const events = require("../../../service/event");

//refresh token mercado livre
module.exports = function (db) {
  return new cron.CronJob({
    cronTime: "0 */2 * * *",
    start: true,
    onTick: function () {
      db.collection("plataform")
        .find({ plataform: "mercadolivre" })
        .toArray(function (err, plataforms) {
          if (err) {
            return logger.error({
              message: err.message,
              meta: {
                resource: "access_token",
                origin: "all",
                destiny: "mercadolivre",
                status: "error on get platforms",
                action: "update token",
              },
            });
          } else {
            plataforms.map(function (plataform) {
              var meliObject = new meli.Meli(
                process.env.MERCADOLIVRE_CLIENT_ID,
                process.env.MERCADOLIVRE_CLIENT_SECRET,
                plataform.settings.accessToken,
                plataform.settings.refreshToken
              );
              meliObject.refreshAccessToken(function (err, result) {
                if (!err && result.refresh_token) {
                  events.emit("updateConnMl", {
                    owner: plataform.owner,
                    statusConn: true,
                  });
                  updateAccessToken(
                    db,
                    plataform._id,
                    result.refresh_token,
                    result.access_token
                  );
                } else {
                  events.emit("updateConnMl", {
                    owner: plataform.owner,
                    statusConn: false,
                  });
                  if (!err && result.error)
                    logger.error({
                      message: result.message,
                      meta: {
                        resource: "access_token",
                        origin: plataform.settings.nickname,
                        destiny: "mercadolivre",
                        status: "error",
                        owner: plataform.owner,
                        action: "update token",
                      },
                    });
                  else
                    logger.error({
                      message: err,
                      meta: {
                        resource: "access_token",
                        origin: plataform.settings.nickname,
                        destiny: "mercadolivre",
                        status: "error",
                        owner: plataform.owner,
                        action: "update token",
                      },
                    });
                }
              });
            });
          }
        });
    },
  });

  function updateAccessToken(db, plataformId, refreshToken, accessToken) {
    db.collection("plataform").update(
      { _id: plataformId },
      {
        $set: {
          "settings.refreshToken": refreshToken,
          "settings.accessToken": accessToken,
        },
      },
      function (err, result) {
        if (err)
          return logger.error({
            message: result.message,
            meta: {
              resource: "access_token",
              origin: plataformId,
              destiny: "mercadolivre",
              status: "error",
              action: "update token",
            },
          });
        else {
          return logger.info({
            message: "updated",
            meta: {
              resource: "access_token",
              origin: plataformId,
              action: "update token",
              destiny: "mercadolivre",
            },
          });
        }
      }
    );
  }
};
