const Synclog = require("../model");

//get list of sync-logs or specific by id
module.exports = async function (req, res) {
  const ownerId = req.decoded._id;
  const limit = req.query.limit ? req.query.limit : 100;
  const sortBy = req.query.sortBy ? req.query.sortBy : "dateTime";
  const direction = req.query.direction
    ? parseInt(req.query.direction == "asc" ? 1 : -1)
    : -1;
  const currentPage = req.query.page
    ? parseInt(req.query.page) * limit - limit
    : 0;

  const date = new Date();
  date.setDate(date.getDate() - 15);
  const dateString = date.toISOString();

  const queryLimitDays = { dateTime: { $gte: new Date(dateString) } };
  let filter = [];
  filter.push(queryLimitDays);
  let currentSort = {};
  currentSort[sortBy] = direction;

  const handleQuery = () => {
    req.query.id
      ? filter.push({ id: { $regex: `.*${req.query.id}.*`, $options: "i" } })
      : null;
    req.query.resource && req.query.resource[0] != ""
      ? filter.push({ resource: { $in: req.query.resource } })
      : null;
    req.query.status && req.query.status[0] != ""
      ? filter.push({ status: { $in: req.query.status } })
      : null;
    req.query.origin && req.query.origin[0] != ""
      ? filter.push({ origin: { $in: req.query.origin } })
      : null;
    req.query.destiny && req.query.destiny[0] != ""
      ? filter.push({ destiny: { $in: req.query.destiny } })
      : null;

    if (filter.length < 1) filter.push({});
  };

  handleQuery();
  let query = { owner: ownerId, $and: filter };
  let result = await Synclog.getAll(query, currentSort, limit, currentPage);

  return result.error ? res.status(400).send(result) : res.json(result);
};
