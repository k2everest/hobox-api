const db = require("../../../service/db");
const synclog = db.collection("synclog");

exports.getAll = async (query, sortBy, limit, page) => {
  let result = [];
  try {
    let count = await synclog.find(query).countAsync({ applySkipLimit: true });
    result = await synclog
      .find(query)
      .sort(sortBy)
      .limit(limit)
      .skip(page)
      .toArrayAsync();
    const pagesTotal = Math.ceil(count / limit);
    let response = {
      logs: result,
      pages: pagesTotal,
      page: page,
      total: count,
    };

    return response;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};
