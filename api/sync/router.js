const express = require("express");
const router = express.Router();
const get = require("./controller/sync.get");

//get sync logs
router.get("/", get);

module.exports = router;
