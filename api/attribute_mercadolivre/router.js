var express = require("express");
var router = express.Router();
const attributesHandlers = require("./controller");

//list attributes from Mercado livre
router.get("/", attributesHandlers.get);

module.exports = router;
