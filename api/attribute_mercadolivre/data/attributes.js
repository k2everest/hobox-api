module.exports =
  /*  meliObject.get('/sites/MLB/listing_types',
      function(err,listingTypes){
          if(err)
              console.log(err);
          else{
              listingTypes =  listingTypes.map(a=> { return {label: a.id, value: a.name} });
              attributesML.push(
                {
                  name: "listing_type_id",
                  label: "Tipo de anuncio",
                  options: listingTypes
                }
              );
              return res.json(attributesML);
          }
      }
  ); */

  //const ownerId = req.decoded._id;
  [
    {
      name: "title",
      label: "Título",
    },
    {
      name: "seller_custom_field",
      label: "SKU",
    },
    {
      name: "description",
      label: "Descrição",
    },
    {
      name: "condition",
      label: "Estado do Produto",
      options: [
        {
          label: "Novo",
          value: "new",
        },
        {
          label: "Usado",
          value: "used",
        },
        {
          label: "Não especificado",
          value: "not_specified",
        },
      ],
    },
    {
      name: "available_quantity",
      label: "Quantidade",
    },
    {
      name: "price",
      label: "Preço",
    },
    {
      name: "promotional_price",
      label: "Preço promocional",
    },
    {
      name: "status",
      label: "Status",
      options: [
        {
          label: "Pausado",
          value: "paused",
        },
        {
          label: "Ativo",
          value: "active",
        },
      ],
    },
    {
      name: "video_id",
      label: "ID Vídeo Youtube",
    },

    /*     {
      name: "buying_mode",
      label: "Modo de pagamento"
    }, */

    {
      name: "sale_terms.WARRANTY_TYPE",
      label: "Tipo de garantia",
      options: [
        {
          value: "2230280",
          label: "Garantia do vendedor"
        },
        {
          value: "2230279",
          label: "Garantia de fábrica"
        }
      ],
    },
    {
      name: "sale_terms.WARRANTY_TIME",
      label: "Tempo de Garantia"
    },
    {
      name: "weight",
      label: "Peso",
    },
    {
      name: "dimensions.height",
      label: "Altura",
    },
    {
      name: "dimensions.width",
      label: "Largura",
    },
    {
      name: "dimensions.length",
      label: "Comprimento",
    },
    /* {
      name: "shipping_mode",
      label: "Modo de Entrega",
      options: [
        {
          label: 'Não especificado',
          value: 'not_specified'
        },
        {
          label: 'A combinar',
          value: 'custom'
        },
        {
          label: 'ME1',
          value: 'me1'
        },
        {
          label: 'Me2',
          value: 'me2'
        }
      ]
    }, */
    /*    {
      name: "express_free_shipping",
      label: "Frete grátis (expresso)"
    },
    {
      name: "normal_free_shipping",
      label: "Frete grátis (normal)"
    },
    {
      name: "custom_free_shipping",
      label: "Frete grátis (a combinar)"
    }, */
    {
      name: "shipping.local_pick_up",
      label: "Retirada na Loja",
      options: [
        {
          label: "Sim",
          value: true,
        },
        {
          label: "Não",
          value: false,
        },
      ],
    },
    /* {
      name: "listing_type_id",
      label: "Tipo de anuncio",
      options: [
        {
          label: 'Premium',
          value: 'gold_pro'
        },
        {
          label: 'Clássico',
          value: 'gold_special'
        },
        {
          label: 'Grátis',
          value: 'free'
        }
      ]
    }*/
  ];
