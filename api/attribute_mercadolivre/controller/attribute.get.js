const data = require("../data");
const mercadolivre = require("../../../service/mercadolivre");

const getOptions = (values) => {
  if (values && values.length > 0)
    return values.map((o) => {
      return {
        label: o.name,
        value: o.name,
      };
    });
};

module.exports = async function (req, res) {
  if (req.query.category) {
    const categoryId = req.query.category;
    let meliObject = await mercadolivre.meli();
    if (!!categoryId && categoryId != 0) {
      let result = await meliObject.getAsync(
        `/categories/${categoryId}/attributes`
      );
      if (result.message) return res.status(404).send(result);
      const attributes = result.map((a) => {
        return {
          name: "attributes." + a.id,
          label: a.name,
          options: getOptions(a.values),
        };
      });
      return res.json(attributes);
    }
  }
  const attributes = data.attributes;

  attributes
    ? res.json(attributes)
    : res.status(400).send({ message: "empty attributes" });
};
