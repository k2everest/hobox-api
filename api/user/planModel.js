const ObjectId = require("mongodb").ObjectId;
const Helper = require("./helper");
const db = require("../../service/db");

function handlePlan(_plan) {
  let plan = _plan;
  if (!Helper.Plan.planExist(plan) || Helper.Plan.isCanceledAndInactive(plan)) {
    console.log("plano noa existe");
    plan = Helper.Plan.getNewPlan();
  } else if (Helper.Plan.isPendingPlan(plan)) {
    console.log("plano pendente");
    plan.pending = false;
  } else if (Helper.Plan.isPlanOK(plan)) {
    console.log("plano ok");
    plan = Helper.Plan.getRenewedPlan(plan);
  }
  return plan;
}

exports.updatePlanByUser = async (userId) => {
  let user = await db.collection("user").findOneAsync(userId);
  const plan = user.plan;
  if (Helper.Plan.planExist(plan)) {
    let currentPlan = plan;
    currentPlan.canceled = Helper.Plan.shouldSetCanceled(plan);
    currentPlan.pending = Helper.Plan.shouldSetPending(plan);
    currentPlan.status = Helper.Plan.getStatus(plan);
    await updatePlan(userId, currentPlan);
    return "plan updated";
  } else return "plan not exist";
};

async function updatePlan(owner, currentPlan) {
  let plan = {};
  for (let key in currentPlan) {
    plan[`plan.${key}`] = currentPlan[key];
  }
  console.log("plano", plan);

  try {
    result = await db
      .collection("user")
      .updateOneAsync(
        { _id: ObjectId(owner) },
        { $set: plan },
        { upsert: false }
      );
    return result;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
}

async function getPlan(id) {
  try {
    let result = await db
      .collection("user")
      .findOneAsync({ _id: ObjectId(id) });
    if (result.plan) return result.plan;
    else return null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
}

exports.updatePlanByPayment = async (payment) => {
  //const isPaymentConfirmed = payment.status == "CO";
  const plan = await getPlan(payment.owner);
  let currentPlan = plan;
  //if(isPaymentConfirmed){
  currentPlan = handlePlan(currentPlan);
  return await updatePlan(payment.owner, currentPlan);
  //}
};

exports.getPlan = getPlan;
exports.updatePlanByOwner = updatePlan;
