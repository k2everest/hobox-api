const Joi = require("joi");
const db = require("../../service/db");
bcrypt = require("bcrypt-nodejs");
const ObjectId = require("mongodb").ObjectId;
const crypto = require("crypto");

exports.Plan = require("./planModel");

let type = {
  email: !undefined,
  password: !undefined,
  name: !undefined,
  lastname: !undefined,
  role: !undefined,
};

const schema = Joi.object()
  .keys({
    name: Joi.string().min(3).max(50).required(),
    lastname: Joi.string().min(3).max(50).required(),
    password: Joi.string()
      .regex(/^[a-zA-Z0-9]{3,30}$/)
      .required(),
    email: Joi.string().email().required(),
    role: Joi.string().required(),
  })
  .with("email", "password");

const passwordSchema = Joi.object().keys({
  currentPassword: Joi.string()
    .regex(/^[a-zA-Z0-9]{3,30}$/)
    .required(),
  newPassword: Joi.string()
    .regex(/^[a-zA-Z0-9]{3,30}$/)
    .required(),
  confirmPassword: Joi.string()
    .regex(/^[a-zA-Z0-9]{3,30}$/)
    .required()
    .valid(Joi.ref("newPassword")),
});

const removeDataFromAccount = async (id) => {
  try {
    await Promise.all([
      db.collection("plataform").removeAsync({ owner: id }),
      db.collection("order").removeAsync({ owner: id }),
      db.collection("product").removeAsync({ owner: id }),
      db.collection("attribute").removeAsync({ owner: id }),
      db.collection("category").removeAsync({ owner: id }),
      db.collection("user").removeAsync({ _id: ObjectId(id) }),
    ]);
    return true;
  } catch (err) {
    return {
      type: false,
      message: err.message,
    };
  }
};

const handleIsDeleted = async (id, isDeleted, deletedDatetime = null) => {
  try {
    await db
      .collection("user")
      .updateOneAsync(
        { _id: ObjectId(id) },
        { $set: { isDeleted: isDeleted, isDeletedCreatedAt: deletedDatetime } },
        { upsert: false }
      );
    return true;
  } catch (err) {
    return {
      type: false,
      message: err.message,
    };
  }
};

exports.upsertGoogleUser = async (accessToken, profile) => {
  let result;
  try {
    //check account exist
    result = await db
      .collection("user")
      .findOneAsync({ email: profile.emails[0].value });
    console.log("resuklt inter", result);
    //create new account
    if (!result) {
      console.log("resuklt 2", result);
      try {
        result = await db.collection("user").findOneAndUpdateAsync(
          { email: profile.emails[0].value },
          {
            name: profile.name.givenName,
            lastname: profile.name.familyName,
            email: profile.emails[0].value,
            googleProvider: {
              id: profile.id,
              token: accessToken,
            },
          },
          { upsert: true, returnOriginal: false }
        );
        result.value.isNew = true;
        return result.value;
      } catch (err) {
        return {
          type: false,
          message: err.message,
        };
      }
    } else {
      return result;
    }
  } catch (err) {
    return {
      type: false,
      message: err.message,
    };
  }
};

exports.checkUserExist = async (User = type) => {
  let result;
  try {
    result = await db.collection("user").findOneAsync({ email: User.email });
    return result
      ? {
          type: false,
          data: "User already exists!",
        }
      : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

exports.softDeleteAccount = async (id, passwordUser) => {
  let trueUser = await isTrueUser(id, passwordUser.currentPassword);
  if (trueUser == false) {
    return {
      type: false,
      message: "wrong password",
    };
  } else {
    return await handleIsDeleted(id, true, new Date());
  }
};

exports.recoverAccount = async (id, passwordUser) => {
  let trueUser = await isTrueUser(id, passwordUser.currentPassword);
  if (trueUser == false) {
    return {
      type: false,
      message: "wrong password",
    };
  } else {
    return await handleIsDeleted(id, false);
  }
};

exports.hardDeleteAccount = async () => {
  try {
    let accountsToDelete = db
      .collection("user")
      .find()
      .toArrayAsync({
        idDeleted: true,
        isDeletedCreatedAt: { $lte: new Date().getTime() - 1000 * 86400 * 30 },
      });
    accountsToDelete.map(
      async (account) => await removeDataFromAccount(account._id)
    );
  } catch (err) {
    return {
      type: false,
      message: err.message,
    };
  }
};

exports.addUser = async (User = type) => {
  let salt = bcrypt.genSaltSync(10);
  let hash_password = bcrypt.hashSync(User.password, salt);
  let valid = validate({
    email: User.email,
    password: User.password,
    name: User.name,
    lastname: User.lastname,
    role: User.role,
  });
  if (valid != null) {
    return {
      type: false,
      validation: valid,
    };
  } else {
    let result;
    try {
      result = await db.collection("user").findOneAndUpdateAsync(
        { email: User.email },
        {
          name: User.name,
          lastname: User.lastname,
          email: User.email,
          password: hash_password,
          role: User.role,
        },
        { upsert: true, returnOriginal: false }
      );
      return result;
    } catch (err) {
      return {
        type: false,
        message: err.message,
      };
    }
  }
};

exports.getUserList = async () => {
  try {
    let users = await db.collection("user").find().toArrayAsync();
    return users.map(function (item) {
      return {
        _id: item._id,
        name: item.name,
        lastname: item.lastname,
        email: item.email,
        role: item.role,
      };
    });
  } catch (err) {
    return {
      type: false,
      message: err.message,
    };
  }
};

function validate(User = type) {
  const result = Joi.validate(User, schema);
  return result.error;
}

function validatePassword(
  Password = {
    currentPassword: !undefined,
    newPassword: !undefined,
    confirmPassword: !undefined,
  }
) {
  const result = Joi.validate(Password, passwordSchema);
  return result.error;
}

async function getCurrentUser(id) {
  try {
    result = await db.collection("user").findOneAsync({ _id: ObjectId(id) });
    return result
      ? {
          id: ObjectId(result._id).toString(),
          name: result.name,
          lastname: result.lastname,
          email: result.email,
          plan: result.plan,
        }
      : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
}

exports.getCurrentUser = getCurrentUser;

exports.getCompanyUser = async (id) => {
  try {
    result = await db.collection("user").findOneAsync({ _id: ObjectId(id) });
    return result && result.company ? result.company : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

exports.updateCurrentUser = async (id, currentUser) => {
  try {
    let result = await db
      .collection("user")
      .findOneAndUpdateAsync(
        { _id: ObjectId(id) },
        { $set: { name: currentUser.name, lastname: currentUser.lastname } },
        { returnOriginal: false }
      );
    result = result.value;
    return result
      ? {
          name: result.name,
          lastname: result.lastname,
          email: result.email,
        }
      : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

exports.updateCompanyUser = async (id, company) => {
  try {
    let result = await db.collection("user").findOneAndUpdateAsync(
      { _id: ObjectId(id) },
      {
        $set: {
          company: {
            name: company.name,
            website: company.website,
            documentNumber: company.documentNumber,
            documentName: company.documentName,
            phone: company.phone,
            address: {
              street: company.address.street,
              city: company.address.city,
              state: company.address.state,
              zip: company.address.zip,
            },
          },
        },
      },
      { returnOriginal: false, upsert: false }
    );
    result = result.value;
    return result ? result.company : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

const isTrueUser = async (id, currentPassword) => {
  let user = await db.collection("user").findOneAsync({ _id: ObjectId(id) });
  if (user.googleProvider && user.googleProvider.token) return true;
  return !user || !bcrypt.compareSync(currentPassword, user.password)
    ? false
    : true;
};

const setNewPassword = async (id, newHashPassword) => {
  try {
    let result = await db
      .collection("user")
      .findOneAndUpdateAsync(
        { _id: ObjectId(id) },
        { $set: { password: newHashPassword } },
        { returnOriginal: false }
      );
    return {
      type: true,
      data: "success",
    };
  } catch (err) {
    return {
      type: false,
      message: "Error occured: " + err,
    };
  }
};

exports.updatePasswordUser = async (id, passwordUser) => {
  console.log("entrou", passwordUser);
  const salt = bcrypt.genSaltSync(10);
  const newHashPassword = bcrypt.hashSync(passwordUser.newPassword, salt);
  let valid = validatePassword({
    currentPassword: passwordUser.currentPassword,
    newPassword: passwordUser.newPassword,
    confirmPassword: passwordUser.confirmPassword,
  });
  if (valid != null) {
    let validMessage = "";
    valid.details.map((a) => (validMessage = a.message + "  "));
    return {
      type: false,
      message: validMessage,
    };
  } else {
    let trueUser = await isTrueUser(id, passwordUser.currentPassword);
    if (trueUser == false) {
      return {
        type: false,
        message: "wrong password",
      };
    } else return await setNewPassword(id, newHashPassword);
  }
};

exports.getUserByEmail = async (currentEmail) => {
  console.log(currentEmail);
  let result;
  try {
    result = await db.collection("user").findOneAsync({ email: currentEmail });
    console.log(result);
    return result ? { user: result, type: true } : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

exports.setResetPasswordToken = async (user, cb) => {
  crypto.randomBytes(20, function (err, buffer) {
    if (err) return cb(err, buffer);
    const token = buffer.toString("hex");
    setResetPasswordToken(token);
  });

  function setResetPasswordToken(token) {
    console.log("id", user);
    db.collection("user").findOneAndUpdate(
      { _id: ObjectId(user._id) },
      {
        $set: {
          resetPasswordToken: token,
          resetPasswordExpires: Date.now() + 86400000,
        },
      },
      { returnOriginal: false },
      function (err, result) {
        cb(err, result.value);
      }
    );
  }
};

exports.setNewPasswordReset = async (token, newPassword) => {
  let result;
  const salt = bcrypt.genSaltSync(10);
  try {
    result = await db.collection("user").findOneAndUpdateAsync(
      {
        resetPasswordToken: token,
        resetPasswordExpires: {
          $gt: Date.now(),
        },
      },
      {
        $set: {
          password: bcrypt.hashSync(newPassword, salt),
          resetPasswordToken: undefined,
          resetPasswordExpires: undefined,
        },
      },
      { returnOriginal: false }
    );
    return result.value ? { user: result.value, type: true } : null;
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

exports.checkIsDeleted = async (id) => {
  try {
    let result = await db
      .collection("user")
      .findOneAsync({ _id: ObjectId(id) });
    return result && result.isDeleted == true
      ? { data: "User is deactivated", status: true }
      : { data: "User is active", status: false };
  } catch (err) {
    return {
      type: false,
      data: "Error occured: " + err,
    };
  }
};

/* function handlePlan(_plan){
    let plan =_plan;
    if(!!!Helper.Plan.planExist(plan) || Helper.Plan.isCanceledAndInactive(plan))
        plan = Helper.Plan.getNewPlan();
    else if(Helper.Plan.isPendingPlan(plan))
        plan.pending = false;
    else if(Helper.Plan.isPlanOK(plan))
        plan = Helper.Plan.getRenewedPlan(plan);
    return plan;
}

exports.updatePlanByUser = async (owner) => {
    const user  = await getCurrentUser(owner);
    const plan =  user.plan;
    if(Helper.Plan.planExist(plan)){
        let currentPlan = plan;
        currentPlan.canceled = Helper.Plan.shouldSetCanceled(plan);
        currentPlan.pending = Helper.Plan.shouldSetPending(plan);
        currentPlan.status = Helper.Plan.getStatus(plan);
        return await updatePlan(owner,currentPlan);
    }
};

async function updatePlan(owner,currentPlan){
    try{
        result  = await db.collection('user').updateOneAsync(
            { _id: ObjectId(owner) },
            { $set:
                { plan: currentPlan }
             },
             {upsert: false}
        );
        return result;
    }catch(err){
        return({
            type: false,
            data: "Error occured: " + err
        });
    }
}

exports.updatePlanByOwner = updatePlan;

exports.updatePlanByPayment = async (payment)=>{
    //const isPaymentConfirmed = payment.status == "CO";
    const user  = await getCurrentUser(payment.owner);
    let currentPlan = user.plan;
    //if(isPaymentConfirmed){
       currentPlan = handlePlan(currentPlan);
       return await updatePlan(payment.owner,currentPlan);
    //}
};

exports.getPlan = async (id) => {
    try{
        let result  = await db.collection('user').findOneAsync({_id: ObjectId(id)});
        if(result.plan)
            return result.plan;
        else
            return null;
    }catch(err){
        return({
            type: false,
            data: "Error occured: " + err
        });
    }
} */
