var express = require("express");
var router = express.Router();
var userHandlers = require("./userController.js");
const authorize = require("../../middleware/authorize");
const helpers = require("../../helpers");

//add new user
router.post("/", userHandlers.register);

//get all users
router.get("/all", authorize(helpers.Role.Admin), userHandlers.list);

//get info about me
router.get("/me", userHandlers.getCurrentUser);

//get info about my company
router.get("/me/company", userHandlers.getCompanyUser);

//update info about me
router.put("/", userHandlers.updateCurrentUser);

//update info about company
router.put("/company", userHandlers.updateCompanyUser);

//update password
router.put("/password", userHandlers.updatePasswordUser);

//softDeleteAccount
router.put("/deactivate", userHandlers.softDeleteAccount);

//RecoverAccount
router.put("/recover", userHandlers.recoverAccount);

module.exports = router;
