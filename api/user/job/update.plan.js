const Model = require("../userModel");

module.exports = async () => {
  let users = await Model.getUserList();
  for (let user of users) {
    try {
      let result = await Model.Plan.updatePlanByUser(user._id);
      console.log("result-auto-user-plan", result);
    } catch (err) {
      return {
        error: true,
        errorMessage: err.message,
      };
    }
  }
};
