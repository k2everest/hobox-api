const User = require("../userModel");
const updatePlan = require("./update.plan");
const agenda = require("../../../service/agenda");

//define agenda para apagar dados de usuario excluido em Hobox
/* agenda.define('hardDeleteAccount', function(job, done) {
  User.hardDeleteAccount();
  done(console.log('Hard Deleted executed'));
});

///start jobs
agenda.on('ready', function() {
  agenda.every('one day', 'hardDeleteAccount',{});
  agenda.start();
}); */

//define agenda para atualizar dados de plano de usuario em Hobox
agenda.define("updateUserPlan", function (job, done) {
  updatePlan();
  done(console.log("Update user Plan is running"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("one day", "updateUserPlan", {});
  agenda.start();
});
