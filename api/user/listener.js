const Model = require("./userModel");
const events = require("../../service/event");

module.exports = (function () {
  events.on("onConfirmPayment", updatePlan);

  async function updatePlan(params) {
    console.log("o evento updatePlan disparou");
    console.log("params", params.payment);
    let result = Model.Plan.updatePlanByPayment(params.payment);
    console.log("model res", result);
  }
})();
