const Plan = {
  defineDueDatePlan(startDate = null) {
    let newDueDatePlan = null;
    if (startDate === null)
      newDueDatePlan = new Date(new Date().getTime() + 1000 * 86400 * 30);
    else newDueDatePlan = new Date(startDate.getTime() + 1000 * 86400 * 30);

    return newDueDatePlan;
  },

  getNewPlan() {
    return {
      name: "premium",
      status: "active",
      startDate: new Date(),
      dueDate: new Date(new Date().getTime() + 1000 * 86400 * 30),
      canceled: false,
      pending: false,
    };
  },

  getRenewedPlan(plan) {
    let renewedPlan = plan;
    renewedPlan.dueDate = new Date(plan.dueDate.getTime() + 1000 * 86400 * 30);
    return renewedPlan;
  },

  planExist(plan) {
    return plan ? true : false;
  },

  isCanceled(plan) {
    return plan && plan.canceled === true;
  },

  isCanceledAndActivated(plan) {
    return (
      plan &&
      plan.canceled === true &&
      plan.status === "active" &&
      plan.pending === false
    );
  },

  isCanceledAndInactive(plan) {
    return (
      plan &&
      plan.canceled === true &&
      plan.status == "inactive" &&
      plan.pending === false
    );
  },

  isPlanOK(plan) {
    return plan && plan.canceled === false && plan.pending === false;
  },

  isPendingPlan(plan) {
    return plan && plan.pending === false;
  },

  shouldSetPending(plan) {
    return plan && plan.dateDue < new Date();
  },

  shouldSetCanceled(plan) {
    return new Date(plan.dueDate.getTime() + 1000 * 86400 * 30) < new Date();
  },

  getStatus(plan) {
    return plan && plan.canceled && plan.dateDue < new Date()
      ? "inactive"
      : "active";
  },
};

module.exports = { Plan };
