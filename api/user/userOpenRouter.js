var express = require("express");
var router = express.Router();
var userHandlers = require("./userController.js");

//add new user
router.post("/", userHandlers.register);

module.exports = router;
