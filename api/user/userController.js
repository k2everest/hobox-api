"use strict";
const User = require("./userModel");
const jwt = require("jsonwebtoken");
const smtpTransport = require("../../service/smtpTransport");
const email = process.env.MAILER_EMAIL_ID || "noreply@hobox.com.br";
const Role = require("../../helpers").Role;

exports.recoverAccount = async function (req, res) {
  const _id = req.decoded._id;
  const userPassword = req.body.userPassword;
  let result = await User.recoverAccount(_id, userPassword);
  console.log(result);
  return result.type == false ? res.status(400).send(result) : res.json(result);
};

exports.register = async function (req, res) {
  const body = req.body.user;
  let user = body;
  user.role = Role.User;
  const key = process.env.JWT_SECRET;
  console.log("user", user);
  let result = await User.checkUserExist(user);
  if (result) return res.status(400).send(result);

  result = await User.addUser(user);
  result.type && result.type == false
    ? res.status(400).send(result)
    : sendEmailAndResponse(result.value);

  function sendEmailAndResponse(user) {
    const data = {
      to: user.email,
      from: email,
      template: "welcome-email",
      subject: "Bem-vindo ao Hobox",
      context: {
        name: user.name,
      },
    };

    smtpTransport.sendMail(data, function (err) {
      if (!err) {
        console.log("enviou email");
      } else {
        console.log("erro ao enviar email");
      }
    });
    return res.json({
      username: user.name,
      token: jwt.sign(
        { email: user.email, fullName: user.name, _id: user._id },
        key /* , { expiresIn: 24*60*60 } */
      ),
    });
  }
};

exports.softDeleteAccount = async function (req, res) {
  const _id = req.decoded._id;
  const userPassword = req.body.userPassword;
  let result = await User.softDeleteAccount(_id, userPassword);
  if (result.type == false) return res.status(401).send(result);
  else return res.json(result);
};

exports.loginRequired = function (req, res, next) {
  if (req.user) {
    next();
  } else {
    return res.status(401).json({ message: "Unauthorized user!" });
  }
};

exports.list = async function (req, res) {
  let result = await User.getUserList();
  return result.type && result.type == false
    ? res.status(400).send(result)
    : res.json(result);
};

exports.getCurrentUser = async function (req, res) {
  const _id = req.decoded._id;
  let result = await User.getCurrentUser(_id);
  result.type && result.type == false
    ? res.status(400).send(result)
    : res.json(result);
};

exports.getCompanyUser = async function (req, res) {
  const _id = req.decoded._id;
  let result = await User.getCompanyUser(_id);
  result && result.type == false
    ? res.status(400).send(result)
    : res.json(result);
};

exports.updateCurrentUser = async function (req, res) {
  const _id = req.decoded._id;
  const currentUser = req.body.user;
  let result = await User.updateCurrentUser(_id, currentUser);
  result.type && result.type == false
    ? res.status(400).send(result)
    : res.json(result);
};

exports.updatePasswordUser = async function (req, res) {
  const _id = req.decoded._id;
  const userPassword = req.body.userPassword;
  let result = await User.updatePasswordUser(_id, userPassword);
  if (result.type == false) {
    res.status(400);
    res.statusMessage = result.message;
    res.send();
  } else res.json(result);
};

exports.updateCompanyUser = async function (req, res) {
  const _id = req.decoded._id;
  let company = req.body.company;
  let result = await User.updateCompanyUser(_id, company);
  if (result.type == false) {
    res.status(400);
    res.statusMessage = result.message;
    res.send();
  } else res.json(result);
};
