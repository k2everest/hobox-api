const syncLog = require("../../../service/syncLog");
const parser = require("../parser");
const magento = require("../../../service/magento");
const logger = require("../../../service/winston");
const Order = require("../model");

module.exports = async (order, platform, callback) => {
  if (order.status.value != "processing")
    return callback("not processing", null);

  const formatedOrder = parser.magento.formatOrder(order);
  const orderCode = formatedOrder.hobox_code;

  invoiceOrderToMagento(orderCode);

  async function invoiceOrderToMagento(hoboxCode) {
    magento(platform).xmlrpc.auto.integrator.order.invoice(
      hoboxCode,
      handleResponse
    );
  }

  function handleResponse(err, res) {
    if (err) {
      console.log("entrou cathc");
    } else {
      console.log("res", res);
      updateOrderHobox(res);
    }
    callback(err, res);
  }

  function setLogs(err) {
    if (err.length > 0) {
      logger.error({
        message: err[0],
        meta: {
          resource: "order",
          origin: "hobox",
          destiny: "magento",
          owner: order.owner,
          action: "invoice order",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        resource: "order",
        status: "fail",
        owner: order.owner,
        message: err[0],
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        id: order.id,
        resource: "order",
        status: "success",
        owner: order.owner,
        message: "invoice order",
      });
    }
  }

  function updateOrderHobox(res = []) {
    let errors = res;
    let currentMessage = "invoiced";
    let currentInvoice = true;

    if (errors.length > 0) {
      currentMessage = errors[0];
      currentInvoice = false;
    }

    setLogs(errors);
    Order.updateInvoiceStatus(
      order._id,
      order.packId,
      currentInvoice,
      currentMessage
    );
  }
};
