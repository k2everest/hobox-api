exports.isAllowedSendOrderToMagento = function (platform) {
  return platform.syncConfig && platform.syncConfig.order.send.value;
};
