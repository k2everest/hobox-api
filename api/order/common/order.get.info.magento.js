const magento = require("../../../service/magento");
const Order = require("../model");
const Event = require("../event");


module.exports = async (platform, callback) => {
  let controller = -1;
  const orders = await Order.findForInfo(platform.owner);

  controlAction();

  function controlAction() {
    controller = controller + 1;
    if (controller < orders.length)
      getInfoFromMagento(orders[controller].externalId);
    else
      Event.onUpdateOrderFromMagento(platform);

  }

  function getInfoFromMagento(orderIncrementId) {
    magento(platform).xmlrpc.auto.integrator.order.getOrderInfo(
      orderIncrementId,
      handleResponse
    );
  }

  function handleResponse(err, res) {
    if (err) {
      console.log("entrou cathc");
    } else {
      if (res != null)
        Order.updateInfo(
          orders[controller]._id,
          orders[controller].packId,
          res,
          "order info updated"
        );
      controlAction();
    }
    callback(err, res);
  }
};
