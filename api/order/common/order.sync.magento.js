const syncLog = require("../../../service/syncLog");
const parser = require("../parser");
const magento = require("../../../service/magento");
const logger = require("../../../service/winston");
const invoiceOrder = require("./order.invoice.magento");
const helpers = require("./helpers");
const Order = require("../model");
const Product = require("../../product/model");

module.exports = async (platform, cb) => {
  let controller = -1;
  //let isUserAllowed = await Common.isUserAllowed(platform.owner);
  //if(!helpers.isAllowedSendOrderToMagento(platform) || !isUserAllowed){
  if (!helpers.isAllowedSendOrderToMagento(platform)) {
    return cb("Ação bloqueada", null);
  }
  let orders = await Order.findNotSyncedMagento(platform.owner);
  orders = await Promise.all(
    orders.map(async (order) => {
      order.freeShippingDiscount =
        order.sellerShippingCost - order.shippingCost;
      order.ruleOffsetFreight = await handleShippingDiscount(order);
      return order;
    })
  );
  //const clear = await db.collection('orders').updateAsync({ owner: platform.owner}, {$unset: { sync: ""}},{ multi:true });
  try {
    controlSync();
    cb(null, "success");
  } catch (err) {
    cb("error dentro cb " + err, null);
  }

  function controlSync() {
    controller = controller + 1;
    if (controller < orders.length) handleOrdersToMagento(orders[controller]);
  }

  async function handleOrdersToMagento(order) {
    if (order.sync != "ok" && order.status.value != "canceled") {
      let formatedOrder = parser.magento.formatOrder(order);
      sendOrderToMagento(formatedOrder);
    } else {
      controlSync();
    }
  }

  async function sendOrderToMagento(formatedOrder) {
    magento(platform).xmlrpc.auto.integrator.order.create(
      "default",
      JSON.stringify(formatedOrder),
      handleResponse
    );
  }

  function handleResponse(err, res) {
    if (err) {
      console.log("entrou cathc");
    } else {
      updateOrderHobox(res);
      controlSync();
    }
  }

  function setLogs(err) {
    const order = orders[controller];
    if (err.length > 0) {
      logger.error({
        message: err[0],
        meta: {
          resource: "order",
          origin: "hobox",
          destiny: "magento",
          owner: order.owner,
          action: "create order",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        resource: "order",
        id: order.id,
        status: "fail",
        owner: order.owner,
        message: err[0],
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        id: order.id,
        resource: "order",
        status: "success",
        owner: order.owner,
        message: "created",
      });
    }
  }

  function updateOrderHobox(res = []) {
    const order = orders[controller];
    let errors = res;
    let currentMessage = "ok";
    let currentSync = "ok";
    let hoboxCode = order.plataform + "-" + order.id;

    if (errors.length > 0) {
      currentMessage = errors[0];
      currentMessage !== "order already imported"
        ? (currentSync = "error")
        : "";
    } else {
      invoiceOrder(order, platform, function (err, res) {
        console.log(err, res);
      });
    }
    setLogs(errors);
    Order.updateSyncStatus(
      order._id,
      order.packId,
      currentSync,
      hoboxCode,
      currentMessage
    );
  }
};

async function handleShippingDiscount(order) {
  let _offsetFreight = false;
  let items = order.orderItems;
  for (let itemCurrent of items) {
    let item = await Product.findByOwnerAndSku(order.owner, itemCurrent.sku);
    if (
      item &&
      item.offsetFreight &&
      item.offsetFreight.includes(order.plataform)
    )
      _offsetFreight = true;
  }

  return _offsetFreight;
}
