const cancelOrder = require("./order.cancel.magento");
const helpers = require("./helpers");
const Order = require("../model");

module.exports = async (platform, cb) => {
  let controller = -1;
  if (!helpers.isAllowedSendOrderToMagento(platform)) return;
  const orders = await Order.findForCanceled(platform.owner);
  try {
    controlSync();
  } catch (err) {
    cb("error dentro cb " + err, null);
  }

  function callback(err, res) {
    console.log("callback sync cancel ", err, res);
    controlSync();
  }

  function controlSync() {
    controller = controller + 1;
    if (controller < orders.length) handleOrdersToMagento(orders[controller]);
  }

  async function handleOrdersToMagento(order) {
    cancelOrder(order, platform, callback);
  }
};
