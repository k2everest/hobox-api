const magento = require("../../../service/magento");
const Order = require("../model");

module.exports = async (platform, callback) => {
  let controller = -1;
  const orders = await Order.findHasNotExternalId(platform.owner);

  controlAction();

  function controlAction() {
    controller = controller + 1;
    if (controller < orders.length) getExternalId(orders[controller].code);
  }

  function getExternalId(hoboxCode) {
    magento(platform).xmlrpc.auto.integrator.order.getIncrementId(
      hoboxCode,
      handleResponse
    );
  }

  function handleResponse(err, res) {
    if (err) {
      console.log("entrou cathc");
    } else {
      if (res[0] == "success")
        Order.updateExternalId(
          orders[controller]._id,
          orders[controller].packId,
          res[1],
          "External id updated"
        );
      controlAction();
    }
    callback(err, res);
  }
};
