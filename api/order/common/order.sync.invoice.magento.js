const db = require("../../../service/db");
const invoiceOrder = require("./order.invoice.magento");
const helpers = require("./helpers");
const Order = require("../model");

module.exports = async (platform, cb) => {
  let controller = -1;
  if (!helpers.isAllowedSendOrderToMagento(platform)) return;
  //const clear = await db.collection('orders').updateAsync({ owner: platform.owner}, {$unset: { sync: ""}},{ multi:true });
  const orders = await Order.findForInvoice(platform.owner);
  try {
    controlSync();
    //cb(null,'success');
  } catch (err) {
    cb("error dentro cb " + err, null);
  }

  function callback(err, res) {
    console.log("callback sync invoice ", err, res);
    controlSync();
  }

  function controlSync() {
    controller = controller + 1;
    if (controller < orders.length) handleOrdersToMagento(orders[controller]);
  }

  async function handleOrdersToMagento(order) {
    invoiceOrder(order, platform, callback);
  }
};
