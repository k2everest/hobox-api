module.exports.syncMagento = require("./order.sync.magento");
module.exports.syncInvoiceMagento = require("./order.sync.invoice.magento");
module.exports.syncCancelMagento = require("./order.sync.cancel.magento");
module.exports.invoiceMagento = require("./order.invoice.magento");
module.exports.helpers = require("./order.invoice.magento");
module.exports.getIdMagento = require("./order.get.id.magento");
module.exports.getInfoMagento = require("./order.get.info.magento");
