const syncLog = require("../../../service/syncLog");
const parser = require("../parser");
const magento = require("../../../service/magento");
const logger = require("../../../service/winston");
const Order = require("../model");

module.exports = async (order, platform, callback) => {
  const formatedOrder = parser.magento.formatOrder(order);
  const orderCode = formatedOrder.hobox_code;

  cancelOrderMagento(orderCode);

  async function cancelOrderMagento(hoboxCode) {
    magento(platform).xmlrpc.auto.integrator.order.cancel(
      hoboxCode,
      handleResponse
    );
  }

  function handleResponse(err, res) {
    if (err) {
      console.log("entrou cathc");
    } else {
      console.log("res", res);
      updateOrderHobox(res);
    }
    callback(err, res);
  }

  function setLogs(err) {
    if (err != "canceled") {
      logger.error({
        message: err,
        meta: {
          resource: "order",
          origin: "hobox",
          id: order.id,
          destiny: "magento",
          owner: order.owner,
          action: "cancel order",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        resource: "order",
        id: order.id,
        status: "fail",
        owner: order.owner,
        message: err,
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "magento",
        id: order.id,
        resource: "order",
        status: "success",
        owner: order.owner,
        message: "cancel order",
      });
    }
  }

  function updateOrderHobox(res) {
    let currentMessage = res;
    let isCanceled = res != "canceled" ? false : true;
    setLogs(res);
    Order.updateCanceledStatus(
      order._id,
      order.packId,
      isCanceled,
      currentMessage
    );
  }
};
