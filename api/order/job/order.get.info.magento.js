const common = require("../common");

module.exports = async function (db) {
  let platforms = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .sort({ _id: -1 })
    .toArrayAsync();
  platforms.forEach(main);

  async function main(platform) {
    common.getInfoMagento(platform, cb);
    console.log("platform " + platform._id);
  }

  function cb(err, res) {
    console.log("platform has updated info of orders  " + err ? err : res);
  }
};
