const common = require("../common");

module.exports = async function (db) {
  let platforms = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .sort({ _id: -1 })
    .toArrayAsync();
  platforms.forEach(main);

  async function main(platform) {
    common.getIdMagento(platform, cb);
    console.log("platform " + platform._id);
  }

  function cb(err, res) {
    console.log(
      "platform has updated external id of orders  " + err ? err : res
    );
  }
};
