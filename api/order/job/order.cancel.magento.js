const common = require("../common");

module.exports = async function (db) {
  let platforms = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .sort({ _id: -1 })
    .toArrayAsync();
  platforms.forEach(main);

  async function main(platform) {
    common.syncCancelMagento(platform, cb);
    console.log("platform " + platform._id);
  }

  function cb(err, res) {
    console.log("platform cancel action finished " + err ? err : res);
  }
};
