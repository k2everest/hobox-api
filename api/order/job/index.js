const invoiceOrders = require("./order.invoice.magento");
const cancelOrders = require("./order.cancel.magento");
const createOrders = require("./order.create.magento");
const getIdOrdersMagento = require("./order.get.id.magento");
const getInfoOrdersMagento = require("./order.get.info.magento");

const db = require("../../../service/db");
const agenda = require("../../../service/agenda");

//define agenda
agenda.define("invoiceOrdersMagento", function (job, done) {
  invoiceOrders(db);
  done(console.log("invoice orders magento"));
});

agenda.define("cancelOrdersMagento", function (job, done) {
  cancelOrders(db);
  done(console.log("cancel orders magento"));
});

agenda.define("createOrdersMagento", function (job, done) {
  createOrders(db);
  done(console.log("create orders magento"));
});

agenda.define("getIdOrdersMagento", function (job, done) {
  getIdOrdersMagento(db);
  done(console.log("get id orders magento"));
});

agenda.define("getInfoOrdersMagento", function (job, done) {
  getInfoOrdersMagento(db);
  done(console.log("get info orders magento"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("20 minute", "cancelOrdersMagento", {});
  agenda.start();
});
agenda.on("ready", function () {
  agenda.every("20 minute", "invoiceOrdersMagento", {});
  agenda.start();
});

agenda.on("ready", function () {
  agenda.every("20 minute", "createOrdersMagento", {});
  agenda.start();
});

agenda.on("ready", function () {
  agenda.every("2 hours", "getIdOrdersMagento", {});
  agenda.start();
});

agenda.on("ready", function () {
  agenda.every("20 minute", "getInfoOrdersMagento", {});
  agenda.start();
});
