const common = require("../common");

module.exports = async function (db) {
  let platforms = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .toArrayAsync();
  platforms.forEach(run);

  async function run(platform) {
    common.syncMagento(platform, cb);
  }

  function cb(err, res) {
    console.log("platform create order cron " + err ? err : res);
  }
};
