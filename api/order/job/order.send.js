const meli = require("mercadolibre");
var fs = require("fs");
const parser = require("../parser/index");
const magento = require("../../../service/magento");
const ObjectId = require("mongodb").ObjectId;

module.exports = async function (db) {
  let platforms = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .toArrayAsync();
  platforms.forEach(main);

  async function main(platform) {
    const orders = await db
      .collection("orders")
      .find({ owner: platform.owner })
      .sort({ id: -1 })
      .toArrayAsync();
    let i = 0;
    sendMagento(i, platform);

    function sendMagento(i, platform) {
      if (i < orders.length) {
        let order = orders[i];
        if (order.sync != "ok") {
          let formatedOrder = parser.formatOrder(order);
          magento(platform).xmlrpc.auto.integrator.order.create(
            "default",
            JSON.stringify(formatedOrder),
            function (err, result) {
              if (err) throw console.log("err", err);
              else {
                setSyncOrder(result, order, formatedOrder.hobox_code);
                return sendMagento(i + 1, platform);
              }
            }
          );
        } else return sendMagento(i + 1, platform);
      } else return;
    }

    function setSyncOrder(stateSync, order, hoboxCode) {
      console.log(stateSync);
      console.log("entrou set", order.id);
      const orderId = ObjectId(order._id);
      let currentStateSync = stateSync.length > 0 ? "error" : "ok";
      const currentMessage = stateSync.length > 0 ? stateSync[0] : "sent";

      currentMessage === "Pedido já importado" ? (currentStateSync = "ok") : "";

      return db.collection("orders").update(
        { _id: orderId },
        {
          $set: { sync: currentStateSync, code: hoboxCode },
          $push: {
            logs: {
              dateCreated: new Date(),
              from: "hobox",
              to: "magento",
              message: currentMessage,
            },
          },
        },
        function (err, result) {
          if (err) {
            fs.createWriteStream("./logs/ml.log", { flags: "a" }).write(
              new Date() + " error save order: " + order.id + "\n"
            );
            console.log(err);
          } else {
            console.log("salvou", orderId);
            return;
          }
        }
      );
    }
  }
};
