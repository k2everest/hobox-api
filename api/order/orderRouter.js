var express = require("express");
var router = express.Router();
const orderHandlers = require("./orderController");
const orderSyncMagento = require("./controller/order.sync.magento");
const listOrders = require("./controller/order.list");

/**
 *@swagger
 * definitions:
 *
 *  Item:
 *   type: object
 *   properties:
 *      id:
 *          type: string
 *      sku:
 *          type: string
 *      qty:
 *          type: integer
 *      name:
 *          type: string
 *      variation:
 *          type: string
 *      price:
 *          type: number
 *  Address:
 *      type: object
 *      properties:
 *          street:
 *            type: string
 *          number:
 *            type: string
 *          complement:
 *            type: string
 *          neighborhood:
 *            type: string
 *          city:
 *            type: string
 *          region:
 *            type: string
 *          country:
 *            type: string
 *          postcode:
 *            type: string
 *
 *
 *
 *
 *
 *  Order:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       plataform:
 *         type: string
 *       status:
 *         type: string
 *       dateCreated:
 *         type: date
 *       items:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Item'
 *       customer:
 *          type: object
 *          properties:
 *           name:
 *               type: string
 *           email:
 *               type: string
 *           dob:
 *               type: string
 *           gender:
 *               type: string
 *           vatNumber:
 *               type: string
 *           phones:
 *               type: array
 *               items:
 *                   type: string
 *       shippingAddress:
 *          $ref: '#/definitions/Address'
 *       billingAddress:
 *          $ref: '#/definitions/Address'
 *       shippingMethod:
 *          type: string
 *       shippingCost:
 *         type: number
 *       total:
 *         type: number
 *
 *
 *  Token:
 *     type: object
 *     properties:
 *      token:
 *        type: string
 *  Error:
 *     type: object
 *     properties:
 *      message:
 *        type: string
 *      error:
 *        type: string
 *
 */

/**
 *@swagger
 * /orders:
 *   get:
 *     tags:
 *       - Orders
 *     description: Returns all orders
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of orders
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Order'
 *
 */

//get orders  - replace params page for query param
router.get("/", listOrders);

router.get("/status", orderHandlers.getOrderStatus);

/**
 *@swagger
 * /orders/{id}:
 *   get:
 *     tags:
 *       - Orders
 *     description: Return an order by id
 *     parameters:
 *       - name: id
 *         description: order id
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An order
 *         schema:
 *           $ref: '#/definitions/Order'
 *
 */

//method sync orders
router.post("/sync/magento", orderSyncMagento);

//get a specific order
router.get("/:id", orderHandlers.getOrder);

//router.get('/shipping',orderHandlers.getShippingML);

//get summary of orders
router.get("/reports/summary", orderHandlers.getOrdersSummaryReports);

/**
 *@swagger
 * /orders/reports/lastMonth:
 *   get:
 *     tags:
 *       - Orders
 *     description: Return an array of orders from last 30 days
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of orders from last 30 days
 *         schema:
 *           type: array
 *           items:
 *              type: object
 *              properties:
 *                  id:
 *                    type: string
 *                  qty:
 *                    type: number
 *                  total:
 *                    type: number
 *
 *
 */

//get reports of orders
router.get("/reports/lastMonth", orderHandlers.getOrdersReportsByDate);

/**
 *@swagger
 * /orders/reports/products:
 *   get:
 *     tags:
 *       - Orders
 *     description: Return an array of products with order informations
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of products with order informations
 *         schema:
 *           type: array
 *           items:
 *              type: object
 *              properties:
 *                  id:
 *                    type: string
 *                  qty:
 *                    type: number
 *                  itemName:
 *                    type: string
 *                  total:
 *                    type: number
 *
 *
 */
//get reports produts of orders
router.get("/reports/products", orderHandlers.getOrdersProductsReports);

//get reports produts of orders - should be removed
router.get("/reportproducts/:orderBy", orderHandlers.getOrdersProductsReports);

//sync orders hobox with Magento
router.post("/status/magento/sync", orderHandlers.getOrderStatusMagento);

//router.post('/sync/mercadolivre/schedule',orderHandlers.getOrdersFromMercadoLivreSchedule);

module.exports = router;
