const common = require("../common");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  const getPlatform = async () => {
    let platform = await db
      .collection("plataform")
      .findOneAsync({ owner: ownerId, plataform: "magento" });
    return platform;
  };

  const run = async (platform) => {
    common.syncMagento(platform, cb);
  };

  const cb = (error, result) => {
    if (error) res.status(400).send({ message: error });
    else res.json(result);
  };

  try {
    let platform = await getPlatform();
    platform
      ? run(platform)
      : res.status(400).send({ message: "Platform is not registered" });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};
