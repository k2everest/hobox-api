//list orders

module.exports = async function (req, res) {
  const Order = require("../model");
  let ownerId = req.decoded._id;
  const sortBy = req.query.sortBy ? req.query.sortBy : "dateCreated";
  const direction = req.query.direction
    ? parseInt(req.query.direction == "asc" ? 1 : -1)
    : -1;
  const limit = req.query.limit ? req.query.limit : 50;
  const page = req.query.page ? parseInt(req.query.page) * limit - limit : 0;
  let filter = [];
  let currentSort = {};

  currentSort[handleSort(sortBy)] = direction;

  handleQuery();

  let result = await Order.findAll(ownerId, filter, currentSort, limit, page);
  result.error ? res.status(400).send(result) : res.json(result);

  function handleSort(sortBy) {
    switch (sortBy) {
      case "customer":
        return "buyer.firstName";
      case "trackingNumber":
        return "tracking_number";
      default:
        return sortBy;
    }
  }

  function handleQuery() {
    req.query.packId ? filter.push({ packId: req.query.packId }) : null;
    req.query.id ? filter.push({ id: req.query.id }) : null;
    req.query.marketplaces
      ? filter.push({ plataform: { $in: req.query.marketplaces } })
      : null;
    req.query.status && req.query.status[0] != ""
      ? filter.push({ "status.value": { $in: req.query.status } })
      : null;
    req.query.trackingNumber
      ? filter.push({
          tracking_number: {
            $regex: `.*${req.query.trackingNumber}.*`,
            $options: "i",
          },
        })
      : null;
    req.query.priceFrom
      ? filter.push({ total: { $gte: parseFloat(req.query.priceFrom) } })
      : null;
    req.query.priceTo
      ? filter.push({ total: { $lte: parseFloat(req.query.priceTo) } })
      : null;
    req.query.datetimeFrom
      ? filter.push({ dateCreated: { $lte: req.query.datetimeFrom } })
      : null;
    req.query.datetimeTo
      ? filter.push({ dateCreated: { $gte: req.query.datetimeTo } })
      : null;
    req.query.customer
      ? filter.push({
          $or: [
            {
              "buyer.firstName": {
                $regex: `.*${req.query.customer}.*`,
                $options: "i",
              },
            },
            {
              "buyer.lastName": {
                $regex: `.*${req.query.customer}.*`,
                $options: "i",
              },
            },
          ],
        })
      : null;
    req.query.sync
      ? filter.push({
          sync: { $in: req.query.sync },
        })
      : null;

    if (filter.length < 1) filter.push({});
  }
};
