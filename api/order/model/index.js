const db = require("../../../service/db");

const pageTotal = (ordersTotal, limit) => Math.ceil(ordersTotal / limit);
const getTotalOrders = async (ownerId, filter) => {
  let ordersCount = await db.collection("orders").aggregateAsync([
    {
      $match: { owner: ownerId, $and: filter },
    },
    {
      $addFields: {
        referenceId: { $ifNull: ["$packId", "$id"] },
      },
    },
    {
      $group: {
        _id: "$referenceId",
      },
    },
    {
      $count: "count",
    },
  ]);

  return ordersCount[0] ? ordersCount[0].count : 0;
};

const setOrderLogs = ({ from = "", to = "", message = "" }) => {
  return {
    dateCreated: new Date(),
    from: from,
    to: to,
    message: message,
  };
};

const setMagentoToHoboxLog = (message) => {
  return setOrderLogs({ from: "magento", to: "hobox", message: message });
};

const setHoboxToMagentoLog = (message) => {
  return setOrderLogs({ from: "hobox", to: "magento", message: message });
};

const getOrders = async (ownerId, filter, sortBy, limit, page) => {
  const sortByModel = {};
  Object.keys(sortBy).forEach(
    (key) => (sortByModel["entries." + key] = sortBy[key])
  );

  return await db.collection("orders").aggregateAsync(
    [
      {
        $match: { owner: ownerId, $and: filter },
      },
      {
        $addFields: {
          referenceId: { $ifNull: ["$packId", "$id"] },
        },
      },
      {
        $group: {
          _id: "$referenceId",
          orderItems: { $push: "$orderItems" },
          entries: { $push: "$$ROOT" },
          count: { $sum: 1 },
        },
      },
      { $sort: sortByModel },
      { $skip: page },
      { $limit: limit },
    ],
    { allowDiskUse: true }
  );
};

exports.findAll = async (
  ownerId,
  filter,
  sortBy = { _id: -1 },
  limit = Number.MAX_SAFE_INTEGER,
  page = 0
) => {
  try {
    let ordersTotal = await getTotalOrders(ownerId, filter);
    let orders = await getOrders(ownerId, filter, sortBy, limit, page);
    return {
      orders: mapOrder(orders),
      total: ordersTotal,
      pages: pageTotal(ordersTotal, limit),
    };
  } catch (err) {
    return { error: true, message: err.message };
  }

  function mapOrder(orders) {
    const result = orders.map((o) => {
      return Object.assign({}, o.entries[0], {
        id: o.entries[0].referenceId,
        orderItems: o.orderItems.reduce((acc, val) => acc.concat(val)),
        total: getTotal(o),
      });
    });

    return result;
  }

  function getTotal(order) {
    const orderItems = order.orderItems.reduce((acc, val) => acc.concat(val));
    return (
      parseFloat(order.entries[0].shippingCost) +
      orderItems.reduce((total, i) => total + i.specialPrice * i.qty, 0)
    );
  }
};

exports.findNotSyncedMagento = async (ownerId) => {
  const filter = [
    {
      $or: [
        { sync: { $exists: false } },
        { sync: "error" },
        { sync: "pending" },
      ],
    },
  ];
  const result = await this.findAll(ownerId, filter);
  return result.orders;
};

exports.findNotSyncedNfe = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      "info.nfe": { $exists: true },
      "info.nfe.key": { $ne: null },
      "dateCreated": { $gte: new Date(new Date().setDate(new Date().getDate() - 30)) },
      $or: [{ nfeStatus: { $exists: false } }, { nfeStatus: "error" }],
    })
    .toArrayAsync();

  return orders;
};

exports.findHasNotExternalId = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      $or: [
        { externalId: { $exists: false } },
        { externalId: "error" },
        { externalId: "pending" },
      ],
    })
    .toArrayAsync();

  return orders;
};

exports.updateExternalId = async (
  orderId,
  packId,
  currentExternalId,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId, $or: [{ packId: packId }] },
    {
      $set: { externalId: currentExternalId },
      $push: {
        logs: setHoboxToMagentoLog(currentMessage),
      },
    }
  );
};

exports.updateInfo = async (orderId, packId, infoData, currentMessage) => {
  db.collection("orders").updateAsync(
    { _id: orderId, $or: [{ packId: packId }] },
    {
      $set: { info: infoData },
      $push: {
        logs: setMagentoToHoboxLog(currentMessage),
      },
    }
  );
};

exports.updateSyncStatus = async (
  orderId,
  packId,
  currentSync,
  hoboxCode,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId, $or: [{ packId: packId }] },
    {
      $set: { sync: currentSync, code: hoboxCode },
      $push: { logs: setHoboxToMagentoLog(currentMessage) },
    }
  );
};

exports.updateInvoiceStatus = async (
  orderId,
  packId,
  currentInvoice,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId, $or: [{ packId: packId }] },
    {
      $set: { invoiced: currentInvoice },
      $push: { logs: setHoboxToMagentoLog(currentMessage) },
    }
  );
};

exports.updateNfeStatus = async (orderId, currentStatus, currentMessage) => {
  db.collection("orders").updateAsync(
    { _id: orderId },
    {
      $set: { nfeStatus: currentStatus },
      $push: { logs: setHoboxToMagentoLog(currentMessage) },
    }
  );
};

exports.updateCanceledStatus = async (
  orderId,
  packId,
  currentStatus,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId, $or: [{ packId: packId }] },
    {
      $set: { canceled: currentStatus },
      $push: { logs: setHoboxToMagentoLog(currentMessage) },
    }
  );
};

exports.findForInfo = async (ownerId) => {
  let date = new Date(new Date().setDate(new Date().getDate() - 30));
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      dateCreated: {
        $gte: date,
      },
      $or: [{ "info.nfe": { $exists: false } }, { "info.nfe.key": null }],
      sync: "ok",
      invoiced: true,
    })
    .toArrayAsync();
  return orders;
};

exports.findForInvoice = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      "status.value": "processing",
      sync: "ok",
      $or: [{ invoiced: false }, { invoiced: { $exists: false } }],
    })
    .toArrayAsync();
  return orders;
};

exports.findForCanceled = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      "status.value": "canceled",
      sync: "ok",
      $or: [{ canceled: false }, { canceled: { $exists: false } }],
    })
    .toArrayAsync();
  return orders;
};

exports.updateOrder = (ownerId, order, cb) => {
  db.collection("orders").update(
    { id: order.id, owner: ownerId },
    {
      $set: order,
      $setOnInsert: { sync: "pending" },
    },
    { upsert: true },
    cb
  );
};
