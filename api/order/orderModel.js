var OrderSchema = {
  code: String,
  marketplace: String,
  createdAt: String,
  updatedAt: String,
  totalAmount: Number,
  shippingCost: Number,
};

module.exports = OrderSchema;
