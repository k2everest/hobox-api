"use strict";
//const meli = require('mercadolibre-new');
const fs = require("fs");
const parser = require("./parser");
//let Order = require('./orderModel.js');
let magento = require("../../service/magento");

//list orders
exports.listOrders = function (req, res) {
  console.log("entrou orders");
  let db = req.db;
  let ownerId = req.decoded._id;
  const limit = req.query.limit ? req.query.limit : 100;
  const page = req.query.page ? parseInt(req.query.page) * limit - limit : 0;

  db.collection("orders")
    .find({ owner: ownerId })
    .count(function (err, count) {
      db.collection("orders")
        .find({ owner: ownerId })
        .sort({ id: -1 })
        .limit(limit)
        .skip(page)
        .toArray(function (err, orders) {
          if (err) res.json(err);
          else {
            console.log(count, "countOrder");
            let tpages = Math.ceil(count / limit);
            res.json({ orders: orders, total: count, pages: tpages });
          }
        });
    });
};

//get a specific order
exports.getOrder = function (req, res) {
  var ObjectID = require("mongodb").ObjectID;
  let db = req.db;
  let ownerId = req.decoded._id;
  if (req.params.id)
    db.collection("orders").findOne(
      { _id: new ObjectID(req.params.id) },
      function (err, order) {
        if (err) res.json(err);
        else {
          res.json(order);
        }
      }
    );
};

exports.sendOrderToMagento = function (req, res) {
  let db = req.db;
  const ownerId = req.decoded._id;
  //adicionar filter status de sincronização - apenas produtos nao enviados
  db.collection("orders")
    .find({ owner: ownerId })
    .sort({ id: -1 })
    .toArray(function (err, orders) {
      if (err) res.json(err);
      else {
        db.collection("plataform").findOne(
          { owner: ownerId, plataform: "magento" },
          function (err, currentPlataform) {
            if (err) console.log(err);
            else {
              let i = 0;
              orders.map((order) => {
                i++;
                if (i <= 4) {
                  let formatedOrder = parser.magento.formatOrder(order);
                  console.log(formatedOrder);
                  magento(currentPlataform).xmlrpc.auto.integrator.order.create(
                    "default",
                    JSON.stringify(formatedOrder),
                    function (err, result) {
                      if (err) console.log(err);
                      else {
                        console.log(result);
                        return;
                        //setSyncOrder(result, order.id);
                      }
                    }
                  );
                }
              });
            }
          }
        );
      }
    });
};

exports.getOrdersProductsReports = function (req, res) {
  let db = req.db;
  let sort = {};
  const orderBy = req.params.orderBy;
  sort[orderBy] = -1;
  let ownerId = req.decoded._id;
  console.log(new Date(new Date().setDate(new Date().getDate() - 30)), ownerId);
  let date = new Date(new Date().setDate(new Date().getDate() - 30));
  db.collection("orders").aggregate(
    [
      {
        $match: {
          owner: ownerId,
          dateCreated: {
            $gte: date,
          },
          "status.value": { $in: ["complete", "processing"] },
        },
      },
      {
        $group: {
          _id: "$orderItems.sku",
          qty: { $sum: 1 },
          itemsSold: { $addToSet: "$orderItems.name" },
          total: { $sum: "$total" },
        },
      },
      {
        $sort: sort,
      },
    ],
    function (err, orders) {
      if (err) res.json(err);
      else {
        let currentOrders = orders.map((order) => {
          return {
            _id: order._id[0],
            qty: order.qty,
            itemName: order.itemsSold[0][0],
            total: order.total,
          };
        });
        res.json(currentOrders);
      }
    }
  );
};

exports.getOrdersSummaryReports = function (req, res) {
  let db = req.db;
  let ownerId = req.decoded._id;
  console.log(new Date(new Date().setDate(new Date().getDate() - 30)), ownerId);
  let date = new Date(new Date().setDate(new Date().getDate() - 30));
  let result = [];
  db.collection("orders").aggregate(
    {
      $match: {
        owner: ownerId,
        dateCreated: {
          $gte: date,
        },
        "status.value": { $in: ["complete", "processing"] },
      },
    },
    {
      $group: {
        _id: "_id",
        orderNumber: { $sum: 1 },
        total: { $sum: "$total" },
      },
    },
    function (err, orders) {
      if (err) res.status(400).send({ message: err.message });
      else {
        result = orders[0]
          ? orders[0]
          : { _id: "_id", total: 0, orderNumber: 0 };
        res.json(result);
      }
    }
  );
};

exports.getOrdersReportsByDate = function (req, res) {
  let db = req.db;
  let ownerId = req.decoded._id;
  let date = new Date(new Date().setDate(new Date().getDate() - 30));
  db.collection("orders").aggregate(
    {
      $match: {
        owner: ownerId,
        dateCreated: {
          $gte: date,
        },
        "status.value": { $in: ["complete", "processing"] },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreated" } },
        qty: { $sum: 1 },
        total: { $sum: "$total" },
      },
    },
    {
      $sort: { _id: 1 },
    },
    function (err, orders) {
      if (err) res.json(err);
      else {
        res.json(orders);
      }
    }
  );
};

exports.getOrderStatusMagento = async function (req, res) {
  let db = req.db;
  let ownerId = req.decoded._id;
  let marketplace = await db
    .collection("plataform")
    .findOneAsync({ plataform: "magento", owner: ownerId });
  console.log(marketplace);
  magento(marketplace).xmlrpc.auto.integrator.order.listStatus(function (
    err,
    status
  ) {
    if (err) {
      return res.status(400).send({
        message: err,
      });
    } else {
      console.log(status);
      db.collection("ordersMagentoStatus").update(
        { owner: ownerId },
        { owner: ownerId, status },
        { upsert: true },
        function (err, result) {
          if (err) return res.status(400).send(err);
          else return res.json("synced");
        }
      );
    }
  });
};

exports.getOrderStatus = function (req, res) {
  let db = req.db;
  let ownerId = req.decoded._id;
  //let marketplace =  await  db.collection('plataform').findOneAsync({ plataform: 'magento', owner: ownerId});

  db.collection("ordersMagentoStatus").findOne({ owner: ownerId }, function (
    err,
    result
  ) {
    if (err) return res.status(400).send(err);
    else return res.json(result ? result.status : []);
  });
};

/* let setSyncOrder = function(currentSync, orderId){
    console.log(currentSync);
    let db = req.db;
    const ownerId = req.decoded._id;
    return db.collection('orders').update({ id: orderId, owner: ownerId}, {sync: currentSync}, { upsert:false },
            function(err, result){
                if(err)
                    fs.createWriteStream('./logs/ml.log', {flags: 'a'}).write(new Date() +' error save order: '+  order.id+ '\n');
                else
                    return;
            }
    );
}; */

/* exports.getOrdersFromMercadoLivreSchedule = function(req, res){
    const db = req.db;
    const mercadolivreCredentials = req.mercadolivreCredentials;
    const ownerId = req.decoded._id;
    agenda.define('outro', (o) => {
        let data = o.attrs.data;
        console.log('ts',data.variable);

        //process.exit(0);
      });
      agenda.on('ready', function() {
        agenda.every('2 minutes', 'outro',{variable:'teste'});
        agenda.start();
      });
    db.collection('plataform').findOne({plataform: 'mercadolivre', owner: ownerId},  function(err, plataform) {
        if(!err){
            console.log('entrou');
            agenda.define('teste', (o) => {
                let data = o.attrs.data;
                console.log('ts',data.variable);

                //process.exit(0);
              });
              agenda.on('ready', function() {
                agenda.every('2 minutes', 'teste',{variable:'teste'});
                agenda.start();
              });
              res.send('ok');

        }
        else
            res.send('erro');
    });

} */

/* exports.getShippingML = function(req, res){
    let db = req.db;
    let mercadolivreCredentials = req.mercadolivreCredentials;
    const ownerId = req.decoded._id;
    console.log('entrou');
    db.collection('plataform').findOne({plataform: 'mercadolivre', owner: ownerId},  function(err, plataform) {
        if (err) {
            res.status(400).send(err);
        }else {
            let meliObject = new meli.Meli(mercadolivreCredentials.clientId, mercadolivreCredentials.clientSecret, plataform.settings.accessToken,plataform.settings.refreshToken);
            let sellerId = plataform.settings.seller;
            let teste = 26921773351;
            meliObject.get(`/shipments/${teste}`,{},
                function(err, result){
                    if(err)
                        res.json(err);
                    else{
                        res.json(result);
                    }

                }
            );
         }
    });
}; */
