exports.formatOrder = function (order) {
  let itemsCurrent = order.orderItems.map((itemCurrent) => {
    return {
      sku: itemCurrent.sku,
      original_price: itemCurrent.price,
      special_price: itemCurrent.specialPrice,
      qty: itemCurrent.qty,
    };
  });

  let formatedOrder = {
    status: order.status.value,
    customer_firstname: order.buyer.firstName,
    customer_lastname: order.buyer.lastName,
    customer_email: order.buyer.email,
    customer_dob: "",
    hobox_code: order.plataform + "-" + order.id,
    shipping_description: order.shippingMethod,
    shipping_amount: order.shippingCost,
    shipping_seller_cost: order.sellerShippingCost,
    billing_address: {
      firstname: order.buyer.firstName,
      lastname: order.buyer.lastName.trim()
        ? order.buyer.lastName
        : "Não informado",
      company: "",
      vat_number: order.buyer.vatNumber,
      street: order.billingAddress.street,
      number: order.billingAddress.number,
      complement: order.billingAddress.complement,
      neighborhood: order.billingAddress.neighborhood,
      city: order.billingAddress.city,
      region: order.billingAddress.region.id.replace("BR-", ""),
      postcode: order.billingAddress.postcode,
      country_id: order.billingAddress.country.id,
      telephone:
        order.billingAddress.phone.trim() &&
        order.billingAddress.phone.trim().length > 8
          ? order.billingAddress.phone
          : "99 99999999",
      secondary_phone: "",
      fax: "",
    },
    shipping_address: {
      firstname: order.buyer.firstName,
      lastname: order.buyer.lastName.trim()
        ? order.buyer.lastName
        : "Não informado",
      company: "",
      vat_number: order.buyer.vatNumber,
      street: order.shippingAddress.street,
      number: order.shippingAddress.number,
      complement: order.shippingAddress.complement,
      neighborhood: order.shippingAddress.neighborhood,
      city: order.shippingAddress.city,
      region: order.shippingAddress.region.id.replace("BR-", ""),
      postcode: order.shippingAddress.postcode,
      country_id: order.shippingAddress.country.id,
      telephone:
        order.shippingAddress.phone.trim() &&
        order.shippingAddress.phone.trim().length > 8
          ? order.shippingAddress.phone
          : order.billingAddress.phone.trim() &&
            order.billingAddress.phone.trim().length > 8
          ? order.billingAddress.phone
          : "99 99999999",
      secondary_phone: "",
      fax: "",
    },
    total: order.total,
    items: itemsCurrent,
    free_shipping_discount: order.freeShippingDiscount,
    rule_offset_freight: order.ruleOffsetFreight,
  };

  return formatedOrder;
};
