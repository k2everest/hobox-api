const ObjectId = require("mongodb").ObjectId;

module.exports = function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);

  db.collection("plataform").deleteOne({ _id: id, owner: ownerId }, function (
    err,
    plataform
  ) {
    err ? res.status(400).send(err) : res.status(200).send("plataform deleted");
  });
};
