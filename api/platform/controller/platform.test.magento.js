const magento = require("../../../service/magento");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;
  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "magento", owner: ownerId });

  timeout(15000, request())
    .then(function (response) {
      res.json({ message: "connection ok" });
    })
    .catch(function (error) {
      res.status(400).send({ message: "connection fail" });
    });

  async function request(page) {
    return new Promise((resolve, reject) => {
      magento(platform).xmlrpc.auto.integrator.test.ping(async function (
        err,
        result
      ) {
        if (err) {
          return reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function timeout(ms, promise) {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        reject(new Error("timeout"));
      }, ms);
      promise.then(resolve, reject);
    });
  }
};
