const ObjectId = require("mongodb").ObjectId;
const Model = require("./../model");

module.exports = async function (req, res) {
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);
  const ownerId = req.decoded._id;
  const attributesTypesUpdated = req.body.mappedAttributesTypes;
  let response = await Model.setMappedAttributesTypes(
    ownerId,
    id,
    attributesTypesUpdated
  );
  return response.error
    ? res.status(400).send(response)
    : res.status(200).send(response);
};
