const ObjectId = require("mongodb").ObjectId;
const common = require("../../platform/common");

module.exports = async function (req, res) {
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);
  const syncConfig = req.body.syncConfig;
  let result = await common.setSyncConfig(id, syncConfig);
  res.json(result);
};
