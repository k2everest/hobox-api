module.exports = function (req, res) {
  let db = req.db;
  const ObjectId = require("mongodb").ObjectId;
  var result = [];
  const ownerId = req.decoded._id;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);

  db.collection("plataform").findOne({ _id: id, owner: ownerId }, function (
    err,
    plataform
  ) {
    if (err) {
      return res.status(400).send({
        message: err.message,
      });
    } else {
      if (plataform.mappedCategories) {
        console.log(plataform.mappedCategories);
        res.status(200).json(plataform.mappedCategories);
      } else res.status(201).send([]);
    }
  });
};
