module.exports = function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  db.collection("plataform")
    .find({ owner: ownerId }, { plataform: true, syncConfig: true })
    .toArray(function (err, plataforms) {
      if (err) {
        res.status(400).send(err);
      } else {
        let result = plataforms.map(function (platform) {
          return {
            _id: platform._id,
            platform: platform.plataform,
            syncConfig: platform.syncConfig,
          };
        });

        res.status(200).send(result);
      }
    });
};
