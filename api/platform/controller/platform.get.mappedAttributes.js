module.exports = function (req, res) {
  const ObjectId = require("mongodb").ObjectId;
  let db = req.db;
  var result = [];
  const ownerId = req.decoded._id;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);

  db.collection("plataform").findOne({ _id: id, owner: ownerId }, function (
    err,
    plataform
  ) {
    if (err) {
      return res.status(400).send({
        message: err,
      });
    } else {
      if (plataform.mappedAttributes) {
        console.log(plataform.mappedAttributes);
        res.status(200).json(plataform.mappedAttributes);
      } else res.status(200).json([]);
    }
  });
};
