module.exports = function (req, res) {
  let db = req.db;
  const ownerId = req.decoded._id;

  db.collection("plataform")
    .find({ owner: ownerId })
    .toArray(function (err, plataforms) {
      if (err) {
        res.status(400).send(err);
      } else {
        let result = plataforms.map(function (plataform) {
          return {
            _id: plataform._id,
            url: plataform.settings.url
              ? plataform.settings.url
              : plataform.settings.nickname,
            plataform: plataform.plataform,
            connected: plataform.connected,
          };
        });
        res.status(200).send(result);
      }
    });
};
