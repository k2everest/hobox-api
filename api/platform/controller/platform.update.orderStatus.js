module.exports = function (req, res) {
  const db = req.db;
  const id = JSON.parse(req.params.id);
  const ownerId = req.decoded._id;
  const orderStatusUpdated = req.body.mappedStatus;

  db.collection("plataform").update(
    { _id: id, owner: ownerId },
    {
      $set: { mappedOrderStatus: orderStatusUpdated },
    },
    function (err, result) {
      if (err) {
        res.status(400).send(err);
      } else res.status(200).send("updated");
    }
  );
};
