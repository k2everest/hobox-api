const common = require("../../platform/common");
const data = require("../data");
const events = require("../../../service/event");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  const handleSettings = (settings) => {
    const newSettings = settings;
    newSettings.url_api = settings.url_api.match(/^\//)
      ? settings.url_api
      : "/" + settings.url_api;
    newSettings.store = settings.store ? settings.store : null;
    return newSettings;
  };

  const handlePlatform = (platform) => {
    const platformData = platform;
    platformData.settings = handleSettings(platform.settings);
    return platformData;
  };

  const isValidSettings = (settings) => {
    return (
      settings.url && settings.url_api && settings.username && settings.password
    );
  };

  const plataform = {
    owner: ownerId,
    plataform: req.body.plataform.plataform,
    settings: req.body.plataform.settings,
    port: 443,
  };

  if (isValidSettings(plataform.settings)) {
    try {
      let result = await db
        .collection("plataform")
        .updateOneAsync(
          { owner: ownerId, plataform: "magento" },
          { $set: handlePlatform(plataform) },
          { upsert: true }
        );
      if (result.result.upserted) {
        setSyncConfigDefault(result.result.upserted[0]._id);
      }
      events.emit("onAddNewMagentoPlatform", { owner: ownerId });
      return res.json({ message: "dados salvos" });
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  } else {
    res.status(400).json({
      status: "fail",
      message: "dados incompletos",
    });
  }
};

function setSyncConfigDefault(_idPlatform) {
  common.setSyncConfig(_idPlatform, data.syncConfigDefault);
}
