const ObjectId = require("mongodb").ObjectId;

module.exports = async function (req, res) {
  let db = req.db;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);
  const ownerId = req.decoded._id;
  console.log("req", req.body);
  let mappedCategoriesUpdated = req.body.mappedCategories;
  console.log(mappedCategoriesUpdated.hobox);

  await db.collection("plataform").updateAsync(
    { _id: id, owner: ownerId },
    {
      $pull: {
        mappedCategories: {
          hobox: mappedCategoriesUpdated.hobox,
        },
      },
    }
  );

  if (mappedCategoriesUpdated.marketplace == undefined) res.json("updated");
  else
    db.collection("plataform").update(
      { _id: id, owner: ownerId },
      {
        $addToSet: {
          mappedCategories: mappedCategoriesUpdated,
        },
      },
      function (err, result) {
        if (err) res.send(err);
        else res.json("updated");
      }
    );
};
