const ObjectId = require("mongodb").ObjectId;

module.exports = function (req, res) {
  let db = req.db;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);
  const ownerId = req.decoded._id;

  db.collection("plataform").findOne({ _id: id, owner: ownerId }, function (
    err,
    plataform
  ) {
    err ? res.status(400).send(err) : res.status(200).send(plataform);
  });
};
