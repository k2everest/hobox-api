const ObjectId = require("mongodb").ObjectId;

module.exports = async function (req, res) {
  let db = req.db;
  const id =
    req.params.id.length === 24
      ? ObjectId(req.params.id)
      : JSON.parse(req.params.id);
  const ownerId = req.decoded._id;
  const attributesUpdated = req.body.mappedAttributes;
  console.log(attributesUpdated);
  try {
    await Promise.all(
      attributesUpdated.map(async (item) => await updateAttributeMapped(item))
    );
    res.status(200).send("updated");
  } catch (err) {
    res.status(400).send(err);
  }

  async function updateAttributeMapped(item) {
    let updated = await db.collection("plataform").updateOneAsync(
      {
        _id: id,
        owner: ownerId,
        "mappedAttributes.marketplaceName": item.marketplaceName,
      },
      {
        $set: { "mappedAttributes.$": item },
        //$addToSet:{ mappedAttributes: { $each: attributesUpdated } }
      },
      { new: true }
    );
    const updatedTotal = updated.result.nModified;
    if (updatedTotal == 0) {
      return await db.collection("plataform").updateOneAsync(
        { _id: id, owner: ownerId },
        {
          $addToSet: { mappedAttributes: item },
        }
      );
    }
  }
};
