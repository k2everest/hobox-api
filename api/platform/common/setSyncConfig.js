const db = require("../../../service/db");

module.exports = async function (idPlatform, currentSyncConfig) {
  console.log("inte", idPlatform);
  try {
    await db.collection("plataform").updateAsync(
      {
        _id: idPlatform,
      },
      {
        $set: { syncConfig: currentSyncConfig },
      },
      { upsert: false }
    );

    return { message: "updated" };
  } catch (err) {
    return err;
  }
};
