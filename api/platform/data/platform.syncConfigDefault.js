module.exports = {
  order: {
    send: {
      value: true,
      changeable: true,
    },
    receive: {
      value: false,
      changeable: false,
    },
  },
  product: {
    send: {
      value: false,
      changeable: false,
    },
    receive: {
      value: true,
      changeable: true,
    },
  },
  stock: {
    send: {
      value: false,
      changeable: false,
    },
    receive: {
      value: true,
      changeable: true,
    },
  },
};
