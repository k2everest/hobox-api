var express = require("express");
var router = express.Router();
const settingHandlers = require("./controller");

/**
 * @swagger
 *  definitions:
 *
 *   Plataform:
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       plataform:
 *         type: string
 *       url:
 *         type: string
 *
 *   MagentoSettings:
 *      properties:
 *          url:
 *              type: string
 *          pathApi:
 *              type: string
 *          user:
 *              type: string
 *          password:
 *              type: string
 *          store:
 *              type: string
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/**
 *@swagger
 * /plataforms:
 *   get:
 *     tags:
 *       - Plataforms
 *     description: Returns all plataforms
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of plataforms
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Plataform'
 *
 */
router.get("/", settingHandlers.list);

router.get("/syncConfig", settingHandlers.getSyncConfig);

/**
 *@swagger
 * /plataforms/{id}:
 *   get:
 *     tags:
 *       - Plataforms
 *     description: Returns a single plataform
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: plataforms id
 *         in: path
 *         required: true
 *         schema:
 *           $ref: "#/definitions/Plataform"
 *     responses:
 *       200:
 *         description: a single plataform
 *         schema:
 *              $ref: '#/definitions/Plataform'
 *
 */

//get plataform
router.get("/:id", settingHandlers.get);

/**
 *@swagger
 * /plataforms/magento:
 *   post:
 *     tags:
 *       - Plataforms
 *     description: Allow users add magento plataform
 *     produces:
 *       - application/json
 *     parameters:
 *            -
 *             in: body
 *             name: body
 *             description: Magento Settings
 *             required: true
 *             schema:
 *              $ref: '#/definitions/MagentoSettings'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *              $ref: '#/definitions/Plataform'
 */
//create magento plataform
router.post("/magento/test", settingHandlers.magentoTest);
//create magento plataform
router.post("/magento", settingHandlers.setMagento);

/**
 *@swagger
 * /plataforms/mercadolivre:
 *   post:
 *     tags:
 *       - Plataforms
 *     description: Allow users add mercadolivre plataform
 *     produces:
 *       - application/json
 *     parameters:
 *            -
 *             in: body
 *             name: body
 *             description: Mercado Livre Settings
 *             required: true
 *             schema:
 *              $ref: '#/definitions/MagentoSettings'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *              $ref: '#/definitions/Plataform'
 */

//delete plataform
router.delete("/:id", settingHandlers.del);

//get orders status mapped for mercado livre
router.get("/:id/orderStatus", settingHandlers.getMappedOrderStatus);
//update orders status mapped for mercado livre
router.put("/:id/orderStatus", settingHandlers.updateOrderStatus);

//update config of synchronization of platform
router.put("/:id/syncConfig", settingHandlers.updateSyncConfig);

router.put("/:id/mapped-attributes", settingHandlers.updateMappedAttributes);
router.put("/:id/attributesTypes", settingHandlers.updateMappedAttributesTypes);

router.get("/:id/mapped-attributes", settingHandlers.getMappedAttributes);

//list attributes from Mercado livre
//router.get('/:id/attributes/mercadolivre',settingHandlers.getMappedAttributes);

//router.get('/:id/categories/mercadolivre/:categoryId',categoriesHandlers.getCategoriesML);

//update mapped categories for mercado livre
router.put("/:id/mapped-categories", settingHandlers.updateMappedCategories);
//get mapped categories for mercado livre
router.get("/:id/mapped-categories", settingHandlers.getMappedCategories);

//should be removed
/* router.get('/mercadolivre/:id',settingHandlers.getMercadoLivre);
router.get('/magento/:id',settingHandlers.getMagento); */

module.exports = router;
