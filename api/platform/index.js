const listeners = require("./listeners");
const platform = require("./router");

listeners.run();

module.exports = platform;
