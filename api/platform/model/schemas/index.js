module.exports.attributesTypes = require("./attributesTypes");
module.exports.orderStatus = require("./orderStatus");
module.exports.amazonPlatform = require("./platform").amazon;
module.exports.blingPlatform = require("./platform").bling;
