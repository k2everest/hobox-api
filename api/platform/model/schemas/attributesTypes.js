const Joi = require("joi");

module.exports = Joi.object()
  .keys({
    baseName: Joi.string().required(),
    attributeType: Joi.string().required(),
    value: Joi.string().required(),
  })
  .with("attributeType", "value");
