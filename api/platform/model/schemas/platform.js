const Joi = require("joi");

const amazon = Joi.object().keys({
  sellerId: Joi.string().min(3).required(),
  accessKeyId: Joi.string().min(3).required(),
  secretKey: Joi.string().min(3).required(),
});

const bling = Joi.object().keys({
  apiKey: Joi.string().min(3).required(),
  syncOrdersFrom: Joi.date().required(),
  platforms: Joi.array(),
});

module.exports.amazon = amazon;
module.exports.bling = bling;
