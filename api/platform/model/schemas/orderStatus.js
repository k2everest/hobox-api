const Joi = require("joi");

module.exports = Joi.object()
  .keys({
    hobox: Joi.string().required(),
    status: Joi.string().required(),
  })
  .with("hobox", "status");
