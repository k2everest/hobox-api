const ObjectId = require("mongodb").ObjectId;
const db = require("../../../service/db");
const Platform = db.collection("plataform");
const types = require("./types");
const schemas = require("./schemas");

function validate(mappedAttributesTypes, schema) {
  const result = schema.validate(mappedAttributesTypes);
  return result.error;
}

exports.get = async (ownerId, platformName) => {
  try {
    let result = await Platform.findOneAsync({
      owner: ownerId,
      plataform: platformName,
    });
    return result;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.listByName = async (name) => {
  try {
    let result = await Platform.find({ plataform: name })
      .sort({ _id: 1 })
      .toArrayAsync();
    return result;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.setMappedAttributesTypes = async (
  ownerId,
  platformId,
  mappedAttributesTypes = types.MappedAttributesType
) => {
  let validElements = mappedAttributesTypes.map((obj) =>
    validate(obj, schemas.attributesTypes)
  );
  if (validElements.some((v) => v != null)) {
    return {
      error: true,
      message: validElements,
    };
  }
  try {
    let result = await Platform.updateAsync(
      { owner: ownerId, _id: platformId },
      {
        $set: {
          mappedAttributesTypes: mappedAttributesTypes,
        },
      },
      { upsert: false }
    );
    return result;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.setMappedOrderStatus = async (
  ownerId,
  platformId,
  mappedOrderStatus = types.MappedOrderStatusType
) => {
  let validElements = mappedOrderStatus.map((obj) =>
    validate(obj, schemas.orderStatus)
  );
  if (validElements.some((v) => v != null)) {
    return {
      error: true,
      message: validElements,
    };
  }
  try {
    let result = await Platform.updateAsync(
      { owner: ownerId, _id: platformId },
      {
        $set: {
          mappedOrderStatus: mappedOrderStatus,
        },
      },
      { upsert: false }
    );
    return result;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.setAmazonPlatform = async (ownerId, settings) => {
  const invalidElements = validate(settings, schemas.amazonPlatform);
  if (invalidElements != null)
    return {
      error: true,
      message: invalidElements,
    };

  try {
    let result = await db
      .collection("plataform")
      .updateOneAsync(
        { owner: ownerId, plataform: "amazon" },
        { $set: { settings: settings } },
        { upsert: true }
      );
    if (result.result.upserted) {
      return { message: "created", upserted: result.result.upserted };
    }
    return { message: "updated" };
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.setBlingPlatform = async (ownerId, settings) => {
  const invalidElements = validate(settings, schemas.blingPlatform);

  if (invalidElements == null) {
    try {
      let result = await db
        .collection("plataform")
        .updateOneAsync(
          { owner: ownerId, plataform: "bling" },
          { $set: { settings: settings } },
          { upsert: true }
        );
      if (result.result.upserted) {
        return { message: "created", upserted: result.result.upserted };
      }
      return { message: "updated" };
    } catch (err) {
      return {
        error: true,
        message: err.message,
      };
    }
  } else {
    return {
      error: true,
      message: invalidElements,
    };
  }
};

exports.updateConnectionStatus = async ({ id: id, status: status }) => {
  let platform = await db
    .collection("plataform")
    .updateAsync(
      { _id: id },
      { $set: { connected: status } },
      { upsert: false }
    );

  return;
};
