const MappedAttributesType = [
  {
    baseName: "",
    attributeType: "",
    value: "",
  },
];

const MappedOrderStatusType = [
  {
    hobox: "",
    status: "",
  },
];

exports.MappedAttributesType = MappedAttributesType;
exports.MappedOrderStatusType = MappedOrderStatusType;
