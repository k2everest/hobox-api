var express = require("express");
var router = express.Router();
const orderHandlers = require("./order/listOrders");

//FEED
const feedHandlers = require("./feed/submitFeed");
const feedTest = require("./feed/product");
const feedProductImage = require("./feed/productImage");
const feedInventory = require("./feed/inventory");
const feedPrice = require("./feed/price");

const getFeedList = require("./feed/getFeedSubmissionList");
const getFeedResult = require("./feed/getFeedSubmissionResult");
const getReports = require("./reports/requestReport");
const getReportsId = require("./reports/getReport");
const getReportList = require("./reports/getReportList");
const feedCancel = require("./feed/cancelFeedSubmissions");

const getProduct = require("./product/getMatchingProduct");
const getCategories = require("./category/getCategories");
const platformSave = require("./platform/save");
const platformStatus = require("./platform/getServiceStatus");
const getAttributes = require("./attribute/getAttributes");
const getTypes = require("./type/getTypes");
const getMappedTypes = require("./platform/getMappedAttributesTypes");

router.post("/platform", platformSave);
router.get("/platform/status", platformStatus);
router.post("/", orderHandlers);
router.post("/feed", feedHandlers);
router.post("/feed/test", feedTest);
router.post("/feed/image", feedProductImage);
router.post("/feed/inventory", feedInventory);
router.post("/feed/price", feedPrice);

router.get("/feed/cancel/:id", feedCancel);
router.get("/feed/:id", getFeedResult);
router.get("/feed", getFeedList);
router.get("/reports", getReports);
router.get("/reports/list", getReportList);
router.get("/reports/:id", getReportsId);
router.get("/product", getProduct);
router.get("/categories", getCategories);
router.get("/attributes", getAttributes);
router.get("/types", getTypes.request);
router.get("/mappedTypes", getMappedTypes);

module.exports = router;
