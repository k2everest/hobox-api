const syncConfig = require("./config/syncConfig");
const common = require("../../platform/common");
const Model = require("../../platform/model");
const data = require("./data");

module.exports = async function (req, res) {
  const ownerId = req.decoded._id;
  let result = await Model.setAmazonPlatform(
    ownerId,
    req.body.platform.settings
  );
  if (result.error) return res.status(400).json(result);
  else {
    if (result.upserted) {
      const _idPlatform = result.upserted[0]._id;
      common.setSyncConfig(_idPlatform, syncConfig);
      await Model.setMappedAttributesTypes(
        ownerId,
        _idPlatform,
        data.mappedAttributesTypesDefault
      );
      await Model.setMappedOrderStatus(
        ownerId,
        _idPlatform,
        data.mappedOrderStatusDefault
      );
    }
    return res.json(result);
  }
};
