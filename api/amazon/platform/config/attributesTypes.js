const types = require("../../type/getTypes");

let attributeValuesDefault = [
  {
    value: "USD",
    attributeName: "CurrencyAmount",
  },
];

let customMapped = [
  {
    value: "BRL",
    attributeName: "CurrencyAmount",
  },
];

module.exports = async () => {
  let attributesTypes = await types.getTypes();

  let mappedAttributesTypes = attributesTypes.map((a) => {
    a.value = getCustomValue(a) ? getCustomValue(a) : getDefaultValue(a);
    return a;
  });
  return mappedAttributesTypes;

  function getDefaultValue(a) {
    let selectedValue = attributeValuesDefault.filter(
      (defaultValue) => a.attributeType == defaultValue.attributeName
    );
    return selectedValue.length > 0 ? selectedValue[0].value : null;
  }

  function getCustomValue(a) {
    let selectedValue = customMapped.filter(
      (customValue) => a.attributeType == customValue.attributeName
    );
    return selectedValue.length > 0 ? selectedValue[0].value : null;
  }
};
