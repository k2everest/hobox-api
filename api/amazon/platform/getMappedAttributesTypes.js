const attributesTypes = require("./config/attributesTypes");

module.exports = async (req, res) => {
  let response = await attributesTypes();
  return res.json(response);
};
