var fs = require("fs");
const xml2js = require("xml2js");
var parser = new xml2js.Parser();
let Promise = require("bluebird");

let amazonBaseFileContent = null;
let types = null;

Promise.promisifyAll(fs);
Promise.promisifyAll(parser);

const handleAttributes = (attribute) => {
  if (isComplexTypeAttribute(attribute))
    return parseComplexTypeToArray(attribute);
};

const parseComplexTypeToArray = (att) => {
  let name = att["$"]["name"];
  let basePath =
    att["xsd:simpleContent"][0]["xsd:extension"][0]["xsd:attribute"][0]["$"];
  let currentType = basePath.type;
  let currentOptions = null;
  let type = types.filter((att) => att["$"]["name"] == currentType);
  if (type.length > 0)
    currentOptions = type[0]["xsd:restriction"]
      ? type[0]["xsd:restriction"][0]["xsd:enumeration"]
      : null;
  currentOptions =
    currentOptions && currentOptions.length > 0
      ? currentOptions.map((a) => a["$"])
      : null;
  let complexAttribute = {
    base: basePath,
    attributeType: name,
    options: currentOptions,
  };

  return complexAttribute;
};

const isComplexTypeAttribute = (attribute) => {
  let basePathType = attribute["xsd:simpleContent"];
  return basePathType ? true : false;
};

const readFileAsync = async (pathFile) => {
  let fileContent = await fs.readFileAsync(pathFile);
  let jsonContent = await parser.parseStringAsync(fileContent);
  return jsonContent;
};

const getListAttributes = (jsonContent) => {
  let baseNode = jsonContent["xsd:schema"]["xsd:complexType"];
  return baseNode;
};

const getSimpleTypes = (jsonContent) => {
  let baseNode = jsonContent["xsd:schema"]["xsd:simpleType"];
  return baseNode;
};

const getHandledTypes = async () => {
  const amazonBaseFilePath = "./api/amazon/data/Core/amzn-base.xsd";
  let jsonContent = await readFileAsync(amazonBaseFilePath);
  types = getSimpleTypes(jsonContent);
  let attributes = getListAttributes(jsonContent);
  let parsedAttributes = await attributes.map(handleAttributes);
  return parsedAttributes.filter((a) => a && a.options);
};

const request = async (req, res) => {
  let response = await getHandledTypes();
  return res.json(response);
};
exports.getTypes = getHandledTypes;

//esse request nao vai existir
exports.request = request;
