const parser = require("./parser/inventory");
const Product = require("../../product/model");
const Platform = require("../../platform/model");
var amazonMws = require("../../../service/amazon").mws;
var fse = require("fs-extra");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  const selectedProductsSkus = ["252", "125"];
  let platform = await Platform.get(ownerId, "amazon");
  if (!platform) return res.send("platform is not exist");
  let productsForSend = await Product.findByOwnerAndSkus(
    ownerId,
    selectedProductsSkus
  );
  const sellerId = platform.settings.sellerId;
  const inventoryParsed = parser.inventoryToAmazon(productsForSend, sellerId);
  fse.writeFileSync("api/amazon/feed/inventory.txt", inventoryParsed, "UTF-8");

  amazonMws.feeds.submit(
    {
      Version: "2009-01-01",
      Action: "SubmitFeed",
      FeedType: "_POST_INVENTORY_AVAILABILITY_DATA_",
      FeedContent: inventoryParsed,
      SellerId: sellerId,
      MWSAuthToken: "amzn.mws.d8af40d1-e5f2-8de7-2e86-cd52482685f4",
    },
    function (error, response) {
      if (error) {
        return res.json(error);
      }
      return res.json(response);
    }
  );
};
