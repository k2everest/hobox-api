function mapPictures(item) {
  let imagesArray = [];
  let isMain = true;
  imagesArray = item.images.map((img, i) => {
    if (isMain == true) {
      isMain = false;
      return {
        SKU: item.sku,
        ImageType: "Main",
        ImageLocation: img,
      };
    } else
      return {
        SKU: item.sku,
        ImageType: "PT" + i,
        ImageLocation: img,
      };
  });

  return imagesArray;
}

function jsonToXml(mappedImages) {
  let xmlContent = "";
  mappedImages.map((elem, i) => {
    xmlContent += "<Message>";
    xmlContent += "<MessageID>" + (i + 1) + "</MessageID>";
    xmlContent += "<OperationType>Update</OperationType>";
    xmlContent += "<ProductImage>";
    xmlContent += "<SKU>" + elem.SKU + "</SKU>";
    xmlContent += "<ImageType>" + elem.ImageType + "</ImageType>";
    xmlContent += "<ImageLocation>" + elem.ImageLocation + "</ImageLocation>";
    xmlContent += "</ProductImage>";
    xmlContent += "</Message>";
    xmlContent += "\n";
  });
  return xmlContent;
}

const Header = (sellerId) => {
  return `<?xml version="1.0" encoding="utf-8"?>
  <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
      <Header>
          <DocumentVersion>1.01</DocumentVersion>
          <MerchantIdentifier>${sellerId}</MerchantIdentifier>
      </Header>
      <MessageType>ProductImage</MessageType>
      \n`;
};

const Footer = () => {
  return `</AmazonEnvelope>`;
};

exports.imageToAmazon = (products, sellerId) => {
  let mappedImages = products.map((item) => mapPictures(item));
  const content = jsonToXml(mappedImages.flat());
  console.log(content);
  return Header(sellerId) + content + Footer();
};
