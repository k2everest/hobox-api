function jsonToXml(products) {
  let xmlContent = "";
  products.map((elem, i) => {
    xmlContent += "<Message>";
    xmlContent += "<MessageID>" + (i + 1) + "</MessageID>";
    xmlContent += "<OperationType>Update</OperationType>";
    xmlContent += "<Inventory>";
    xmlContent += "<SKU>" + elem.sku + "</SKU>";
    xmlContent += "<Quantity>" + elem.qty + "</Quantity>";
    xmlContent += "</Inventory>";
    xmlContent += "</Message>";
    xmlContent += "\n";
  });
  return xmlContent;
}

const Header = (sellerId) => {
  return `<?xml version="1.0" encoding="utf-8"?>
  <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
      <Header>
          <DocumentVersion>1.01</DocumentVersion>
          <MerchantIdentifier>${sellerId}</MerchantIdentifier>
      </Header>
      <MessageType>Inventory</MessageType>
      \n`;
};

const Footer = () => {
  return `</AmazonEnvelope>`;
};

exports.inventoryToAmazon = (products, sellerId) => {
  const content = jsonToXml(products);
  console.log(content);
  return Header(sellerId) + content + Footer();
};
