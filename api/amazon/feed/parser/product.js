const defaultFields = [
  "name",
  "description",
  "qty",
  "sku",
  "weight",
  "price",
  "status",
];

//return options
const mapOptions = (options, value) => {
  return options.reduce(
    (option, current) =>
      current.value == value ? (option = current.ml) : option,
    ""
  );
};

const getAttributeValue = (mappedAt, attributes) => {
  let value = attributes.reduce(
    (acum, current) =>
      current.key == mappedAt.hoboxName ? (acum = current.value) : acum,
    ""
  );
  if (mappedAt.options) {
    value = mapOptions(mappedAt.options, value);
  }
  return value;
};

const getBaseAttribute = (mappedAt, attributesTypes) => {
  let base = attributesTypes.filter(
    (type) => type.attributeType == mappedAt.type
  );
  return base.length > 0
    ? {
        name: base[0].baseName,
        value: base[0].value,
      }
    : null;
};

const mapCategoriesToAmazon = (item, mappedCategories) => {
  let reducer = function (cat, current) {
    for (let i = 0; i < item.categories.length; i++) {
      if (item.categories[i].id == current.hobox) cat = current.marketplace;
    }
    return cat;
  };
  let categoryId = mappedCategories
    ? mappedCategories.reduce(reducer, null)
    : null;
  return {
    name: "ProductData",
    value: categoryId,
  };
};

const mapItemforMarketplace = ({
  item,
  mappedAttributes,
  mappedCategories,
  mappedAttributesTypes,
}) => {
  //console.log(mappedAttributes); //verificar estrutura de atributos
  //console.log(mappedAttributesTypes);
  let itemCategory = mapCategoriesToAmazon(item, mappedCategories);
  let itemMapped = mappedAttributes.map((mappedAt) => {
    if (defaultFields.includes(mappedAt.hoboxName)) {
      return {
        name: mappedAt.marketplaceName,
        value: mappedAt.options
          ? mapOptions(mappedAt.options, item[mappedAt.hoboxName])
          : item[mappedAt.hoboxName],
      };
    } else
      return {
        name: mappedAt.marketplaceName,
        value: getAttributeValue(mappedAt, item.attributes),
        base: getBaseAttribute(mappedAt, mappedAttributesTypes),
      };
  });

  /*   for (let elem in itemsMapped){
      if(itemsMapped[elem]['value'].length > 0)
        return
    } */
  itemMapped = itemMapped.filter((e) => e.value.length > 0);
  itemMapped.splice(1, 0, { name: "StandardProductID_Type", value: "EAN" }); //define type)
  itemMapped.push(itemCategory); //add category mapped
  //itemId ? itemMapped.push(itemId) : null; //add id of product if exist on mercado livre
  //console.log('mapped',itemMapped);
  return itemMapped;
};

const writeComplexElement = (complexElement, value, base) => {
  let content = "\n";
  for (let j = 1; j < complexElement.length; j++) {
    content =
      content +
      `\t<${complexElement[j]} ${
        j == complexElement.length - 1 ? getBaseValueForXml(base) : ""
      }>`;
  }
  content = content + value;
  for (let j = complexElement.length - 1; j > 0; j--) {
    content = content + `</${complexElement[j]}>`;
  }
  return content;
};

const Header = () => {
  return `<?xml version="1.0" encoding="utf-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>A1OJQ1C2S6V5S4</MerchantIdentifier>
    </Header>
    <MessageType>Product</MessageType>
    <PurgeAndReplace>false</PurgeAndReplace>\n`;
};

const Footer = () => {
  return `</AmazonEnvelope>`;
};

const headerMessage = (id) => {
  return `<Message> <MessageID>${id}</MessageID>
      <OperationType>Update</OperationType>
      <Product>\n`;
};

const footerMessage = () => {
  return ` </Product>\n </Message>`;
};

const convertCategoryToXml = (value = []) => {
  if (value == null) return;
  let categoryContent = "\n";
  let categories = value.split("/");
  categories.push(categories[categories.length - 1]);
  categories[categories.length - 2] = "ProductType";
  for (let i = 0; i < categories.length; i++) {
    categoryContent = categoryContent + `<${categories[i]}>\n`;
  }
  for (let i = categories.length - 1; i >= 0; i--) {
    categoryContent = categoryContent + `</${categories[i]}>\n`;
  }
  return categoryContent;
};

const getBaseValueForXml = (base) => {
  return base != null ? `${base.name}="${base.value}"` : "";
};

const jsonToXml = (itemsJson) => {
  let contentXml = [];
  for (let i = 0; i < itemsJson.length; i++) {
    let parents = [];
    let complexElements = [];
    let complexElementsValues = [];
    let complexElementsBase = [];

    contentXml = contentXml + headerMessage(i + 1);

    for (let elem in itemsJson[i]) {
      let childElement = itemsJson[i][elem]["name"].split("_");
      if (childElement.length > 1) {
        complexElements.push(childElement);
        complexElementsValues.push(itemsJson[i][elem]["value"]);
        complexElementsBase.push(itemsJson[i][elem]["base"]);
        if (!parents.includes(childElement[0])) parents.push(childElement[0]);
      }
      /* }else{
        if(itemsJson[i][elem]['name']=="ProductData")
          contentXml = contentXml + `<${itemsJson[i][elem]['name']}>${convertCategoryToXml(itemsJson[i][elem]['value'])}</${itemsJson[i][elem]['name']}>\n`;
        else
          contentXml = contentXml + `<${itemsJson[i][elem]['name']}> ${itemsJson[i][elem]['value']} </${itemsJson[i][elem]['name']}>\n`;
      } */
    }

    for (let elem in itemsJson[i]) {
      let isShouldSkip = false;
      let childElement = itemsJson[i][elem]["name"].split("_");
      for (let p = 0; p < parents.length; p++) {
        if (childElement[0] == parents[p]) {
          contentXml = contentXml + `<${parents[p]}>`;
          for (let c = 0; c < complexElements.length; c++) {
            if (parents[p] == complexElements[c][0])
              contentXml =
                contentXml +
                writeComplexElement(
                  complexElements[c],
                  complexElementsValues[c],
                  complexElementsBase[c]
                );
          }
          contentXml = contentXml + `\n</${parents[p]}>\n`;
          isShouldSkip = true;
          parents.splice(p, 1);
          continue;
        }
      }
      if (isShouldSkip == true || childElement.length > 1) continue;

      if (childElement[0] == "ProductData")
        contentXml =
          contentXml +
          `<${itemsJson[i][elem]["name"]}>${convertCategoryToXml(
            itemsJson[i][elem]["value"]
          )}</${itemsJson[i][elem]["name"]}>\n`;
      else
        contentXml =
          contentXml +
          `<${itemsJson[i][elem]["name"]} ${getBaseValueForXml(
            itemsJson[i][elem]["base"]
          )}>${itemsJson[i][elem]["value"]}</${itemsJson[i][elem]["name"]}>\n`;

      console.log("xml", itemsJson[i][elem]);
    }

    contentXml = contentXml + footerMessage();
  }
  return Header() + contentXml + Footer();
};

exports.itemToAmazon = ({
  products = [],
  mappedAttributesMarketplace = [],
  mappedCategories = [],
  mappedAttributesTypes = [],
  configuration = null,
}) => {
  const parameters = (product) => {
    return {
      item: product,
      mappedAttributes: mappedAttributesMarketplace,
      mappedCategories: mappedCategories,
      mappedAttributesTypes: mappedAttributesTypes,
    };
  };
  let itemsMapped = products.map((item) =>
    mapItemforMarketplace(parameters(item))
  );
  //console.dir(itemsMapped,{ depth: null});
  return jsonToXml(itemsMapped);
};
