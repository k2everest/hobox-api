function jsonToXml(products) {
  let xmlContent = "";
  products.map((elem, i) => {
    xmlContent += "<Message>";
    xmlContent += "<MessageID>" + (i + 1) + "</MessageID>";
    xmlContent += "<OperationType>Update</OperationType>";
    xmlContent += "<Price>";
    xmlContent += "<SKU>" + elem.sku + "</SKU>";
    xmlContent +=
      "<StandardPrice currency='BRL'>" + elem.price + "</StandardPrice>";
    xmlContent += "</Price>";
    xmlContent += "</Message>";
    xmlContent += "\n";
  });
  return xmlContent;
}

const Header = (sellerId) => {
  return `<?xml version="1.0" encoding="utf-8"?>
  <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
      <Header>
          <DocumentVersion>1.01</DocumentVersion>
          <MerchantIdentifier>${sellerId}</MerchantIdentifier>
      </Header>
      <MessageType>Price</MessageType>
      \n`;
};

const Footer = () => {
  return `</AmazonEnvelope>`;
};

exports.priceToAmazon = (products, sellerId) => {
  const content = jsonToXml(products);
  return Header(sellerId) + content + Footer();
};
