const mercadolivre = require("../../../service/mercadolivre");

module.exports = async (req, res) => {
  const categoryId = req.params.id;
  let meliObject = await mercadolivre.meli();

  if (!!categoryId && categoryId != 0)
    meliObject.get(`/categories/${categoryId}`, function (err, result) {
      if (err) console.log(err);
      else {
        return res.json(result);
      }
    });
  else
    meliObject.get("/sites/MLB/categories", function (err, result) {
      if (err) console.log(err);
      else {
        res.json(result);
      }
    });
};
