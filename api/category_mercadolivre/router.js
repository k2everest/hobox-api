var express = require("express");
var router = express.Router();
const handlers = require("./controller");

//list categories from Mercado livre
router.get("/:id", handlers.get);

module.exports = router;
