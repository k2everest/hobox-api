const defaultFields = [
  "name",
  "description",
  "qty",
  "sku",
  "weight",
  "price",
  "status",
];

exports.itemToMercadolivre = function (
  products,
  mappedAttributesMarketplace,
  mappedCategories,
  configuration = null
) {
  let itemsMapped = products.map((item) =>
    mapItemforMarketplace(item, mappedAttributesMarketplace, mappedCategories)
  );
  itemsMapped = convertToMercadolivreFormat(itemsMapped);
  itemsMapped = !!configuration
    ? setConfiguration(itemsMapped, configuration)
    : itemsMapped;
  itemsMapped = clearItems(itemsMapped);
  itemsMapped = handleVariations(itemsMapped);
  return itemsMapped;
};

function handleVariations(itemsMapped) {
  return itemsMapped.map((e) => {
    if (e.variations) {
      e.variations = mapVariations(e);
    }
    return e;
  });
}

function mapVariations(item) {
  const variations = item.variations;
  let result = convertToMercadolivreFormat(variations);

  result = result
    .map(deleteEmptyAttributes)
    .map(handleAttributes)
    .map(mapAttributeForVariation);
  result = result.map((i) => {
    i.price = item.price;
    return i;
  });
  return result;
}

function mapAttributeForVariation(itemVariation) {
  itemVariation.attribute_combinations = itemVariation.attributes.filter((e) =>
    itemVariation.variationAttributes.includes(e.id.toLowerCase())
  );
  delete itemVariation["attributes"];
  delete itemVariation["variationAttributes"];
  return itemVariation;
}

function setConfiguration(items, configuration) {
  let itemsMapped = items.map((item) => {
    item.shipping = {
      mode: configuration.shippingMode,
      free_methods:
        configuration.shippingFree != false
          ? [
            {
              id: configuration.shippingFree,
              rule: {
                free_mode: "country",
                value: null,
              },
            },
          ]
          : null,
    };
    item.listing_type_id = configuration.listingType;

    return item;
  });
  return itemsMapped;
}

function convertToMercadolivreFormat(items) {
  let mappedItems = items.map((arrayItem) => {
    let item = arrayItem.reduce((obj, item) => {
      obj[item.key] = item.value;
      return obj;
    }, {});
    return item;
  });
  return mappedItems;
}

function mapAllAttributes(item, mappedAttributes) {
  let result = mappedAttributes.map((mappedAt) => {
    if (defaultFields.includes(mappedAt.hoboxName)) {
      return {
        key: mappedAt.marketplaceName,
        value: mappedAt.options
          ? mapOptions(mappedAt.options, item[mappedAt.hoboxName])
          : item[mappedAt.hoboxName],
      };
    } else
      return {
        key: mappedAt.marketplaceName,
        value: mappingAttributes(mappedAt, item.attributes),
      };
  });

  return result;
}

//return item mapped for marketplace
function mapItemforMarketplace(item, mappedAttributes, mappedCategories) {
  let categoryMapped = mapCategories(item, mappedCategories);
  let picturesMapped = mapPictures(item);
  let itemId = getItemId(item);
  let variations = getVariations(item);
  let variationAttributes = getVariationAttributes(item);
  if (variations)
    variations.value = variations.value.map((variation) => {
      let result = mapAllAttributes(variation, mappedAttributes);
      result.push(mapPicturesForVariation(variation));
      result.push(variationAttributes);
      return result;
    });
  let itemMapped = mapAllAttributes(item, mappedAttributes);
  itemMapped.push(categoryMapped); //add category mapped
  itemMapped.push(picturesMapped); //add pictures mapped
  itemId ? itemMapped.push(itemId) : null; //add id of product if exist on mercado livre
  variations ? itemMapped.push(variations) : null;
  return itemMapped;
}

//return value of attribute
mappingAttributes = (mappedAt, attributes) => {
  let value = attributes.reduce(
    (acum, current) =>
      current.key == mappedAt.hoboxName ? (acum = current.value) : acum,
    ""
  );
  if (mappedAt.options) {
    value = mapOptions(mappedAt.options, value);
  }
  return value;
};

function mapOptions(options, value) {
  return options.reduce(
    (option, current) =>
      current.value == value ? (option = current.ml) : option,
    ""
  );
}

function mapPictures(item) {
  let imagesArray = [];
  imagesArray = item.images.map((img) => {
    return { source: img };
  });
  return {
    key: "pictures",
    value: imagesArray,
  };
}

function mapPicturesForVariation(item) {
  let imagesArray = [];
  imagesArray = item.images.map((img) => {
    return img;
  });
  return {
    key: "picture_ids",
    value: imagesArray,
  };
}

function getItemId(item) {
  if (item.marketplaces && item.marketplaces.mercadolivre)
    return {
      key: "id",
      value: item.marketplaces.mercadolivre.id,
    };
}

function getVariations(item) {
  if (Array.isArray(item.variations))
    return {
      key: "variations",
      value: item.variations,
    };
}

function getVariationAttributes(item) {
  if (Array.isArray(item.variation_attributes))
    return {
      key: "variationAttributes",
      value: item.variation_attributes,
    };
}

function mapCategories(item, mappedCategories) {
  categoryDefault = "";
  let reducer = function (cat, current) {
    for (let i = 0; i < item.categories.length; i++) {
      if (item.categories[i].id == current.hobox) cat = current.marketplace;
    }
    return cat;
  };
  let categoryId = mappedCategories
    ? mappedCategories.reduce(reducer, categoryDefault)
    : categoryDefault;
  return {
    key: "category_id",
    value: categoryId,
  };
}

function clearItems(items) {
  return items.map((item) => {
    if (!item.shipping) item.shipping = {};

    if (
      item["dimensions.height"] &&
      item["dimensions.width"] &&
      item["dimensions.length"] &&
      item.weight
    )
      item.shipping.dimensions = `${item["dimensions.height"]}x${item["dimensions.height"]}x${item["dimensions.length"]},${item.weight}`;

    delete item["dimensions.height"];
    delete item["dimensions.width"];
    delete item["dimensions.length"];
    delete item.weight;
    delete item.promotional_price;
    item["currency_id"] = "BRL";
    item["description"] = {
      plain_text: item.description
        .replace(/<br\s*[\/]?>/gi, "\n")
        .replace(/<[^>]*>?/gm, ""),
    };

    if (item['sale_terms.WARRANTY_TIME']) {
      item.sale_terms = [
        {
          "id": "WARRANTY_TYPE",
          value_id: item['sale_terms.WARRANTY_TYPE'] ? item['sale_terms.WARRANTY_TYPE'] : '2230280'

        }, {
          "id": "WARRANTY_TIME",
          value_name: item['sale_terms.WARRANTY_TIME']
        }
      ];

      delete item['sale_terms.WARRANTY_TYPE'];
      delete item['sale_terms.WARRANTY_TIME'];
    }

    item = deleteEmptyAttributes(item);
    item = handleAttributes(item);
    return item;
  });
}

function deleteEmptyAttributes(item) {
  let result = item;
  for (t in result) {
    if (result[t] == "" || result[t] == undefined) delete result[t];
  }

  return result;
}

function handleAttributes(item) {
  let attributes = [];
  for (t in item) {
    if (t.includes("attributes")) {
      let splited = t.split(".");
      let attributeName = "";
      if (splited.length <= 2) {
        attributeName = splited[1];
        attributes.push({ id: attributeName, value_name: item[t] });
      }
      delete item[t];
    }
  }
  item.attributes = attributes;
  return item;
}
