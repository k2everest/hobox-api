const Meli = require("../../../service/mercadolivre");
const parser = require("../parser");
const common = require("./");
const helpers = require("./helpers");
const Product = require("../../product/model");
const logger = require("../../../service/winston");
const syncLog = require("../../../service/syncLog");

module.exports = async function (platform, cb) {
  let meliObject = undefined;

  if (helpers.isAllowedUpdateStockProductMercadolivre(platform)) {
    try {
      common.sync(platform, async (err, res) => {
        err ? cb(err, null) : await run();
      });
    } catch (err) {
      cb(err.message, null);
    }
  } else {
    cb("Verifique as configurações de sincronização", null);
  }

  const asyncUpdateStockAndStatus = async (item) => {
    await updateStockItem(item);
    return await updateStatusItem(item);
  };

  async function run() {
    meliObject = await Meli.meli(
      platform.settings.accessToken,
      platform.settings.refreshToken
    );
    let products = await Product.getProductsForUpdate(platform);
    let mappedProducts = parser.itemToMercadolivre(
      products,
      platform.mappedAttributes,
      platform.mappedCategories
    );
    await sendApi(mappedProducts);
  }

  async function sendApi(items) {
    //const delay = ms => new Promise(res => setTimeout(res, ms));
    const itemsLength = items.length;
    for (let i = 0; i < itemsLength; i++) {
      // const requests = items.slice(i, i + 2).map((item) => {
      //   return asyncUpdateStockAndStatus(item);
      // });
      //await delay(3000);
      await asyncUpdateStockAndStatus(items[i]);
      console.log(i);
      //await Promise.all(requests);
    }
    common.sync(platform, async (err, res) => cb(null, 'ok'));
  }

  async function getVariationItem(itemId) {
    let itemCurrent = await meliObject.getAsync(`/items/${itemId}`, {});
    if (itemCurrent.variations.length > 0) return itemCurrent.variations;
    return null;
  }

  function enrichVariation(aItem, variations) {
    let result = variations.map((v) => {
      if (aItem.variations) {
        const currentElem = aItem.variations.reduce(
          (acum, elem) =>
            elem.seller_custom_field == v.seller_custom_field
              ? (acum = elem)
              : acum,
          null
        );
        return {
          available_quantity: currentElem.available_quantity,
          id: v.id,
        };
      } else {
        return {
          available_quantity: aItem.available_quantity,
          id: v.id,
        };
      }
    });

    return result;
  }

  async function updateStockItem(item) {
    const stock = item.available_quantity;
    const id = item.id;
    let variations = await getVariationItem(id);
    let resultUpdateStockItem = null;
    if (variations != null) {
      let variationsContent = enrichVariation(item, variations);
      resultUpdateStockItem = await meliObject.putAsync(`/items/${id}`, {
        variations: variationsContent,
      });
    } else
      resultUpdateStockItem = await meliObject.putAsync(`/items/${id}`, {
        available_quantity: stock,
      });
    if (resultUpdateStockItem.error) {
      logger.error({
        message: JSON.stringify(resultUpdateStockItem),
        meta: {
          resource: "stock",
          origin: "hobox",
          destiny: "mercadolivre",
          status: "error",
          owner: platform.owner,
          action: "update stock",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "stock",
        status: "error",
        owner: platform.owner,
        message: JSON.stringify(resultUpdateStockItem),
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "stock",
        status: "success",
        owner: platform.owner,
        message: "stock updated",
      });
    }
    return;
  }

  function isAvaliableStockItem(item) {
    let variationQty = 0;
    if (item.variations)
      variationQty = item.variations.reduce(
        (acum, e) =>
          e.available_quantity > 1 ? (acum = e.available_quantity) : 0,
        0
      );
    const result =
      item.available_quantity > 1 || variationQty > 1 ? true : false;
    return result;
  }

  async function updateStatusItem(item) {
    const status = isAvaliableStockItem(item) ? "active" : "paused";
    let resultUpdateStatusItem = null;
    resultUpdateStatusItem = await meliObject.putAsync(`/items/${item.id}`, {
      status: status,
    });
    if (resultUpdateStatusItem.error) {
      logger.error({
        message: JSON.stringify(resultUpdateStatusItem),
        meta: {
          resource: "product",
          origin: "hobox",
          destiny: "mercadolivre",
          status: "error",
          owner: platform.owner,
          action: "update status",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "product",
        status: "error",
        owner: platform.owner,
        message: JSON.stringify(resultUpdateStatusItem),
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "product",
        status: "success",
        owner: platform.owner,
        message: "status updated",
      });
    }
    return;
  }
};
