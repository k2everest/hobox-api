exports.isAllowedUpdateProductMercadolivre = function (platform) {
  return platform.syncConfig && platform.syncConfig.product.send.value;
};

exports.isAllowedUpdateStockProductMercadolivre = function (platform) {
  return platform.syncConfig && platform.syncConfig.stock.send.value;
};
