module.exports.sync = require("./product.sync");
module.exports.update = require("./product.update");
module.exports.updateStock = require("./product.update.stock");
