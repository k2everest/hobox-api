const Meli = require("../../../service/mercadolivre");
const logger = require("../../../service/winston");
const events = require("../../../service/event");
const Product = require("../../product/model");

module.exports = async function (platform, cb) {
  const ownerId = platform.owner;
  let meliObject;
  const sellerId = platform.settings.seller;
  const pageSize = 100;
  let itemsTotal = 100;
  let skusMeli = [];
  let connect = await connectMeli();
  let products = [];
  let itemsForUpdate = [];

  if (connect.error) {
    events.emit("updateConnMl", { owner: ownerId, statusConn: false });
    return cb(connect.error, null);
  }

  if (isAllowedUpdateProductMercadolivre(platform)) {
    for (let i = 0; i < itemsTotal; i = i + pageSize) {
      let items = await getItemsFromMercadolivre(i);
      if (items.error) {
        return cb(items.message, null);
      }
      itemsTotal = items.paging.total;

      const result = await Promise.all(
        items.results.map(
          async (itemId, index) => await getItemDetailsAndSave(itemId)
        )
      );
    }
    manageResult();
  } else {
    return cb("Verifique as configurações de sincronização", null);
  }

  function isAllowedUpdateProductMercadolivre(platform) {
    return platform.syncConfig && platform.syncConfig.product.send.value;
  }

  async function manageResult() {
    analyseAnnouncesBySku();
    const resultHandle = await handleItems();
    await Product.unsetManyMercadolivre(ownerId, skusMeli);
    return cb(null, "success");
  }

  async function handleItems() {
    await Promise.all(
      products.map(
        async (product) =>
          itemsForUpdate.includes(product)
            ? saveItem(product) //shold update hobox
            : pauseAnnounce(product.id) //shold pause announce
      )
    );
  }

  async function pauseAnnounce(itemId) {
    let resultUpdateItem = await meliObject.putAsync(`/items/${itemId}`, {
      status: "paused",
    });
    //console.log(resultUpdateItem);
  }

  async function getItemsFromMercadolivre(i) {
    try {
      let items = await meliObject.getAsync(`/users/${sellerId}/items/search`, {
        limit: pageSize,
        offset: i,
      });
      return items;
    } catch (err) {
      return { error: err };
    }
  }

  async function connectMeli() {
    try {
      meliObject = await Meli.meli(
        platform.settings.accessToken,
        platform.settings.refreshToken
      );
      return meliObject;
    } catch (err) {
      logger.error({
        message: err,
        meta: {
          resource: "product",
          origin: "mercadolivre",
          destiny: "hobox",
          seller: sellerId,
          owner: ownerId,
          action: "meli.get.products",
        },
      });
      return { error: err };
    }
  }

  async function getItemDetailsAndSave(itemId) {
    try {
      let itemCurrent = await meliObject.getAsync(`/items/${itemId}`, {});
      products.push(itemCurrent);
      return "ok";
    } catch (err) {
      logger.error({
        message: err,
        meta: {
          resource: "product",
          origin: "mercadolivre",
          destiny: "hobox",
          seller: sellerId,
          owner: ownerId,
          action: "meli.get.product",
        },
      });
      return "fail";
    }
  }

  async function analyseAnnouncesBySku() {
    let skus = products.map((p) => p.seller_custom_field).filter(onlyUnique);
    skusMeli = skus;
    skus.forEach((sku) => {
      let listItems = listItemsBySku(sku);
      let selectedItem = selectItemForUpdate(listItems);
      itemsForUpdate.push(selectedItem);
    });

    function listItemsBySku(sku) {
      return products.filter((product) => product.seller_custom_field == sku);
    }

    function selectItemForUpdate(listItems) {
      return listItems.reduce(function (acum, elem) {
        if (
          acum == null ||
          (elem.sold_quantity > acum.sold_quantity && elem.status != "closed")
        ) {
          acum = elem;
        } else if (
          acum.sold_quantity <= elem.sold_quantity &&
          (acum.status == "paused" || acum.status == "closed") &&
          elem.status == "active"
        ) {
          acum = elem;
        } else if (
          acum.status == "closed" &&
          (elem.status == "paused" || elem.status == "active")
        )
          acum = elem;

        return acum;
      }, null);
    }
  }

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  async function saveItem(item) {
    if (item.status == "closed") await Product.unsetMercadolivre(ownerId, item);
    else await Product.setMercadolivre(ownerId, item);
  }
};
