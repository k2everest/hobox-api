const Meli = require("../../../service/mercadolivre");
const parser = require("../parser");
const common = require("./");
const helpers = require("./helpers");
const Product = require("../../product/model");
const logger = require("../../../service/winston");
const syncLog = require("../../../service/syncLog");
const rules = require("./rules");

module.exports = async function (platform, cb) {
  let meliObject = null;
  let products = null;

  if (helpers.isAllowedUpdateProductMercadolivre(platform)) {
    try {
      common.sync(platform, async (err, res) => {
        err ? cb(err, null) : await run();
      });
    } catch (err) {
      cb(err.message, null);
    }
  } else {
    cb("Verifique as configurações de sincronização", null);
  }

  async function run() {
    meliObject = await Meli.meli(
      platform.settings.accessToken,
      platform.settings.refreshToken
    );
    products = await Product.getProductsForUpdate(platform);
    let items = await Promise.all(
      products.map(async (item) => {
        item.price = await apllyOffsetFreight(item);
        return item;
      })
    );
    items = applyRules(items);
    let mappedProducts = parser.itemToMercadolivre(
      items,
      platform.mappedAttributes,
      platform.mappedCategories
    );

    common.sync(platform, async (err, res) => cb(null, 'ok'));
    await sendApi(mappedProducts);


  }

  function applyRules(items) {
    let itemsRules = [].concat(platform.rules.offsetFee.products);
    let itemsHandled = items.map((i) =>
      itemsRules.includes(i.sku) ? rules.offsetFee(i) : i
    );
    return itemsHandled;
  }

  const AsyncUpdate = async (item) => {
    return await updateInfoItem(item);
  };

  async function sendApi(items) {
    const timer = ms => new Promise(res => setTimeout(res, ms));

    const itemsLength = items.length;
    for (let i = 0; i < itemsLength; i++) {
      await AsyncUpdate(items[i]);
      await timer(3000);
    }
  }

  async function updateDescriptionItem(item) {
    const id = item.id;
    let resultUpdateDescription = await meliObject.putAsync(
      `/items/${id}/description`,
      item.description
    );
    return;
  }

  async function updateVariationsItem(item) {
    let result = null;
    if (item.variations)
      result = await meliObject.putAsync(`/items/${item.id}/`, {
        variations: item.variations,
      });
  }

  async function updateInfoItem(item) {
    let currentProduct = products.find(
      (e) => e.marketplaces.mercadolivre.id == item.id
    );
    if (currentProduct.marketplaces.mercadolivre.sold_quantity > 0)
      delete item["title"];
    const id = item.id;
    await updateDescriptionItem(item);
    await updateVariationsItem(item);
    delete item["id"];
    delete item["description"];
    delete item["category_id"];
    delete item["shipping"];
    delete item["shipping.local_pick_up"];
    delete item["condition"];
    delete item["status"];
    delete item.available_quantity;
    let idVariation = await getVariationItem(id);
    item.price = item.price.toFixed(2);
    let resultUpdateItem = null;
    if (idVariation != null) {
      resultUpdateItem = await meliObject.putAsync(
        `/items/${id}/variations/${idVariation}`,
        { price: item.price }
      );
    } else resultUpdateItem = await meliObject.putAsync(`/items/${id}`, item);
    if (resultUpdateItem.error) {
      logger.error({
        message: JSON.stringify(resultUpdateItem.cause),
        meta: {
          resource: "product",
          origin: "hobox",
          destiny: "mercadolivre",
          status: "error",
          owner: platform.owner,
          action: "update item",
        },
      });
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "product",
        status: "error",
        owner: platform.owner,
        message: JSON.stringify(resultUpdateItem),
      });
    } else {
      syncLog.log({
        origin: "hobox",
        destiny: "mercadolivre",
        id: item.seller_custom_field,
        resource: "product",
        status: "success",
        owner: platform.owner,
        message: "update item",
      });
      logger.info({
        message: "status updated",
        meta: {
          resource: "product",
          origin: "hobox",
          id: item.seller_custom_field,
          action: "update item",
          destiny: "mercadolivre",
          owner: platform.owner,
        },
      });
    }
  }

  async function getVariationItem(itemId) {
    let itemCurrent = await meliObject.getAsync(`/items/${itemId}`, {});
    if (itemCurrent.variations.length == 1) return itemCurrent.variations[0].id;
    return null;
  }

  async function apllyOffsetFreight(item) {
    const id = item.marketplaces.mercadolivre.id;
    let newPrice = parseFloat(item.price);
    if (item.offsetFreight && item.offsetFreight.includes("mercadolivre")) {
      let shippingInfo = await meliObject.getAsync(
        `/items/${id}/shipping_options/free`,
        {}
      );
      if (
        shippingInfo.coverage &&
        shippingInfo.coverage.all_country.list_cost
      ) {
        let shippingCost = shippingInfo.coverage.all_country.list_cost;
        newPrice = newPrice + parseFloat(shippingCost);
      }
    }

    return newPrice;
  }
};
