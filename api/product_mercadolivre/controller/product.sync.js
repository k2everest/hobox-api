const syncLog = require("../../../service/syncLog");
const common = require("../common");

//syncronize products from mercado livre
module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  try {
    let platform = await db
      .collection("plataform")
      .findOneAsync({ plataform: "mercadolivre", owner: ownerId });
    platform
      ? common.sync(platform, cb)
      : res.status(400).send({ message: "There is not Meli platform" });
  } catch (err) {
    console.log("catch", err);
  }

  function cb(error, result) {
    if (error) res.status(400).send({ message: error });
    else res.json(result);
  }
};
