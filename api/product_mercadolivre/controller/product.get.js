//get list of product or get product by sku from Mercado livre
module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;
  let sku = req.query.sku ? req.query.sku : null;

  try {
    if (sku) {
      let product = null;
      product = await db.collection("product").findOneAsync({
        "marketplaces.mercadolivre.seller_custom_field": sku,
        owner: ownerId,
      });
      let result = product ? product.marketplaces.mercadolivre : null;
      res.json(result);
    } else {
      let products = await db
        .collection("product")
        .find({ owner: ownerId })
        .toArrayAsync();
      res.json(products.map((result) => result.marketplaces.mercadolivre));
    }
  } catch (err) {
    res.status(400).send(err);
  }
};
