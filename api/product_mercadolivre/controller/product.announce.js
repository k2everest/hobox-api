const mercadolivre = require("../../../service/mercadolivre");
const syncLog = require("../../../service/syncLog");
const parser = require("../parser");
const logger = require("../../../service/winston");
const common = require("../common");
const helpers = require("../common/helpers");
const Product = require("../../product/model");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;
  let selectedProductsSkus = req.body.productList;
  const configuration = req.body.configuration;
  const command = req.body.command;
  console.log("command", command);
  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "mercadolivre", owner: ownerId });
  let sellerId;
  let meliObject;
  let generalResult = [];

  if (platform) {
    if (!helpers.isAllowedUpdateProductMercadolivre(platform)) {
      return res
        .status(403)
        .send({ message: "Verifique as configurações de sincronização" });
    }


    sellerId = platform.settings.seller;
    let mappedCategories = platform.mappedCategories;
    let products = await db
      .collection("product")
      .find({ sku: { $in: selectedProductsSkus }, owner: ownerId })
      .toArrayAsync();
    let itemsMapped = parser.itemToMercadolivre(
      products,
      platform.mappedAttributes,
      mappedCategories,
      configuration
    );
    console.log("item mapeado", JSON.stringify(itemsMapped));
    meliObject = await mercadolivre.meli(
      platform.settings.accessToken,
      platform.settings.refreshToken
    );
    await sendApi(itemsMapped);
  } else {
    return res
      .status(400)
      .send({ message: "There is not platform registered" });
  }

  async function checkProductIsConnectedML(item) {
    let product = await db.collection("product").findOneAsync({
      owner: platform.owner,
      "marketplaces.mercadolivre": { $exists: true },
      "marketplaces.mercadolivre.seller_id": platform.settings.seller,
      "marketplaces.mercadolivre.seller_custom_field": item.seller_custom_field,
    });
    return product ? product.id : undefined;
  }

  async function updateListingType(item) {
    const id = item.id;
    let result = null;
    if (item.listing_type_id != null)
      result = await meliObject.postAsync(`/items/${id}/listing_type`, {
        id: item.listing_type_id,
      });
    //console.log('update listing on announce',result);
    return;
  }

  function getSyncParams(item, result) {
    return (params = {
      resource: "product",
      id: item.seller_custom_field,
      message: result.message ? result.message : result.permalink,
      error: result.error ? result.error : "",
      info: result.cause,
      origin: "hobox",
      destiny: "meli",
      status: result.id ? "success" : "fail",
      owner: ownerId,
    });
  }

  function isComandForActiveAnnounce() {
    if (!configuration.shippingMode && !configuration.listingType && !configuration.shippingFree)
      return true;
    return false;

  }


  async function updateInfoItem(item) {
    const id = item.id;
    await updateListingType(item); //update Listing Type
    delete item["id"];
    delete item["description"];
    delete item.listing_type_id;
    delete item.category_id;
    delete item.shipping;
    delete item.available_quantity;
    delete item.condition;
    delete item.price;
    item.status = "active";
    let result = await meliObject.putAsync(`/items/${id}`, item);
    if (result.id) {
      await Product.setActiveManual(ownerId, item, "mercadolivre");
      generalResult.push(
        `${item.seller_custom_field} - Anúncio atualizado com sucesso.`
      );
    } else
      generalResult.push(
        `${item.seller_custom_field} - Problema ao atualizar o anúncio.`
      );

    let params = getSyncParams(item, result);
    syncLog.log(params);
  }

  async function addNewAnnounce(item) {
    delete item.status;
    let result = await meliObject.postAsync(`/items/`, item);

    if (result.id) {
      generalResult.push(
        `${item.seller_custom_field} - Anúncio criado com sucesso.`
      );
    } else
      generalResult.push(
        `${item.seller_custom_field} - Problema ao criar o anúncio.`
      );

    let params = getSyncParams(item, result);
    syncLog.log(params);
  }

  async function handleStatusAnnounce(item, status) {
    const id = item.id;
    let result = await meliObject.putAsync(`/items/${id}`, {
      status: status,
    });

    let params = getSyncParams(item, result);
    syncLog.log(params);

    if (result.id) {
      generalResult.push(
        `${item.seller_custom_field} - Operação realizada com sucesso.`
      );

      return true;

    } else {
      generalResult.push(
        `${item.seller_custom_field} - Problema ao operar o anúncio. Por favor, verifique os logs`
      );
      return false;
    }

  }

  async function pauseAnnounce(item) {
    await handleStatusAnnounce(item, 'paused');
    await Product.setPausedManual(ownerId, item, "mercadolivre");


  }

  async function activeAnnounce(item) {
    await handleStatusAnnounce(item, 'active');
    await Product.setActiveManual(ownerId, item, "mercadolivre");

  }

  async function closeAnnounce(item) {
    const id = item.id;
    let result = await meliObject.putAsync(`/items/${id}`, {
      status: "closed",
    });
    if (result.id) {
      generalResult.push(
        `${item.seller_custom_field} - Anúncio encerrado com sucesso.`
      );
    } else
      generalResult.push(
        `${item.seller_custom_field} - Problema ao encerrar o anúncio.`
      );

    let params = getSyncParams(item, result);
    syncLog.log(params);
  }

  async function handleItems(item) {
    let idProductMl = await checkProductIsConnectedML(item);
    if (command == "connect") {
      if (idProductMl && isComandForActiveAnnounce())
        await activeAnnounce(item)
      else if (idProductMl)
        await updateInfoItem(item);
      else
        await addNewAnnounce(item);
    } else if (command == "pause" && idProductMl) {
      await pauseAnnounce(item);
    } else if (command == "disconnect" && idProductMl) {
      await closeAnnounce(item);
    }
  }

  async function sendApi(items) {
    await Promise.all(items.map(async (item) => await handleItems(item)));
    await setTimeout(syncMercadolivre, 2000);
  }

  async function syncMercadolivre() {
    await common.sync(platform, async (err, res) => cb(err, res));
  }

  function cb(err, result) {
    console.log(err, "puxou", "todo");
    return res.json(generalResult);
  }
};
