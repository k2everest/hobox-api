const sync = require("./product.sync.general");
const agenda = require("../../../service/agenda");
const common = require("../common");

/* //define agenda para atualizar produtos em Hobox
agenda.define('getProductsFromMercadoLivre', function(job, done) {
  sync(common.sync);
  done(console.log('get Products ML'));
}); */

//define agenda para atualizar produtos em Hobox
agenda.define("updateProductsMercadoLivre", function (job, done) {
  sync(common.update);
  done(console.log("Products ML updated"));
});

//define agenda para atualizar produtos em Hobox
agenda.define("updateStockMercadoLivre", function (job, done) {
  sync(common.updateStock);
  done(console.log("Stock ML updated"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("20 minute", "updateProductsMercadoLivre", {
    variable: "updateProductsML",
  });
  agenda.start();
});
agenda.on("ready", function () {
  agenda.every("15 minute", "updateStockMercadoLivre", {
    variable: "updateStockML",
  });
  agenda.start();
});
