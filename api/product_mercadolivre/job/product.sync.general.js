const syncLog = require("../../../service/syncLog");
const common = require("../common");
const db = require("../../../service/db");
const logger = require("../../../service/winston");

//get products from mercado livre
module.exports = async function (action) {
  const cb = (err, result) => {
    err
      ? logger.warn({
        message: err,
        meta: {
          resource: "product",
          origin: "hobox",
          destiny: "meli",
          status: "error",
        },
      })
      : logger.info({
        message: "updated: " + result.length,
        meta: {
          resource: "product",
          origin: "hobox",
          destiny: "meli",
          status: "success",
        },
      });
  };

  try {
    let marketplaces = await db
      .collection("plataform")
      .find({ plataform: "mercadolivre" })
      .sort({ _id: 1 })
      .toArrayAsync();
    if (marketplaces.length > 0)
      marketplaces.forEach(
        async (marketplace) => await action(marketplace, cb)
      );
  } catch (err) {
    throw err;
  }
};
