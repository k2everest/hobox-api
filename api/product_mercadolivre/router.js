const express = require("express");
const router = express.Router();
const announce = require("./controller/product.announce");
const get = require("./controller/product.get");
const sync = require("./controller/product.sync");
const update = require("./controller/product.update");

//get products from Mercado livre
router.get("/mercadolivre", get);
//update products
router.put("/mercadolivre", update);
//update stock
router.put("/mercadolivre/stock", update);
//sync products from Mercado livre
router.post("/mercadolivre/sync", sync);
//sync products with marketplaces
router.post("/marketplace/send", announce);

module.exports = router;
