const data = require("../../../../data");

module.exports.parser = (originalOrder, owner, mappedOrderStatus) => {
  //console.log(JSON.stringify(originalOrder));
  const order = {
    owner: owner,
    id: originalOrder.pedido.numeroPedidoLoja,
    plataform: originalOrder.pedido.tipoIntegracao,
    dateCreated: new Date(originalOrder.pedido.data),
    dateUpdated: new Date(),
    dateClosed: new Date(originalOrder.pedido.dataSaida),
    shippingMethod: "Desconhecido",
    shippingCost: originalOrder.pedido.valorfrete,
    sellerShippingCost: originalOrder.pedido.valorfrete,
    tracking_number: "",
    shippingId: null,
    buyer: parserCustomer(originalOrder.pedido.cliente),
    orderItems: parserItems(originalOrder.pedido.itens),
    total: originalOrder.pedido.totalvenda,
    shippingAddress: isShippingAddressExist(originalOrder.pedido.transporte)
      ? parserShippingAddress(originalOrder.pedido.transporte.enderecoEntrega)
      : parserCustomerAddress(originalOrder.pedido.cliente),
    billingAddress: parserCustomerAddress(originalOrder.pedido.cliente),
    packId: originalOrder.pedido.numeroOrdemCompra
      ? originalOrder.pedido.numeroOrdemCompra
      : null,
    status: parserOrderStatus(mappedOrderStatus, originalOrder.pedido.situacao),
  };

  return order;
};

function parserCustomer(originalCustomer) {
  console.log('original', originalCustomer);
  return {
    firstName:
      originalCustomer.nome.split(" ").slice(0, -1).length < 2
        ? originalCustomer.nome
        : originalCustomer.nome.split(" ").slice(0, -1).join(" "),
    lastName: originalCustomer.nome.split(" ").slice(-1).join(" "),
    nickname: "",
    email: originalCustomer.email,
    vatNumber: originalCustomer.cnpj,
    phones: [`${originalCustomer.fone}`, `${originalCustomer.celular}`],
  };
}

function parserCustomerAddress(originalAddress) {
  return {
    street: originalAddress.endereco,
    number: originalAddress.numero,
    neighborhood: originalAddress.bairro,
    complement: originalAddress.complemento,
    city: originalAddress.cidade,
    region: { id: originalAddress.uf, name: originalAddress.uf },
    postcode: originalAddress.cep,
    country: { id: "BR", name: "Brasil" },
    phone: originalAddress.fone,
  };
}

function parserShippingAddress(originalAddress) {
  return {
    street: originalAddress.endereco,
    number: originalAddress.numero,
    neighborhood: originalAddress.bairro,
    complement: originalAddress.complemento,
    city: originalAddress.cidade,
    region: { id: originalAddress.uf, name: originalAddress.uf },
    postcode: originalAddress.cep,
    country: { id: "BR", name: "Brasil" },
    phone: "",
  };
}

function parserItems(originalItems) {
  console.log("items", originalItems);
  return originalItems.map((e) => {
    return {
      sku: e.item.codigo,
      qty: e.item.quantidade,
      name: e.item.descricao,
      price: parseFloat(e.item.valorunidade),
      specialPrice: parseFloat(e.item.valorunidade - e.item.descontoItem),
    };
  });
}

function parserOrderStatus(mappedOrderStatus, statusOrder) {
  let size = mappedOrderStatus.length;
  let currentStatus = undefined;
  for (let i = 0; i < size; i++) {
    if (mappedOrderStatus[i].status === statusOrder)
      currentStatus = mappedOrderStatus[i].hobox;
    if (i == size - 1)
      return data.hoboxStatus.filter((val) => {
        if (val.value == currentStatus) return val;
      })[0];
  }
}

function isShippingAddressExist(transporte) {
  return transporte && transporte.enderecoEntrega ? true : false;
}

module.exports.convertDateToBrFormat = (date) => {
  let current = date.split("-").reverse().join("/");
  return current;
};
