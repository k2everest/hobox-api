const http = require("https");
const Api = require("../../service/api");
const apiKeyDefault = "b1a36f6914ad80a590edb5815d5c83be816be9f8";
const helper = require("../helper");
const PlatformModel = require("../../../platform/model");
const OrderModel = require("../../../order/model");
const syncLog = require("../../../../service/syncLog");
const logger = require("../../../../service/winston");
const { order } = require("../../platform/data/platform.syncConfigDefault");

module.exports = async (ownerId) => {
  let platform = await PlatformModel.get(ownerId, "bling");
  const currentDate = `${new Date().toISOString().slice(0, 10)}`;
  const filter = `dataEmissao[${helper.convertDateToBrFormat(
    platform.settings.syncOrdersFrom
  )} TO ${helper.convertDateToBrFormat(currentDate)}]`;
  let hasError = false;
  let isDone = false;
  for (let i = 1; isDone === false; i++) {
    const queryData = {
      apikey: platform.settings.apiKey,
      filters: filter,
      page: i,
    };
    let ordersBling = await Api.getOrders(queryData);
    if (ordersBling.retorno.erros) {
      isDone = true;
      hasError = ordersBling.retorno.erros[0].erro.cod == 14 ? false : true;
      continue;
    }

    let orders = ordersBling.retorno.pedidos;
    await Promise.all(orders.map((e) => handleOrder(e, platform)));
  }
  if (hasError) throw new Error("Houve uma falha na sincronição de pedidos");
  return "Pedidos Sincronizados";

  async function handleOrder(blingOrder, platform) {
    console.log(blingOrder.pedido.tipoIntegracao);
    if (platform.settings.platforms.includes(blingOrder.pedido.tipoIntegracao))
      return false;
    let order = helper.parser(
      blingOrder,
      platform.owner,
      platform.mappedOrderStatus
    );
    await OrderModel.updateOrder(ownerId, order, handleResponse);

    function handleResponse(err, res) {
      if (err)
        logger.error({
          message: err,
          meta: {
            resource: "order",
            origin: "bling",
            action: "save order on db",
            id: order.id,
            destiny: "hobox",
            owner: ownerId,
          },
        });
      else {
        if (res.result.nModified > 0) {
          syncLog.log({
            origin: "bling",
            destiny: "hobox",
            id: order.id,
            resource: "order",
            status: "success",
            owner: ownerId,
            message: "updated",
          });
        } else if (res.result.upserted) {
          syncLog.log({
            origin: "bling",
            destiny: "hobox",
            id: order.id,
            resource: "order",
            status: "success",
            owner: ownerId,
            message: "created",
          });
        }
      }
    }
  }
};
