const common = require("../common");
/* const helper = require('../helper');
const PlatformModel = require('../../../platform/model');
const OrderModel = require('../../../order/model'); */

module.exports = async function (req, res) {
  const ownerId = req.decoded._id;

  try {
    /* let platform = await db.collection('plataform').findOneAsync({ plataform: 'bling', owner: ownerId });
    if(!platform){
      res.status(400).send({message: 'There is not Bling platform'});
    }
     common.sync(platform,cb) */
    //let platform =  await PlatformModel.get(ownerId,'bling');
    let response = await common.sync(ownerId);
    /*  let order = response.retorno.pedidos.map(e=>helper.parser(e,ownerId, platform.mappedOrderStatus));
     OrderModel.updateOrder(ownerId,order,) */
    return res.json(response);
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};
