const express = require("express");
const router = express.Router();
const orderController = require("./order/controller");
const platformController = require("./platform/controller");

router.post("/orders", orderController.sync); //sync orders hobox with Bling
router.post("/platform", platformController.save); //save platform settings of Bling
router.get("/platform/status", platformController.test); //save platform settings of Bling

module.exports = router;
