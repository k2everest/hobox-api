const sync = require("./sync");
const agenda = require("../../../service/agenda");

//define agenda
agenda.define("getOrdersFromAmazon", function (job, done) {
  sync.order();
  done(console.log("entrou"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("60 minute", "getOrdersFromAmazon", { variable: "teste" });
  agenda.start();
});
