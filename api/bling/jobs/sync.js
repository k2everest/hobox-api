//const syncLog = require('../../../service/syncLog');
const orderCommon = require("../order/common");
const Platform = require("../../platform/model");

//get orders from bling
getPlatforms = async () => {
  try {
    let marketplaces = await Platform.listByName("bling");
    return marketplaces;
  } catch (err) {
    throw err;
  }
};

module.exports.order = async () => {
  let marketplaces = await getPlatforms();
  if (!marketplaces.error) {
    marketplaces.forEach(
      async (marketplace) => await orderCommon.sync(marketplace.owner)
    );
  }
};
