const http = require("https");
const { filter } = require("bluebird");

const Api = {
  getOrders: async (query = { apikey, page, filters }) =>
    requestApi(query.apikey, "pedidos", query.page, query.filters),
  test: async (apikey) => requestApi(apikey, "pedidos"),
};

async function requestApi(apikey, resource, page = 1, filters = "") {
  console.log(page);
  console.log(filters);

  return new Promise(function (resolve, reject) {
    http
      .get(
        `https://bling.com.br/Api/v2/${resource}/page=${page}/json/?apikey=${apikey}&filters=${filters}`,
        function (res) {
          let data = "";
          res.on("data", function (stream) {
            data += stream;
          });
          res.on("end", function () {
            resolve(JSON.parse(data));
          });
        }
      )
      .on("error", (err) => {
        reject("Error: " + err.message);
      });
  });
}

module.exports = Api;
