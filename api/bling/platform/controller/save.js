const common = require("../../../platform/common");
const Model = require("../../../platform/model");
const data = require("../data");

module.exports = async function (req, res) {
  const ownerId = req.decoded._id;
  let result = await Model.setBlingPlatform(
    ownerId,
    req.body.platform.settings
  );
  if (result.error) return res.status(400).json(result);
  else {
    if (result.upserted) {
      const _idPlatform = result.upserted[0]._id;
      common.setSyncConfig(_idPlatform, data.syncConfigDefault);
      await Model.setMappedOrderStatus(
        ownerId,
        _idPlatform,
        data.mappedOrderStatusDefault
      );
    }
    return res.json(result);
  }
};

/* module.exports = async function(req, res){
    const db = req.db;
    const ownerId = req.decoded._id;

    const plataform = {
        owner: ownerId,
        plataform: req.body.plataform.plataform,
        settings: req.body.plataform.settings
    };

  

};

function setSyncConfigDefault(_idPlatform){
  common.setSyncConfig(_idPlatform, data.syncConfigDefault);

}
 */
