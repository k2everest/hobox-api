const Api = require("../../service/api");
const Model = require("../../../platform/model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let platform = await Model.get(ownerId, "bling");
  let response = await Api.test(platform.settings.apiKey);
  const currentStatus = response.retorno.erros ? false : true;
  await Model.updateConnectionStatus({
    status: currentStatus,
    id: platform._id,
  });
  return response.retorno.erros
    ? res.status(400).send(response)
    : res.json(response);
};
