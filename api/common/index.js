const UserPlanModel = require("../user/planModel");

const isUserAllowed = async (owner) => {
  let plan = await UserPlanModel.getPlan(owner);
  console.log("plan", plan);
  if (plan && plan.status === "active") return true;
  else return false;
};

module.exports = { isUserAllowed };
