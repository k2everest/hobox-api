const Ebanx = {
  status: {
    pending: "PE",
    confirmed: "CO",
    canceled: "CA",
    open: "OP",
  },
  payment_type_code: {
    All: "_all",
    Amex: "amex",
    Boleto: "boleto",
    Diners: "diners",
    Discover: "discover",
    Elo: "elo",
    Hipercard: "hipercard",
    Mastercard: "mastercard",
    Visa: "visa",
  },
  freePayment: {
    payment: {
      hash: "0",
      status: "CO",
      confirm_date: new Date(),
      due_date: new Date(),
      amount_br: "0.00",
      payment_type_code: "free",
    },
  },
};

const Plan = {
  value: 100.0,
  free: 0.0,
};

module.exports = { Ebanx, Plan };
