let Promise = require("bluebird");
const Ebanx = require("../../../lib/ebanx");
let ebanx = new Ebanx();
ebanx = Promise.promisifyAll(ebanx);

class EbanxAdapter {
  constructor(id) {
    this.INTEGRATION_KEY = process.env.INTEGRATION_KEY;
    this.id = id;
    ebanx.configure({
      integrationKey: this.INTEGRATION_KEY,
      testMode: true,
    });
  }

  async queryByPaymentId() {
    const params = {
      merchant_payment_code: this.id,
    };
    let response = await ebanx.queryAsync(params);
    return handleResponse(response);
  }

  async queryByPaymentHash(_hash) {
    const params = {
      hash: _hash,
    };
    let response = await ebanx.queryAsync(params);

    return this.handleResponse(response);
  }

  async create(_user, _amount = "100.00", _payment_type_code = "_all") {
    const params = {
      name: `${_user.name} ${_user.lastname}`,
      email: `${_user.email}`,
      country: "br",
      payment_type_code: _payment_type_code,
      merchant_payment_code: `${this.id}`,
      currency_code: "BRL",
      amount: _amount,
    };

    let response = await ebanx.requestAsync(params);
    return this.handleResponse(response);
  }

  async cancel(_hash) {
    const params = {
      hash: _hash,
    };
    return await ebanx.cancelAsync(params);
  }

  handleResponse(response) {
    let res = JSON.parse(response);
    if (res.status == "ERROR")
      return {
        error: true,
        errorMessage: res.status_message,
      };
    return res;
  }
}

module.exports = EbanxAdapter;
