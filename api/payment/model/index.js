const db = require("../../../service/db");
const Payment = db.collection("payment");
const PaymentCounters = db.collection("paymentCounters");
const CONST = require("../constants");
const Event = require("../event");

async function getNextOrderIncrementId() {
  try {
    await PaymentCounters.updateAsync(
      { _id: "orderIncrementId" },
      {
        $inc: { seq: 1 },
      },
      { upsert: true, returnOriginal: false }
    );
    let result = await PaymentCounters.findOneAsync({
      _id: "orderIncrementId",
    });
    return result.seq;
  } catch (err) {
    return {
      error: true,
      messageError: err.message,
    };
  }
}

exports.createPayment = async (ownerId) => {
  const paymentOrder = {
    hash: null,
    owner: ownerId,
    orderIncrementId: await getNextOrderIncrementId(),
    dueDate: null,
    openDate: new Date(),
    confirmDate: null,
    paymentType: null,
    description: "service hobox",
    status: "PE",
  };

  try {
    let payment = await Payment.findOneAsync({
      owner: ownerId,
      status: CONST.Ebanx.status.open,
    });
    if (payment) return payment._id;

    let result = await Payment.insertOneAsync(paymentOrder, {
      returnOriginal: false,
    });
    return result.insertedId;
  } catch (err) {
    return {
      error: true,
      errorMessage: err.message,
    };
  }
};

exports.update = async (
  paymentId,
  params = {
    hash,
    status,
    confirm_date,
    due_date,
    payment_type_code,
    amount_br,
  }
) => {
  const paymentOrder = {
    hash: params.hash,
    dueDate: params.due_date,
    confirmDate: params.confirm_date,
    paymentType: params.payment_type_code,
    status: params.status,
    total: params.amount_br,
  };

  const paymentIsConfirmed =
    paymentOrder.status == CONST.Ebanx.status.confirmed ? true : false;

  try {
    console.log("order", paymentOrder);
    let result = await Payment.findOneAndUpdateAsync(
      { _id: paymentId },
      { $set: paymentOrder },
      { upsert: false, new: true }
    );
    console.log("object-retorno", result.value);
    if (paymentIsConfirmed) Event.onConfirmPayment(result.value);
    return result;
  } catch (err) {
    return {
      error: true,
      errorMessage: err.message,
    };
  }
};

exports.updateReferenceDate = async (paymentId, _referenceDate) => {
  try {
    let result = await Payment.updateAsync(
      { _id: paymentId },
      { $set: { referenceDate: _referenceDate } },
      { upsert: false }
    );
    return result;
  } catch (err) {
    return {
      error: true,
      errorMessage: err.message,
    };
  }
};

exports.getForUpdate = async () => {
  try {
    let result = Payment.find({
      status: { $nin: ["CO", "CA"] },
    }).toArrayAsync();
    return result;
  } catch (err) {
    return { error: true, errorMessage: err.message };
  }
};

exports.getByOwner = async (ownerId) => {
  try {
    let result = Payment.find({
      owner: ownerId,
    }).toArrayAsync();
    return result;
  } catch (err) {
    return { error: true, errorMessage: err.message };
  }
};
