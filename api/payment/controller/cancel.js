const User = require("../../user/userModel");
const UserHelper = require("../../user/helper");

const cancelPlan = async (ownerId) => {
  await User.Plan.updatePlanByOwner(ownerId, { canceled: true });
};
//deve cancelar o plano
module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let result = null;
  const user = await User.getCurrentUser(ownerId);
  try {
    if (
      UserHelper.Plan.planExist(user.plan) &&
      !!!UserHelper.Plan.isCanceled(user.plan)
    ) {
      await cancelPlan(ownerId);
      result = "subscribe has canceled";
    } else {
      result = "subscribe not can be canceled";
    }
  } catch (err) {
    result = { error: true, message: err.message };
  }

  return result.error
    ? res.status(400).send(result)
    : res.status(200).send(result);
};
