const Payment = require("../model");
const User = require("../../user/userModel");
const UserHelper = require("../../user/helper");
const CONST = require("../constants");
const EbanxAdapter = require("../adapter/ebanx");

const createFreePayment = () => {
  return CONST.Ebanx.freePayment;
};

const userHasPlan = (user) => (user.plan ? true : false);

const createPaymentMethodService = async (user, paymentId) => {
  let paymentService = new EbanxAdapter(paymentId);
  if (userHasPlan(user))
    return await paymentService.create(user, CONST.Plan.value);
  return createFreePayment();
};

const updatePayment = async (paymentId, result) => {
  const { payment } = result;
  return await Payment.update(paymentId, payment);
};

const createNewPayment = async (user) => {
  try {
    const paymentId = await Payment.createPayment(user.id);
    const paymentResponse = await createPaymentMethodService(user, paymentId);
    await updatePayment(paymentId, paymentResponse);
    return paymentResponse;
  } catch (err) {
    return { error: true, message: err.message };
  }
};

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let result = null;
  const user = await User.getCurrentUser(ownerId);
  if (UserHelper.Plan.isCanceledAndActivated(user.plan))
    result = await User.Plan.updatePlanByOwner(ownerId, { canceled: false });
  else result = await createNewPayment(user);

  return result.error ? res.status(400).send(result) : res.json(result);
};
