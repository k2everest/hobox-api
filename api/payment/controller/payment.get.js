const Model = require("../model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let result = await Model.getByOwner(ownerId);
  return result.error ? res.status(400).send(result) : res.json(result);
};
