const Payment = require("../model");
const User = require("../../user/userModel");
const EbanxAdapter = require("../adapter/ebanx");
const CONST = require("../constants");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let paymentId = null;
  try {
    const user = await User.getCurrentUser(ownerId);
    let result = await Payment.createPayment(ownerId);
    //if (!result.error) {
    paymentId = result;
    let request = new EbanxAdapter(paymentId);
    console.log("test-result", result);

    result = request.create(user, CONST.Plan.value);
    //if (!result.error) {
    let responseUpdate = await Payment.update(paymentId, result.payment);
    //if (responseUpdate.error)
    result = responseUpdate;
    //}
    return res.json(result);
  } catch (err) {
    return res.status(400).send(err);
  }

  //return result.error ? res.status(400).send(result) : res.json(result);
};
