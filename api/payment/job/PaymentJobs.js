const Model = require("../model");
const EbanxAdapter = require("../adapter/ebanx");
const Event = require("../event");
const CONST = require("../constants");

class PaymentJobs {
  constructor() {
    this.payments = [];
  }

  async queryAndUpdate() {
    this.payments = await Model.getForUpdate();
    for (let e of this.payments) {
      try {
        let request = new EbanxAdapter(e._id);
        let result = await request.queryByPaymentHash(e.hash);
        await Model.update(e._id, result.payment); //update payment
        if (result.payment.status == CONST.Ebanx.status.pending) {
          Event.onConfirmPayment(e);
        }
      } catch (err) {
        return {
          error: true,
          errorMessage: err.message,
        };
      }
    }
  }
}

module.exports = PaymentJobs;
