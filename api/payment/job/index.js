const agenda = require("../../../service/agenda");
const PaymentJobs = require("./PaymentJobs");
let paymentJobs = new PaymentJobs();

//define agenda
agenda.define("updatePayments", function (job, done) {
  paymentJobs.queryAndUpdate();
  done(console.log("query update payment done"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("30 minute", "updatePayments", {});
  agenda.start();
});
