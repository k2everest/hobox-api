const events = require("../../../service/event");

exports.onConfirmPayment = (payment) => {
  console.log("event confirm payment");
  events.emit("onConfirmPayment", { payment });
};
