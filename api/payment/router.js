const express = require("express");
const router = express.Router();
const controller = require("./controller");

router.post("/", controller.create);
router.post("/subscribe/cancel", controller.cancel);
router.post("/subscribe", controller.subscribe);
router.get("/", controller.get);

module.exports = router;
