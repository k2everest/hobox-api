const parser = require("../parser");
const data = require("../../../data");

module.exports = function (order, mappedOrderStatus, billing_info,shipping) {
  let customer = parser.getMercadoLivreCustomer(order.buyer, billing_info);
  let currentAddress = parser.getMercadoLivreShippingAddress(null, customer);
  let statusMapped = undefined;
  let current = {
    owner: order.owner,
    id: `${order.id}`,
    plataform: "mercadolivre",
    dateCreated: new Date(order.date_created),
    dateUpdated: new Date(order.date_last_updated),
    dateClosed: new Date(order.date_closed),
    shippingMethod: "A combinar",
    shippingCost: 0,
    sellerShippingCost: 0,
    tracking_number: "",
    shippingId: order.shipping.id,
    buyer: customer,
    orderItems: parser.getItems(order.order_items),
    total: order.total_amount,
    shippingAddress: currentAddress,
    billingAddress: currentAddress,
    packId: order.pack_id ? `${order.pack_id}` : null,
  };

  statusMapped = parser.getMercadoLivreStatus(mappedOrderStatus, order.status);
  statusMapped = data.hoboxStatus.filter((val) => {
    if (val.value == statusMapped) return val;
  })[0];
  current.status = statusMapped;

  if (shipping) {
    statusMapped = parser.getMercadoLivreStatus(
      mappedOrderStatus,
      order.status,
      order.shipping.status
    );
    statusMapped = data.hoboxStatus.filter((val) => {
      if (val.value == statusMapped) return val;
    })[0];
    currentAddress = parser.getMercadoLivreShippingAddress(
      shipping.receiver_address,
      customer
    );
    current.shippingAddress = currentAddress;
    current.billingAddress = currentAddress;
    current.status = statusMapped;
    current.shippingId = shipping.id;
    (current.shippingMethod = `${shipping.mode ? shipping.mode : ""} - ${
      shipping.shipping_option ? shipping.shipping_option.name : ""
    }`),
      (current.shippingCost = shipping.shipping_option
        ? shipping.shipping_option.cost
        : "");
    current.sellerShippingCost = shipping.shipping_option
      ? shipping.shipping_option.list_cost
      : "";
    current.tracking_number = shipping.tracking_number;
  }

  return current;
};
