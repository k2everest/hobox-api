module.exports = (mappedOrderStatus, statusOrder, statusShipping = "") => {
  let size = mappedOrderStatus.length;
  let currentStatus = undefined;
  for (let i = 0; i < size; i++) {
    if (
      mappedOrderStatus[i].status === statusShipping &&
      statusShipping != "cancelled"
    ) {
      currentStatus = mappedOrderStatus[i].hobox;
      return currentStatus;
    } else {
      if (mappedOrderStatus[i].status === statusOrder)
        currentStatus = mappedOrderStatus[i].hobox;
      if (i == size - 1) return currentStatus;
    }
  }
};
