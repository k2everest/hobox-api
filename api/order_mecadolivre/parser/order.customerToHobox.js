module.exports = (buyer,billing_info) => {
    
  function getValue(fieldName){
     const result = billing_info.additional_info.filter(a=>a.type==fieldName)[0];
     return result? result.value : ""; 
  }

  return {
    firstName: getValue('FIRST_NAME')? getValue('FIRST_NAME'): buyer.nickname,
    lastName: getValue('LAST_NAME'),
    nickname: buyer.nickname,
    email: buyer.email? buyer.email :'noemail@mail.mercadolivre.com',
    vatNumber: billing_info.doc_number,
    phones: [
      `${buyer.phone && buyer.phone.area_code ? buyer.phone.area_code : ""} ${
        buyer.phone && buyer.phone.number ? buyer.phone.number : ""
      }`,
      `${buyer.alternative_phone && buyer.alternative_phone.area_code ?buyer.alternative_phone.area_code : ""} ${
        buyer.alternative_phone && buyer.alternative_phone.number
          ? buyer.alternative_phone.number
          : ""
      }`,
    ],
  };

 
};
