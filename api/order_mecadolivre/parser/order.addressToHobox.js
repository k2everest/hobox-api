module.exports = (address, customer) => {
  if (!address) {
    return {
      street: "Não informado",
      number: "Não informado",
      neighborhood: "Não informado",
      complement: "Não informado",
      city: "Não informado",
      region: { id: "SP", name: "São Paulo" },
      postcode: "Não informado",
      country: { id: "BR", name: "Brasil" },
      phone: customer.phones[0],
    };
  }
  return {
    street: address.address_line,
    number: address.street_number,
    neighborhood: address.neighborhood.name,
    complement: address.comment,
    city: address.city.name,
    region: address.state,
    postcode: address.zip_code,
    country: address.country,
    phone: address.receiver_phone ? address.receiver_phone : customer.phones[0],
  };
};
