module.exports = (items) => {
  return items.map((e) => {
    return {
      sku: e.item.seller_custom_field,
      qty: e.quantity,
      name: e.item.title,
      price: e.unit_price,
      specialPrice: e.full_unit_price,
    };
  });
};
