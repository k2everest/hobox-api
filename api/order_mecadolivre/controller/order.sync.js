const common = require("../common");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  try {
    let platform = await db
      .collection("plataform")
      .findOneAsync({ plataform: "mercadolivre", owner: ownerId });
    if (!platform) {
      res.status(400).send({ message: "There is not Meli platform" });
    }
    common.sync(platform, cb);
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
  function cb(error, result) {
    if (error) res.status(400).send({ message: error });
    else res.json(result);
  }
};
