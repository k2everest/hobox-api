const handle = require("../common");
const events = require("../../../service/event");

module.exports.run = function () {
    console.log("entrou no run listerner order_mercadolivre");
    events.on("onUpdateOrderFromMagento", action);
};

function action(params) {
    console.log("The event Send NFE To ML has started");
    handle.sendNfe(params);
}
