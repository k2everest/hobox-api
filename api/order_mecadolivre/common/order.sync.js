const mercadolivre = require("../../../service/mercadolivre");
const syncLog = require("../../../service/syncLog");
const parser = require("../parser");
const logger = require("../../../service/winston");
const Order = require("../../order/model");

/**
 * @param  {Object} marketplace object of marketplace
 * @param  {Function} cb callback
 * @return {Promise}
 */

//get orders from mercado livre
module.exports = async (marketplace, cb) => {
  let meliObject;
  let sellerId;
  let ownerId;
  let mappedOrderStatus;

  if (isAllowedReceiveOrderFromML(marketplace)) main(marketplace);
  else return;

  function isAllowedReceiveOrderFromML(platform) {
    return platform.syncConfig && platform.syncConfig.order.receive.value;
  }

  async function main(marketplace) {
    ownerId = marketplace.owner;
    sellerId = marketplace.settings.seller;
    try {
      meliObject = await mercadolivre.meli(
        marketplace.settings.accessToken,
        marketplace.settings.refreshToken
      );
      if (ownerId) {
        mappedOrderStatus = marketplace.mappedOrderStatus;
        await controlRequestOrders();
        cb(null, "success");
      }
    } catch (err) {
      logger.error({
        message: err,
        meta: {
          resource: "order",
          origin: "mercadolivre",
          destiny: "hobox",
          seller: sellerId,
          owner: ownerId,
          action: "start sync orders from meli",
        },
      });
      syncLog.log({
        origin: "meli",
        destiny: "hobox",
        resource: "order",
        status: "fail",
        owner: ownerId,
        message: "failed to get orders",
      });
      cb("error dentro cb " + err.message, null);
    }
  }

  async function controlRequestOrders() {
    let total = 1;
    for (let _offset = 0; _offset < total; _offset = _offset + 50) {
      let result = await requestOrders(_offset, total);

      total = result.paging.total;
      await handleResult(result);

      if (!checkDateLimitOrders(result)) break;
    }
  }

  async function requestOrders(_offset, total) {
    let result = await meliObject.getAsync(`orders/search/`, {
      seller: sellerId,
      offset: _offset,
      limit: 50,
      sort: "date_desc",
    });
    return result;
  }

  async function handleResult(result) {
    if (result.error) throw result.error;
    result.results.every(mapOrderAndSave);
  }

  function checkDateLimitOrders(result) {
    return result.results.every(isDateTimeGreaterLimit);
  }

  function isDateTimeGreaterLimit(order) {
    let isTrue =
      new Date(order.date_created) >
      new Date(marketplace.settings.syncOrdersFrom);
    return isTrue;
  }

  async function saveOrder(order) {
    const ownerId = order.owner;
    await Order.updateOrder(ownerId, order, handleResponse);

    function handleResponse(err, res) {
      if (err)
        logger.error({
          message: err,
          meta: {
            resource: "order",
            origin: "meli",
            action: "save order on db",
            id: order.id,
            destiny: "hobox",
            owner: ownerId,
          },
        });
      else {
        if (res.result.nModified > 0) {
          syncLog.log({
            origin: "meli",
            destiny: "hobox",
            id: order.id,
            resource: "order",
            status: "success",
            owner: ownerId,
            message: "updated",
          });
        } else if (res.result.upserted) {
          syncLog.log({
            origin: "meli",
            destiny: "hobox",
            id: order.id,
            resource: "order",
            status: "success",
            owner: ownerId,
            message: "created",
          });
        }
      }
    }
  }

  async function mapOrderAndSave(order) {
    let shipping = undefined;
    let billing_info = undefined;
    if (isDateTimeGreaterLimit(order)) {
      order.owner = ownerId;
      try {
        billing_info = await meliObject.getAsync(
          `/orders/${order.id}/billing_info`,
          {}
        );
      } catch (err) {
        logger.warn({
          message: err,
          meta: {
            resource: "order",
            origin: "mercadolivre",
            id: order.id,
            action: "getBillingInfo",
            destiny: "hobox",
            owner: ownerId,
          },
        });
      }
      if (order.shipping.id) {
        try {
          shipping = await meliObject.getAsync(
            `/shipments/${order.shipping.id}`,
            {}
          );
        } catch (err) {
          logger.warn({
            message: err,
            meta: {
              resource: "order",
              origin: "mercadolivre",
              id: order.id,
              action: "getShipping",
              destiny: "hobox",
              owner: ownerId,
            },
          });
        }
      }
      let current = parser.getOrderMapped(order, mappedOrderStatus, billing_info.billing_info, shipping);
      saveOrder(current);
    }
  }
};
