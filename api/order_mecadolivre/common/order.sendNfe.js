const mercadolivre = require("../../../service/mercadolivre");
const logger = require("../../../service/winston");
const Order = require("../../order/model");
const http = require("https");
const fs = require("fs");
const xmldoc = require("xmldoc");

/**
 * @param  {Object} marketplace object of marketplace
 * @param  {Function} cb callback
 * @return {Promise}
 */

//get orders from mercado livre
module.exports = async (marketplace, cb) => {
  let meliObject;
  let controller = -1;
  let orders = [];

  if (isAllowedReceiveOrderFromML(marketplace)) {
    meliObject = await mercadolivre.meli(
      marketplace.settings.accessToken,
      marketplace.settings.refreshToken
    );
    orders = await Order.findNotSyncedNfe(marketplace.owner);
    await controlSync();
    cb(null, "success on send nfe");
  }

  async function controlSync() {
    controller = controller + 1;
    if (controller < orders.length)
      await handleOrdersToMercadoLivre(orders[controller]);
  }

  async function handleOrdersToMercadoLivre(order) {
    let key = order.info.nfe.key;
    const url = `https://bling.com.br/relatorios/nfe.xml.php\?s&chaveAcesso=${key}`;
    await requestBlingNfe(url);
    async function requestBlingNfe(url) {
      http.get(url,
        async function (res) {
          if (res.statusCode > 300 && res.statusCode < 400 && res.headers.location) {
            return requestBlingNfe(res.headers.location);
          }
          let data = "";
          res.on("data", function (stream) {
            data += stream;
          });
          res.on("end", async function () {
            const headerXml = '<?xml version="1.0" encoding="UTF-8"?>';
            data = data.replace(headerXml, "").replace("\n", "");
            let docXml = new xmldoc.XmlDocument(data);
            let xmlData = headerXml + "\n" + docXml.toString();
            fs.createWriteStream("./logs/ml.log", { flags: "a" }).write(
              ` ${xmlData} \n\n`
            );
            let shipping = await meliObject.getAsync(
              `/shipments/${order.shippingId}`,
              {}
            );
            fs.createWriteStream("./logs/ml_shipping.log", { flags: "a" }).write(
              ` ${shipping.status} - ${shipping.substatus} \n\n`
            );
            if (
              shipping.status == "ready_to_ship" &&
              shipping.substatus == "invoice_pending"
            ) {
              let result = await meliObject.postXmlAsync(
                `/shipments/${order.shippingId}/invoice_data/`,
                xmlData,
                {
                  siteId: "MLB",
                }
              );
              if (result.fiscal_key && result.fiscal_key.length > 0) {
                Order.updateNfeStatus(order._id, "sent", result.message);
              } else {
                Order.updateNfeStatus(order._id, "error", result.message);
              }
              logger.info({
                message: "send nfe",
                meta: {
                  resource: "order",
                  origin: "hobox",
                  id: order.id,
                  action: "send nfe",
                  destiny: "mercadolivre",
                  owner: order.owner,
                  response: result,
                },
              });
            }
            controlSync();
          });
        }
      );

    }
  }

  function isAllowedReceiveOrderFromML(platform) {
    return platform.syncConfig && platform.syncConfig.order.receive.value;
  }
};

/* else
        return;



    async function main(marketplace){
        ownerId = marketplace.owner;
        sellerId = marketplace.settings.seller;
        try{
            meliObject = await mercadolivre.meli(marketplace.settings.accessToken, marketplace.settings.refreshToken);
            if(ownerId){
                mappedOrderStatus = marketplace.mappedOrderStatus;
                await controlRequestOrders();
                cb(null,'success');
            }
        }catch(err){
            logger.error({message: err, meta:{resource:'order', origin:'mercadolivre', destiny:'hobox',seller: sellerId, owner: ownerId, action: 'start sync orders from meli' } });
            syncLog.log({origin: 'meli',destiny:'hobox', resource:'order', status:'fail', owner: ownerId, message:'failed to get orders'})
            cb('error dentro cb '+err,null);

        }
    }

    async function controlRequestOrders(){
        let total = 1;
        for(let _offset=0; _offset<total; _offset=_offset+50){
            let result = await requestOrders(_offset, total);
            total = result.paging.total;
            await handleResult(result);
            if(!checkDateLimitOrders(result)) break;
        }

    }

    async function requestOrders(_offset, total){
      let result = await meliObject.getAsync(`orders/search/`,{
            seller: sellerId,
            offset: _offset,
            limit: 50,
            sort: 'date_desc'
        });
        return result;

    }

    async function handleResult(result){
        if(result.error) throw(result.error);
        result.results.every(mapOrderAndSave);
    }

    function checkDateLimitOrders(result){
        return result.results.every(isDateTimeGreaterLimit)
    }

    function isDateTimeGreaterLimit(order){
      return new Date(order.date_created) > new Date(marketplace.settings.syncOrdersFrom);
    }

    async function saveOrder(order){
        const ownerId = order.owner;
        Order.updateOrder(ownerId, order, handleResponse);

        function handleResponse(err, res){
            if(err)
              logger.error({message: err,  meta:{resource:'order', origin:'meli', action:'save order on db', id: order.id, destiny:'hobox', owner: ownerId} });
            else{
                if(res.result.nModified > 0){
                  logger.info({message:'updated', meta:{resource:'order', origin:'meli', action:'updated', id: order.id, destiny:'hobox', owner: ownerId} } );
                  syncLog.log({origin: 'meli',destiny:'hobox',id: order.id, resource:'order', status:'success', owner: ownerId, message:'updated'})
                }else if(res.result.upserted){
                  logger.info({message:'created', meta:{resource:'order', origin:'meli', id: order.id, action:'created', destiny:'hobox', owner: ownerId} } );
                  syncLog.log({origin: 'meli',destiny:'hobox',id: order.id, resource:'order', status:'success', owner: ownerId, message:'created'})
                }
            }
        }

    }

    async function mapOrderAndSave(order){
        let shipping = undefined;
        if(isDateTimeGreaterLimit(order)){
                order.owner = ownerId;
                if(order.shipping.id){
                    try{
                        shipping = await meliObject.getAsync(`/shipments/${order.shipping.id}`,{});
                    }catch(err){
                        logger.warn({message: err, meta:{resource:'order', origin:'mercadolivre', id: order.id, action:'getShipping', destiny:'hobox', owner: ownerId} });
                    }
                }
            let current = parser.getOrderMapped(order,mappedOrderStatus,shipping);
            saveOrder(current);
        }
    }

}













const http = require('https');

module.exports =(req,response) => {

let req1 = http.get("https://bling.com.br/relatorios/nfe.xml.php\?s&chaveAcesso=35171114034781000140550010000078121553776147", function(res) {
    let data = '';
    res.on('data', function(stream) {
        data += stream;
    });
    res.on('end', function(){
        response.send(data);
    });
});
} */
