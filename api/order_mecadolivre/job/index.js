const sync = require("./order.sync");
const sendNfe = require("./order.sendNfe");

const db = require("../../../service/db");
const agenda = require("../../../service/agenda");

//define agenda
agenda.define("getOrdersFromMercadoLivre", function (job, done) {
  sync(db);
  done(console.log("entrou"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("30 minute", "getOrdersFromMercadoLivre", { variable: "teste" });
  agenda.start();
});

agenda.define("sendNfeToMercadolivre", function (job, done) {
  sendNfe(db);
  done(console.log("entrou"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("30 minute", "sendNfeToMercadolivre", { variable: "teste" });
  agenda.start();
});
