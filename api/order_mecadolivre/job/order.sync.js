const syncLog = require("../../../service/syncLog");
const common = require("../common");

//get orders from mercado livre
module.exports = async function (db) {
  const cb = (err, result) => {
    err ? console.log("err", err) : console.log("result", result);
  };

  try {
    let marketplaces = await db
      .collection("plataform")
      .find({ plataform: "mercadolivre" })
      .sort({ _id: 1 })
      .toArrayAsync();
    if (marketplaces.length > 0)
      marketplaces.forEach(
        async (marketplace) => await common.sync(marketplace, cb)
      );
  } catch (err) {
    throw err;
  }
};
