var express = require("express");
var router = express.Router();
const orderSync = require("./controller/order.sync");

//sync orders hobox with Mercado Livre
router.post("/sync/mercadolivre", orderSync);

module.exports = router;
