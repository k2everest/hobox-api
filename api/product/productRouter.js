const express = require("express");
const router = express.Router();
const productHandlers = require("./controller/product.js");
const productList = require("./controller/product.list");
const productBulk = require("./controller/product.patch.bulk");

const syncProductMagento = require("./controller/product.sync.magento");
const syncStocktMagento = require("./controller/product.sync.stock.magento");

/**
 *@swagger
 * definitions:
 *
 *  Attribute:
 *   type: object
 *   properties:
 *      key:
 *          type: string
 *      name:
 *          type: string
 *
 *
 *  Product:
 *   type: object
 *   properties:
 *      id:
 *          type: string
 *      sku:
 *          type: string
 *      status:
 *          type: string
 *      name:
 *          type: string
 *      description:
 *          type: string
 *      images:
 *          type: string
 *      categories:
 *        schema:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Category'
 *      attributes:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *             key:
 *               type: string
 *             value:
 *               type: string
 *      options_attributes:
 *          type: array
 *          items:
 *             $ref: '#/definitions/Attribute'
 *      price:
 *        type: number
 *      qty:
 *        type: number
 *      weight:
 *        type: number
 *
 **/

/**
 *@swagger
 * /products:
 *   get:
 *     parameters:
 *      - in: query
 *        name: page
 *        schema:
 *          type: integer
 *        description: The page of collection
 *      - in: query
 *        name: limit
 *        schema:
 *          type: integer
 *        description: The numbers of items to return
 *      - in: query
 *        name: sku
 *        schema:
 *          type: string
 *        description: product sku
 *      - in: query
 *        name: name
 *        schema:
 *          type: string
 *        description: product name
 *     tags:
 *       - Products
 *     description: Returns all products
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of products
 *         schema:
 *              type: array
 *              items:
 *                  type: object
 *                  properties:
 *                    _id:
 *                      type: string
 *                    sku:
 *                      type: string
 *                    status:
 *                      type: string
 *                    name:
 *                      type: string
 *                    description:
 *                      type: string
 *                    images:
 *                      type: string
 *                    categories:
 *                        type: array
 *                        items:
 *                          $ref: '#/definitions/Category'
 *                    price:
 *                      type: number
 *                    qty:
 *                      type: number
 *
 */

//get all products
router.get("/", productList);

/**
 *@swagger
 * /products/{sku}:
 *   get:
 *     tags:
 *       - Products
 *     description: Returns a product by sku
 *     parameters:
 *       - name: sku
 *         description: product sku
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A product
 *         schema:
 *              type: array
 *              items:
 *                 $ref: '#/definitions/Product'
 *
 *
 */
//get a specific product
router.get("/:sku", productHandlers.getProduct);

//update field produts /produts
router.patch("/", productBulk.patchBulk);

/**
 *@swagger
 * /products/magento/sync:
 *   post:
 *     tags:
 *       - Products
 *     description: Synchronize products with magento plataform
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: sync started
 *
 */

//sync products integrator
router.post("/magento/sync", syncProductMagento);

/**@swagger
 * /products/magento/sync/stock:
 *   post:
 *     tags:
 *       - Products
 *     description: Synchronize stock of product from magento plataform
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: sync started
 *
 */

//sync stock of products
router.post("/magento/sync/stock", syncStocktMagento);

//router.post('/shippingPreferences',sync);

//sync products
//router.get('/sync',productHandlers.sync);

//sync categories of the products
//router.get('/sync/categories',productHandlers.categories);
//router.get('/sync/info',productHandlers.info);
//router.get('/custom',productHandlers.customapi);
//router.get('/test',productHandlers.getMagentoProducts);

module.exports = router;
