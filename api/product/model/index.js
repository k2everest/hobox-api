const db = require("../../../service/db");
const logger = require("../../../service/winston");
const syncLog = require("../../../service/syncLog");
const Product = db.collection("product");

exports.save = async (ownerId, product) => {
  const setLastTimeUpdated = async (product) => {
    try {
      await Product.updateAsync(
        { id: product.id, owner: ownerId },
        {
          $currentDate: { lastModified: true },
          $set: product,
        },
        { upsert: false }
      );

      logger.info({
        message: "updated",
        meta: {
          resource: "product",
          id: product.sku,
          origin: "magento",
          destiny: "hobox",
          status: "success",
          owner: ownerId,
          action: "update product",
        },
      });
    } catch (err) {
      logger.error({
        message: err,
        meta: {
          resource: "product",
          id: product.sku,
          origin: "magento",
          destiny: "hobox",
          status: "error",
          owner: ownerId,
          action: "update product",
        },
      });
    }
  };

  try {
    let result = await Product.updateAsync(
      { id: product.id, owner: ownerId },
      { $set: product },
      { upsert: true }
    );

    if (result.result.nModified > 0) {
      setLastTimeUpdated(product);
      syncLog.log({
        origin: "magento",
        destiny: "hobox",
        id: product.id,
        resource: "product",
        status: "success",
        owner: product.owner,
        message: "product updated",
      });
    } else if (result.result.upserted) {
      syncLog.log({
        origin: "magento",
        destiny: "hobox",
        id: product.id,
        resource: "product",
        status: "success",
        owner: product.owner,
        message: "product inserted",
      });
      logger.info({
        message: "created",
        meta: {
          resource: "product",
          origin: "magento",
          id: product.id,
          action: "create",
          destiny: "hobox",
          owner: ownerId,
        },
      });
    }
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "magento",
        destiny: "hobox",
        status: "error",
        owner: ownerId,
        action: "update product",
      },
    });
    syncLog.log({
      origin: "magento",
      destiny: "hobox",
      id: product.id,
      resource: "product",
      status: "error",
      owner: product.owner,
      message: err.message,
    });
  }
};

exports.deleteAll = async (ownerId) => {
  try {
    let result = await Product.remove({ owner: ownerId });
    if (result)
      syncLog.log({
        origin: "magento",
        destiny: "hobox",
        resource: "product",
        status: "success",
        owner: ownerId,
        message: "products removed",
      });
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "magento",
        destiny: "hobox",
        status: "error",
        owner: ownerId,
        action: "remove products",
      },
    });
    syncLog.log({
      origin: "magento",
      destiny: "hobox",
      resource: "product",
      status: "error",
      owner: ownerId,
      message: err.message,
    });
  }
};

exports.deleteByIds = async (ownerId, ids = []) => {
  try {
    let result = await Product.remove({ owner: ownerId, id: { $in: ids } });
    if (result)
      syncLog.log({
        origin: "magento",
        destiny: "hobox",
        resource: "product",
        status: "success",
        owner: ownerId,
        message: "products removed",
      });
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "magento",
        destiny: "hobox",
        status: "error",
        owner: ownerId,
        action: "remove products",
      },
    });
    syncLog.log({
      origin: "magento",
      destiny: "hobox",
      resource: "product",
      status: "error",
      owner: ownerId,
      message: err.message,
    });
  }
};

async function updateVariations(ownerId, product) {
  try {
    let result = await Product.findOneAndUpdateAsync(
      { owner: ownerId, "variations.id": product.product_id },
      { $set: { "variations.$.qty": parseInt(product.qty) } },
      { returnOriginal: false }
    );
    return result.value;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
}

exports.updateStock = async (ownerId, product) => {
  try {
    let current = await Product.updateAsync(
      { id: product.product_id, owner: ownerId },
      { $set: { qty: parseInt(product.qty) } },
      { upsert: false }
    );

    if (current.result.nModified > 0 || current.result.upserted) {
      logger.info({
        message: "updated",
        meta: {
          resource: "stock",
          origin: "magento",
          destiny: "hobox",
          status: "success",
          owner: ownerId,
          action: "update stock",
        },
      });
      syncLog.log({
        origin: "magento",
        destiny: "hobox",
        id: product.id,
        resource: "stock",
        status: "success",
        owner: ownerId,
        message: "stock updated",
      });
    } else {
      await updateVariations(ownerId, product);
    }
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "stock",
        origin: "magento",
        destiny: "hobox",
        status: "error",
        owner: ownerId,
        action: "update stock",
      },
    });
  }
};

exports.findByOwner = async (ownerId) => {
  try {
    let result = await db
      .collection("product")
      .find({ owner: ownerId })
      .toArrayAsync();
    return result;
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.findByOwnerAndSku = async (ownerId, sku) => {
  try {
    let result = await db
      .collection("product")
      .findOneAsync({ owner: ownerId, sku: sku });
    return result;
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.findByOwnerAndSkus = async (ownerId, skus) => {
  try {
    let result = await db
      .collection("product")
      .find({ owner: ownerId, sku: { $in: skus } })
      .toArrayAsync();
    return result;
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.findAll = async (ownerId, filter, sortBy, limit, page) => {
  try {
    const count = await db
      .collection("product")
      .find({ owner: ownerId, $and: filter })
      .countAsync();
    const tpages = Math.ceil(count / limit);
    const products = await db
      .collection("product")
      .find({ owner: ownerId, $and: filter })
      .sort(sortBy)
      .limit(limit)
      .skip(page)
      .toArrayAsync();
    return { products: products, total: count, pages: tpages };
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.setMercadolivre = async (ownerId, item) => {
  try {
    await db
      .collection("product")
      .updateAsync(
        { owner: ownerId, sku: item.seller_custom_field },
        { $set: { "marketplaces.mercadolivre": item } },
        { upsert: false }
      );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        seller: item.seller_id,
        owner: ownerId,
        action: "save product",
      },
    });
  }
};

exports.setPausedManual = async (ownerId, item, marketplace) => {
  try {
    await db
      .collection("product")
      .updateAsync(
        { owner: ownerId, sku: item.seller_custom_field },
        { $addToSet: { pausedManual: marketplace } },
        { upsert: false }
      );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        seller: item.seller_id,
        owner: ownerId,
        action: "update to paused product",
      },
    });
  }
};

exports.setActiveManual = async (ownerId, item, marketplace) => {
  try {
    await db
      .collection("product")
      .updateAsync(
        { owner: ownerId, sku: item.seller_custom_field },
        { $pull: { pausedManual: marketplace } },
        { upsert: false }
      );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        seller: item.seller_id,
        owner: ownerId,
        action: "update to active product",
      },
    });
  }
};

exports.unsetMercadolivre = async (ownerId, item) => {
  try {
    await db
      .collection("product")
      .updateAsync(
        { owner: ownerId, sku: item.seller_custom_field },
        { $unset: { "marketplaces.mercadolivre": null } },
        { upsert: false }
      );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        seller: item.seller_id,
        owner: ownerId,
        action: "unset product",
      },
    });
  }
};

exports.unsetManyMercadolivre = async (ownerId, skus) => {
  try {
    await db
      .collection("product")
      .updateManyAsync(
        { owner: ownerId, sku: { $nin: skus } },
        { $unset: { "marketplaces.mercadolivre": null } }
      );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        owner: ownerId,
        action: "clear product",
      },
    });
  }
};

exports.getProductsForUpdate = async (platform) => {
  const isFullSync = platform.isFullSync;

  let listProduct = await db
    .collection("product")
    .find({
      owner: platform.owner,
      "marketplaces.mercadolivre": { $exists: true },
      "marketplaces.mercadolivre.seller_id": platform.settings.seller,
      pausedManual: { $nin: ["mercadolivre"] },
    })
    .toArrayAsync();


  if (listProduct.length > 0)
    return isFullSync ? listProduct : listProduct.filter((obj) => filterBydate(obj));

  return [];

  async function filterBydate(obj) {
    if (
      !obj.lastModified ||
      obj.marketplaces.mercadolivre.last_updated <
      obj.lastModified.toISOString()
    ) {
      return obj;
    }
  }
};

exports.getProductsFreeShippingMercadolivre = async (platform) => {
  let listProduct = await db
    .collection("product")
    .find({
      owner: platform.owner,
      "marketplaces.mercadolivre": { $exists: true },
      "marketplaces.mercadolivre.seller_id": platform.settings.seller,
      "marketplaces.mercadolivre.shipping.free_shippping": true,
    })
    .toArrayAsync();
  return listProduct.length > 0 ? listProduct : [];
};

exports.setManyOffsetFreight = async (ownerId, skus, marketplace) => {
  console.log("skus", skus);
  try {
    await db.collection("product").updateManyAsync(
      { owner: ownerId, sku: { $in: skus } },
      {
        $push: { offsetFreight: marketplace },
        $currentDate: { lastModified: true },
      }
    );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        owner: ownerId,
        action: "clear product",
      },
    });
  }
};

exports.unsetManyOffsetFreight = async (ownerId, skus, marketplace) => {
  try {
    await db.collection("product").updateManyAsync(
      { owner: ownerId, sku: { $in: skus } },
      {
        $pull: { offsetFreight: marketplace },
        $currentDate: { lastModified: true },
      }
    );
  } catch (err) {
    logger.error({
      message: err,
      meta: {
        resource: "product",
        origin: "mercadolivre",
        destiny: "hobox",
        owner: ownerId,
        action: "clear product",
      },
    });
  }
};
