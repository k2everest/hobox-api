const magento = require("../../../service/magento");
const Product = require("../model");

module.exports = async function (platform, cb) {
  const ownerId = platform.owner;
  magento(platform).xmlrpc.auto.integrator.product.deletedItems(async function (
    err,
    ids
  ) {
    console.log("delete", ids);
    if (err) {
      console.log(err);
      cb(err, null);
    } else {
      await Product.deleteByIds(ownerId, ids);
      cb(null, "ok");
    }
  });
};
