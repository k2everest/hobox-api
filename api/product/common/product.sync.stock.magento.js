const magento = require("../../../service/magento");
const db = require("../../../service/db");
const Product = require("../model");

module.exports = async function (platform, cb) {
  const pageSize = 50;
  const ownerId = platform.owner;


  if (isAllowedReceiveStockFromMagento(platform)) {
    let products = await Product.findByOwner(ownerId);
    if (products.error) return cb(products.message, null);

    let productsIds = products.map((product) => {
      return product.id;
    });
    const productsWithVariations = products.filter(p => p.variations);
    let productsIdsVariations = getProductsIdsVariations(productsWithVariations);
    productsIds.concat(productsIdsVariations);

    const totalPages = productsIds.length / pageSize;
    let initialIdPosition = 0;

    for (let i = 0; i < totalPages; i++) {
      try {
        let productsIdsCurrent = productsIds.slice(initialIdPosition, initialIdPosition + pageSize);
        await requestMagentoApi(productsIdsCurrent);
        initialIdPosition = initialIdPosition + pageSize;
      } catch (err) {
        return cb("Falha na sincronização", null)
      }
    }

    return cb(null, "ok");

  }

  async function requestMagentoApi(productsIds) {
    return new Promise((resolve, reject) => {
      magento(platform).xmlrpc.auto.integrator.stock.list(
        productsIds,
        async function (err, result) {
          if (err) {
            return reject(err);
          } else {
            await updateStock(result, ownerId);
            setLastTimeSyncAction();
            resolve(result);
          }
        }
      );
    })
  }

  function getProductsIdsVariations(products) {
    let variationIds = [];
    products.filter((p) => {
      return p.variations.reduce((acum, e) => {
        variationIds.push(e.id);
      }, false);
    });

    return variationIds;
  }

  async function setLastTimeSyncAction() {
    await db
      .collection("plataform")
      .updateAsync(
        { _id: platform._id },
        { $set: { "syncConfig.stock.receive.lastModified": new Date() } },
        { upsert: false }
      );
  }

  function isAllowedReceiveStockFromMagento(platform) {
    return platform.syncConfig && platform.syncConfig.stock.receive.value;
  }

  async function updateStock(result, ownerId) {
    result.map(async (product) => {
      await Product.updateStock(ownerId, product);
    });
  }
};
