const magento = require("../../../service/magento");
const helper = require("../helper");
const db = require("../../../service/db");
const Product = require("../model");
const Category = require("../../category/model");
const syncStockMagento = require("./product.sync.stock.magento");
const syncDeletedItems = require("./product.sync.deleted.magento");

//sync products
module.exports = async function (platform, isFullSync = false, callback) {
  const ownerId = platform.owner;
  let categories = [];
  if (isAllowedReceiveProductsFromMagento(platform)) {
    await checkCategoriesExist(ownerId);
    resolveAllRequest()
      .then((result) => result.map((l) => actionsUpdate(l.products)))
      .then(() => syncStockMagento(platform, (err, res) => {}))
      .then(() => syncDeletedItems(platform, callback))
      .catch(() => callback("Falha na sincronização", null));
  } else {
    callback("Verifique as configurações de sincronização", null);
  }

  async function checkCategoriesExist() {
    categories = await Category.get(ownerId);
    if (!categories || categories.length < 1)
      return callback("there are not categories", null);
  }

  async function actionsUpdate(products) {
    let result = handleProducts(products);
    await updateProducts(result);
    await setLastTimeSyncAction();
  }

  async function resolveAllRequest() {
    let result = [];
    let res = await request(0);
    let pageTotal = Math.ceil(res.total / res.limit);
    for (let page = 0; page <= pageTotal; page++) {
      result[page] = await request(page + 1);
    }

    return result;
  }

  async function request(page) {
    return new Promise((resolve, reject) => {
      magento(platform).xmlrpc.auto.integrator.product.items(
        platform.settings.store,
        getDatetimeLastSync(),
        null,
        page,
        async function (err, result) {
          if (err) return reject(err);
          else resolve(result);
        }
      );
    });
  }

  function isAllowedReceiveProductsFromMagento(platform) {
    return platform.syncConfig && platform.syncConfig.product.receive.value;
  }

  function getDatetimeLastSync() {
    return platform.syncConfig &&
      platform.syncConfig.product.receive.lastModified &&
      isFullSync == false
      ? platform.syncConfig.product.receive.lastModified
      : null;
  }

  function assignCategoryToProduct(categories, product) {
    let categoriesCurrent = product.attributes
      .filter((obj) => helper.getValue("categories", obj))
      .map((obj) => obj.value.replace(/\s/g, "").split(";"))[0];
    let filteredCategories = [];
    if (categoriesCurrent) {
      filteredCategories = categories.filter((f) =>
        categoriesCurrent.includes(f.id)
      );
    }
    return Object.assign(product, { categories: filteredCategories });
  }

  function handleProducts(products) {
    let handledItems = products.map((product) => {
      let enrichedProduct = assignCategoryToProduct(categories, product);
      return helper.mapProduct(enrichedProduct);
    });

    return handledItems;
  }

  async function updateProducts(products) {
    products.map(async (product) => {
      await Product.save(ownerId, product);
    });
  }

  async function setLastTimeSyncAction() {
    await db
      .collection("plataform")
      .updateAsync(
        { _id: platform._id },
        { $set: { "syncConfig.product.receive.lastModified": new Date() } },
        { upsert: false }
      );
  }
};
