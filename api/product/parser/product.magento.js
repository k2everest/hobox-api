exports.getConfig = function (plataform) {
  let config = {
    host: plataform.settings.url,
    port: 443,
    xmlrpc_path: plataform.settings.url_api,
    login: plataform.settings.username,
    pass: plataform.settings.password,
  };
  return config;
};
