const events = require("../../../service/event");

exports.OnUpdateProducts = (ownerId) => {
  console.log("event after update products hobox");
  events.emit("onUpdateProducts", { ownerId });
};
