//synchonize product magento
module.exports = async function (req, res) {
  let db = req.db;
  const ownerId = req.decoded._id;
  let categories = [];

  try {
    let result = await db
      .collection("category")
      .findOneAsync({ owner: ownerId }); //get categories
    if (result) categories = result.categories;
  } catch (err) {
    throw err;
  }

  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "magento", owner: ownerId });
  !platform
    ? res
        .status(400)
        .send({ message: "There is not magento platform registered" })
    : syncMagento(platform);

  function syncMagento(platform) {
    if (!platform.syncConfig || !platform.syncConfig.product.receive.value)
      return res
        .status(400)
        .send({ message: "Verifique as configurações de sincronização" });

    magento(platform).xmlrpc.auto.integrator.product.items(
      "default",
      null,
      null,
      handleMagentoResponse
    );
  }

  async function handleMagentoResponse(error, result) {
    if (error)
      res.status(400).send({
        message: "wrong credentials or magento server is not connected",
      });
    else {
      res.json("ok");
      result.map(function (product) {
        let categoriesCurrent = product.attributes
          .filter((obj) => helper.getValue("categories", obj))
          .map((obj) => {
            return obj.value.replace(/\s/g, "").split(";");
          })[0];
        let filtered = [];
        if (categoriesCurrent) {
          filtered = categories.filter((f) => categoriesCurrent.includes(f.id));
        }
        const priceCurrent = parseFloat(
          product.attributes
            .filter((obj) => helper.getValue("price", obj))
            .map((obj) => {
              return obj.value;
            })[0]
        );

        product = {
          owner: ownerId,
          id: product.attributes.filter((obj) =>
            helper.getValue("product_id", obj)
          )[0].value,
          sku: product.attributes.filter((obj) =>
            helper.getValue("sku", obj)
          )[0].value,
          status: product.attributes.filter((obj) =>
            helper.getValue("status", obj)
          )[0].value,
          name: product.attributes.filter((obj) =>
            helper.getValue("name", obj)
          )[0].value,
          description: product.attributes.filter((obj) =>
            helper.getValue("description", obj)
          )[0].value,
          weight: parseFloat(
            product.attributes
              .filter((obj) => helper.getValue("weight", obj))
              .map((obj) => {
                return obj.value;
              })[0]
          ),
          images: product.image_urls,
          categories: filtered,
          attributes: product.attributes.filter((obj) =>
            helper.getAttributes(obj)
          ),
          option_attributes: product.multiselect_attributes,
          price: !isNaN(priceCurrent) ? priceCurrent : 0,
        };

        saveProduct(product);
      });
    }
  }

  function saveProduct(product) {
    db.collection("product").update(
      { id: product.id, owner: ownerId },
      {
        $set: product,
      },
      { upsert: true },
      function (err, result) {
        if (err) {
          logger.error({
            message: err,
            meta: {
              resource: "product",
              origin: "magento",
              destiny: "hobox",
              owner: ownerId,
              action: "update product",
            },
          });
        } else {
          if (result.nModified > 0) console.log("modificiou");
          return;
        }
      }
    );
  }

  function setLastTimeUpdated(product) {
    db.collection("product").update(
      { id: product.id, owner: ownerId },
      {
        $currentDate: {
          lastModified: true,
        },
        $set: product,
      },
      { upsert: false },
      function (err, result) {
        err
          ? console.log(err)
          : logger.info({
              message: err,
              meta: {
                resource: "product",
                origin: "magento",
                destiny: "hobox",
                results: result,
                owner: ownerId,
                action: "update product",
              },
            });
      }
    );
  }
};
