const Product = require("../model");

//get list of products
module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  const limit = req.query.limit ? req.query.limit : 100;
  const page = req.query.page ? parseInt(req.query.page) * limit - limit : 0;
  const sortBy = req.query.sortBy ? req.query.sortBy : "id";
  const direction = req.query.direction
    ? parseInt(req.query.direction == "asc" ? 1 : -1)
    : -1;
  let filter = [];
  let currentSort = {};
  currentSort[sortBy] = direction;
  req.query.skus = ["253", "254", "248"];
  const ninSkus = [].concat(req.query.ninSkus);
  const inSkus = [].concat(req.query.inSkus);

  handleQuery();

  let result = await Product.findAll(ownerId, filter, currentSort, limit, page);
  if (result.error) return res.status(400).send(result);
  else {
    const products = result.products.map((item) => {
      return {
        id: item.id,
        sku: item.sku,
        name: item.name,
        description: item.description,
        image: item.images[0],
        categories: item.categories,
        price: item.price,
        qty: item.qty,
        status:
          item.marketplaces && item.marketplaces.mercadolivre
            ? item.marketplaces.mercadolivre.status
            : "desconectado",
        marketplaces: getAllProperties(item.marketplaces),
        offsetFreight: item.offsetFreight ? item.offsetFreight : [],
      };
    });

    return res.json({
      products: products,
      total: result.total,
      pages: result.pages,
    });
  }

  function handleQuery() {
    req.query.priceTo
      ? filter.push({ price: { $lte: parseFloat(req.query.priceTo) } })
      : null;
    req.query.priceFrom
      ? filter.push({ price: { $gte: parseFloat(req.query.priceFrom) } })
      : null;
    req.query.qtyTo
      ? filter.push({ qty: { $lte: parseInt(req.query.qtyTo) } })
      : null;
    req.query.qtyFrom
      ? filter.push({ qty: { $gte: parseInt(req.query.qtyFrom) } })
      : null;
    req.query.sku
      ? filter.push({ sku: { $regex: `.*${req.query.sku}.*`, $options: "i" } })
      : null;
    req.query.name
      ? filter.push({
          name: { $regex: `.*${req.query.name}.*`, $options: "i" },
        })
      : null;
    req.query.categories && req.query.categories[0] != ""
      ? filter.push({ "categories.id": { $all: req.query.categories } })
      : null;
    req.query.status && req.query.status[0] != ""
      ? filter.push({
          "marketplaces.mercadolivre.status": { $in: req.query.status },
        })
      : null;
    req.query.freeShipping && req.query.freeShipping == "mercadolivre"
      ? filter.push({
          "marketplaces.mercadolivre.shipping.free_shipping": true,
        })
      : null;
    req.query.offsetFreight
      ? filter.push({ offsetFreight: req.query.offsetFreight })
      : null;
    req.query.inSkus != undefined
      ? filter.push({ sku: { $in: inSkus } })
      : null;
    req.query.ninSkus != undefined
      ? filter.push({ sku: { $nin: ninSkus } })
      : null;
    let marketplaces = req.query.marketplaces;

    if (marketplaces && marketplaces[0] != "") {
      let filterOr = [];

      marketplaces.map((m) => {
        marketplace = {};
        if (m == "none") {
          filterOr = [
            { marketplaces: { $eq: {} } },
            { marketplaces: { $exists: false } },
          ];
          return;
        } else marketplace["marketplaces." + m] = { $exists: true };

        filterOr.push(marketplace);
      });

      filter.push({ $or: filterOr });
    }

    if (filter.length < 1) filter.push({});
  }

  function getAllProperties(obj) {
    let properties = [];
    for (property in obj) {
      properties.push(property);
    }

    return properties;
  }
};
