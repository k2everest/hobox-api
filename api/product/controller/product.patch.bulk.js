process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
const Product = require("../model");

exports.patchBulk = async (req, res) => {
  const action = req.body.action;
  const skus = req.body.skus;
  const value = req.body.value;
  let ownerId = req.decoded._id;
  const attribute = req.body.attribute;
  let result = null;

  console.log(req.body);

  if (attribute === "offsetFreight") await handleoffsetFreight();
  else return res.status(404).send("resource not found");

  return res.json("success");

  async function handleoffsetFreight() {
    console.log(action);
    switch (action) {
      case "add":
        await Product.setManyOffsetFreight(ownerId, skus, value);
        console.log("passou aqui");
        break;
      case "remove":
        await Product.unsetManyOffsetFreight(ownerId, skus, value);
        break;
      default:
        result = "none action selected";
    }
  }
};
