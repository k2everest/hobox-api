const common = require("../common");

//get stock from magento and update stock hobox
module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "magento", owner: ownerId });
  !platform
    ? res
        .status(400)
        .send({ message: "There is not magento platform registered" })
    : syncMagento(platform);

  function syncMagento(platform) {
    common.syncStockMagento(platform, cb);
  }

  function cb(error, result) {
    if (error) res.status(400).send({ message: error });
    else res.json(result);
  }
};
