process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
const Product = require("../model");

//get product by SKU
exports.getProduct = async (req, res) => {
  let ownerId = req.decoded._id;
  let skuParam = req.params.sku;
  const result = await Product.findByOwnerAndSku(ownerId, skuParam);

  return result.error ? res.status(400).send(result) : res.json(result);
};
