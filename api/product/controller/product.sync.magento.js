const common = require("../common");

//get products from magento and update product hobox
module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;
  const isFullSync = req.body.isFullSync ? req.body.isFullSync : false;

  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "magento", owner: ownerId });
  !platform
    ? res
        .status(400)
        .send({ message: "There is not magento platform registered" })
    : syncMagento(platform);

  async function syncMagento(platform) {
    common.syncProductsMagento(platform, isFullSync, cb);
  }

  function cb(error, result) {
    if (error) res.status(400).send({ message: error });
    else res.json(result);
  }
};
