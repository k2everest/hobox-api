const common = require("../common");

//sync products
module.exports = async function (db) {
  let marketplaces = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .toArrayAsync();
  marketplaces.forEach(main);

  async function main(platform) {
    await common.syncStockMagento(platform, cb);
  }

  function cb(err, res) {
    console.log("stock updated action " + err ? err : res);
  }
};
