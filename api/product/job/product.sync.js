const common = require("../common");

//sync products
module.exports = async function (db) {
  let marketplaces = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .toArrayAsync();
  marketplaces.forEach(main);

  async function main(platform) {
    await common.syncProductsMagento(platform, null, cb);
  }

  function cb(err, res) {
    console.log("product updated action " + err ? err : res);
  }
};
