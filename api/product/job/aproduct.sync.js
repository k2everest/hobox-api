var fs = require("fs");
const parser = require("../parser");
var Promise = require("bluebird");
let magento = require("../../../lib/magento");
Promise.promisifyAll(magento);
Object.keys(magento).forEach(function (key) {
  var value = magento[key];
  if (typeof value === "function") {
    Promise.promisifyAll(value);
    Promise.promisifyAll(value.prototype);
  }
});

//sync products
module.exports = async function (db) {
  let marketplaces = await db
    .collection("plataform")
    .find({ plataform: "magento" })
    .toArrayAsync();
  marketplaces.forEach(main);
  async function main(platform) {
    if (isAllowedReceiveProductsFromMagento(platform)) {
      let ownerId = platform.owner;
      magento(parser.getConfig(platform)).xmlrpc.auto.integrator.product.items(
        "default",
        null,
        null,
        function (err, result) {
          if (!err && result) updateProducts(result, ownerId);
        }
      );
    }
  }

  function isAllowedReceiveProductsFromMagento(platform) {
    return platform.syncConfig && platform.syncConfig.product.receive.value;
  }

  async function updateProducts(result, ownerId) {
    const cat = await db
      .collection("category")
      .findOneAsync({ owner: ownerId });
    const categories = cat.categories;
    console.log(categories);
    result.map(function (product) {
      let categoriesCurrent = product.attributes
        .filter((obj) => parser.getValue("categories", obj))
        .map((obj) => {
          return obj.value.replace(/\s/g, "").split(";");
        })[0];
      let filtered = [];
      if (categoriesCurrent) {
        filtered = categories.filter((f) => categoriesCurrent.includes(f.id));
      }
      const priceCurrent = parseFloat(
        product.attributes
          .filter((obj) => parser.getValue("price", obj))
          .map((obj) => {
            return obj.value;
          })[0]
      );

      product = {
        owner: ownerId,
        id: product.attributes.filter((obj) =>
          parser.getValue("product_id", obj)
        )[0].value,
        sku: product.attributes.filter((obj) => parser.getValue("sku", obj))[0]
          .value,
        status: product.attributes.filter((obj) =>
          parser.getValue("status", obj)
        )[0].value,
        name: product.attributes.filter((obj) =>
          parser.getValue("name", obj)
        )[0].value,
        description: product.attributes.filter((obj) =>
          parser.getValue("description", obj)
        )[0].value,
        weight: parseFloat(
          product.attributes
            .filter((obj) => parser.getValue("weight", obj))
            .map((obj) => {
              return obj.value;
            })[0]
        ),
        images: product.image_urls,
        categories: filtered,
        attributes: product.attributes.filter((obj) =>
          parser.getAttributes(obj)
        ),
        option_attributes: product.multiselect_attributes,
        price: !isNaN(priceCurrent) ? priceCurrent : 0,
      };

      //save product
      db.collection("product").update(
        { id: product.id, owner: ownerId },
        { $set: product },
        { upsert: true },
        function (err, result) {
          if (err)
            fs.createWriteStream("./logs/magento.log", { flags: "a" }).write(
              new Date() +
                " error on update/insert product from magento: " +
                product.id +
                "\n"
            );
          else {
            return;
          }
        }
      );
    });
  }
};
