const db = require("../../../service/db");
const agenda = require("../../../service/agenda");
const syncProductsMagento = require("./product.sync");
const syncStockMagento = require("./product.sync.stock");

//define agenda
agenda.define("syncProductsMagento", function (job, done) {
  syncProductsMagento(db);
  done(console.log("update products on hobox"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("15 minutes", "syncProductsMagento", {});
  agenda.start();
});

//define agenda
agenda.define("syncStockMagento", function (job, done) {
  syncStockMagento(db);
  done(console.log("update stock on hobox"));
});

///start jobs
agenda.on("ready", function () {
  agenda.every("15 minutes", "syncStockMagento", {});
  agenda.start();
});
