exports.getValue = function getValue(attribute, obj) {
  if (attribute == obj.key) return obj.value;
};

exports.getAttributes = function (obj) {
  if (
    obj.key != "status" &&
    obj.key != "price" &&
    obj.key != "name" &&
    obj.key != "sku" &&
    obj.key != "product_id" &&
    obj.key != "description" &&
    obj.key != "weight"
  )
    return obj;
};

exports.getVariationAttributes = (configurableAttributes) => {
  let result = [];
  if (Array.isArray(configurableAttributes)) {
    result = configurableAttributes.map((e) => e.attribute_code);
  }

  return result;
};

exports.mapVariationItems = (variations) => {
  let result = [];
  console.log("variations", variations);
  if (Array.isArray(variations)) {
    result = variations.map((e) => {
      return {
        id: e.associated_product_id,
        sku: e.associated_product_sku,
        weight: e.weight,
        price: e.price,
        images: e.image_urls,
        attributes: e.attributes,
      };
    });
  }

  return result;
};

exports.mapProduct = (product) => {
  const priceCurrent = parseFloat(
    product.attributes
      .filter((obj) => this.getValue("price", obj))
      .map((obj) => obj.value)[0]
  );

  return {
    id: product.attributes.filter((obj) => this.getValue("product_id", obj))[0]
      .value,
    sku: product.attributes.filter((obj) => this.getValue("sku", obj))[0].value,
    status: product.attributes.filter((obj) => this.getValue("status", obj))[0]
      .value,
    name: product.attributes.filter((obj) => this.getValue("name", obj))[0]
      .value,
    description: product.attributes.filter((obj) =>
      this.getValue("description", obj)
    )[0].value,
    weight: parseFloat(
      product.attributes
        .filter((obj) => this.getValue("weight", obj))
        .map((obj) => obj.value)[0]
    ),
    images: product.image_urls,
    categories: product.categories,
    attributes: product.attributes.filter((obj) => this.getAttributes(obj)),
    option_attributes: product.multiselect_attributes,
    price: !isNaN(priceCurrent) ? priceCurrent : 0,
    variation_attributes: this.getVariationAttributes(
      product.configurable_attributes
    ),
    variations: this.mapVariationItems(product.variations),
  };
};
