"use strict";
const jwt = require("jsonwebtoken"),
  bcrypt = require("bcrypt-nodejs");
const User = require("../user/userModel");
const smtpTransport = require("../../service/smtpTransport");
const mercadolibre = require("../../service/mercadolivre");
let email = process.env.MAILER_EMAIL_ID || "rogeres@k2everest.com.br";
const passport = require("passport");
const GoogleTokenStrategy = require("passport-google-token").Strategy;

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    },
    async function (accessToken, refreshToken, profile, done) {
      console.log("google-profile", profile);
      let user = await User.upsertGoogleUser(accessToken, profile);
      if (user.type && user.type == false) return done(user, null);
      else return done(null, user);
    }
  )
);

exports.sign_in = function (req, res) {
  var db = req.db;
  const key = process.env.JWT_SECRET;
  const currentEmail = req.body.credentials.email.trim();
  const currentPassword = req.body.credentials.password.trim();

  db.collection("user").findOne({ email: currentEmail }, function (err, user) {
    if (err) throw err;
    if (!user || !bcrypt.compareSync(currentPassword, user.password)) {
      return res
        .status(401)
        .json({ message: "Authentication failed. Invalid user or password." });
    }
    return res.json({
      username: user.name,
      role: user.role,
      token: jwt.sign(
        {
          email: user.email,
          fullName: user.name,
          _id: user._id,
          role: user.role,
        },
        key /* , { expiresIn: 24*60*60 } */
      ),
    });
  });
};

exports.authGoogle = passport.authenticate("google-token", { session: false });

exports.handleAuthGoogle = async (req, res) => {
  console.log("auth google");
  let user = req.user;
  if (!req.user) {
    return res.send(401, "User Not Authenticated");
  }
  const key = process.env.JWT_SECRET;
  if (user.isNew) sendConfirmationEmail(user);
  return res.json({
    username: user.name,
    token: jwt.sign(
      { email: user.email, fullName: user.name, _id: user._id },
      key /* , { expiresIn: 24*60*60 } */
    ),
  });

  function sendConfirmationEmail(user) {
    const data = {
      to: user.email,
      from: email,
      template: "welcome-email",
      subject: "Bem-vindo ao Hobox",
      context: {
        name: user.name,
      },
    };

    smtpTransport.sendMail(data, function (err) {
      if (!err) {
        console.log("enviou email");
      } else {
        console.log("erro ao enviar email", err);
      }
    });
  }
};

exports.authotizarionMercadoLivre = async function (req, res) {
  var db = req.db;
  const app = require("../../app");
  let code = req.query.code;
  const baseUrl = req.protocol + "://" + req.headers.host;
  const clientUrl = app.settings.clientUrl;
  var meliObject = await mercadolibre.meli(
    process.env.MERCADOLIVRE_CLIENT_ID,
    process.env.MERCADOLIVRE_CLIENT_SECRET
  );
  meliObject.authorize(
    code,
    baseUrl + "/login/plataforms/mercadolivre/authorization",
    function (err, result) {
      if (err) return res.send(err);
      else {
        meliObject.get("users/me", function (err, user) {
          if (err) res.send(err);
          else {
            db.collection("plataform").update(
              { _id: user.id },
              {
                $set: {
                  plataform: "mercadolivre",
                  "settings.seller": user.id,
                  "settings.email": user.email,
                  "settings.nickname": user.nickname,
                  "settings.accessToken": result.access_token,
                  "settings.refreshToken": result.refresh_token,
                },
              },
              { upsert: true },
              function (err, ml) {
                if (err) {
                  return res.status(400).send({
                    message: err,
                  });
                } else {
                  return res.redirect(
                    clientUrl +
                      "/mercadolivre/" +
                      user.id +
                      "/" +
                      user.email +
                      "/" +
                      user.nickname
                  );
                }
              }
            );
          }
        });
      }
    }
  );
};

exports.forgotPassword = async function (req, res) {
  let result = await User.getUserByEmail(req.body.email);
  const app = require("../../app");
  const clientUrl = app.settings.clientUrl;

  if (result && result.type == true) {
    User.setResetPasswordToken(result.user, cb);
  } else res.status(422).send("User not found.");

  function cb(err, user) {
    if (err || user == null) return res.status(422).json({ message: err });
    else sendEmail(user);
  }

  function sendEmail(user) {
    const data = {
      to: user.email,
      from: email,
      template: "forgot-password-email",
      subject: "Recuperar Senha",
      context: {
        url:
          clientUrl + "/user/reset-password?token=" + user.resetPasswordToken,
        name: user.name,
      },
    };

    smtpTransport.sendMail(data, function (err) {
      if (!err) {
        return res.json({
          message: "Kindly check your email for further instructions",
        });
      } else {
        return res.status(422).json({ message: err });
      }
    });
  }
};

/**
 * Reset password
 */
exports.resetPassword = async function (req, res) {
  if (req.body.newPassword != req.body.confirmPassword) {
    return res.status(422).send({
      message: "Passwords do not match",
    });
  } else {
    let result = await User.setNewPasswordReset(
      req.body.token,
      req.body.newPassword
    );
    if (result && result.type == true) {
      sendEmail(result.user);
    } else
      return res.status(400).json({
        message: "Password reset token is invalid or has expired.",
      });
  }

  function sendEmail(user) {
    const data = {
      to: user.email,
      from: email,
      template: "reset-password-email",
      subject: "Sua senha foi alterada",
      context: {
        name: user.name,
      },
    };

    smtpTransport.sendMail(data, function (err) {
      if (!err) {
        return res.json({ message: "Password reset" });
      } else {
        return res.status(422).json({ message: err });
      }
    });
  }
};
