var express = require("express");
var router = express.Router();
var authHandlers = require("./authController");

/**
 * @swagger
 *   /login/signIn:
 *      post:
 *          tags:
 *              - Authentication
 *          description:
 *              Allow users to log in, and to receive a Token
 *          parameters:
 *            -
 *              in: body
 *              name: user
 *              description: The email and password
 *              required: true
 *              schema:
 *                type: object
 *                properties:
 *                  credentials:
 *                    $ref: '#/definitions/Credentials'
 *          responses:
 *              '200':
 *               description: Login Success
 *               schema:
 *                $ref: '#/definitions/Token'
 *              '400':
 *               description:
 *                  Whether the user is not found or error while login
 *               schema:
 *                $ref: '#/definitions/Error'
 *              '401':
 *               description: >-
 *                If user is not found (bad credentials) OR if user can not login (a
 *                 concierge of an unsctive client)
 *               schema:
 *                $ref: '#/definitions/Error'
 * definitions:
 *   Credentials:
 *     name: user
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   Token:
 *     type: object
 *     properties:
 *      username:
 *       type: string
 *      token:
 *        type: string
 *   Error:
 *     type: object
 *     properties:
 *      message:
 *        type: string
 *      error:
 *        type: string
 *
 */

//signIn
router.post("/signIn", authHandlers.sign_in);

router.post("/google", [
  authHandlers.authGoogle,
  authHandlers.handleAuthGoogle,
]);

//forgot password recover
router.post("/forgotPassword", authHandlers.forgotPassword);

//reset password
router.post("/resetPassword", authHandlers.resetPassword);

router.get(
  "/plataforms/mercadolivre/authorization",
  authHandlers.authotizarionMercadoLivre
);
//router.get('/plataforms/mercadolivre/accesstoken',authHandlers.getMercadoLivreAccessToken);

module.exports = router;
