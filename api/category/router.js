const express = require("express");
const router = express.Router();
const handlers = require("./controller");

/**
 *@swagger
 * definitions:
 *
 *  Category:
 *   type: object
 *   properties:
 *      id:
 *          type: string
 *      name:
 *          type: string
 *
 **/

/**
 *@swagger
 * /categories:
 *   get:
 *     tags:
 *       - Categories
 *     description: Returns all categories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of categories
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Category'
 */

//get all categories from hobox
router.get("/", handlers.get);

/**
 *@swagger
 * /categories/magento/sync:
 *   post:
 *     tags:
 *       - Categories
 *     description: sync categories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of categories synced
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Category'
 */

//get all categories from magento plataform
router.post("/magento/sync", handlers.sync);

/**
 *@swagger
 * /categories/{id}:
 *   put:
 *     tags:
 *       - Categories
 *     description: Update a category by id
 *     parameters:
 *       - name: id
 *         description: category id
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *       - in: body
 *         name: category
 *         description: category name
 *         schema:
 *          type: object
 *          properties:
 *            name:
 *              type: string
 *
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Update a category by id
 *         schema:
 *           type: string
 */

//update category
router.put("/:id", handlers.update);

/**
 *@swagger
 * /categories/{id}:
 *   delete:
 *     tags:
 *       - Categories
 *     description: sync categories
 *     parameters:
 *       - name: id
 *         description: category id
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Remove a category by id
 *         schema:
 *           type: string
 *
 */
//remove category
router.delete("/:id", handlers.del);

module.exports = router;
