const magento = require("../../../service/magento");
const db = require("../../../service/db");
const logger = require("../../../service/winston");
const Category = require("../model");

module.exports = async (ownerId, cb = function (err, res) {}) => {
  categories = [];

  const getCategoriesFromMagento = async (platform) => {
    magento(platform).xmlrpc.auto.catalog.category.tree(1, "default", function (
      error,
      item
    ) {
      if (error) {
        return cb(error, null);
      } else {
        organizeCategory(item.children, item.name);
        updateCategories(platform.owner);
      }
    });
  };

  const organizeCategory = (result, parent) => {
    if (result.length > 0) {
      result.map(function (item) {
        if (item.children.length > 0) {
          organizeCategory(item.children, item.name);
        }
        categories.push({
          id: item.category_id,
          name: parent + ">" + item.name,
        });
      });
    }
  };

  const updateCategories = async (ownerId) => {
    let result = await Category.saveAll(ownerId, categories);
    if (result.error)
      logger.error({
        message: err,
        meta: {
          resource: "category",
          origin: "magento",
          destiny: "hobox",
          owner: ownerId,
          action: "sync categories",
        },
      });
    else return cb(null, result);
  };

  try {
    let platform = await db
      .collection("plataform")
      .findOneAsync({ plataform: "magento", owner: ownerId });
    if (platform._id) {
      getCategoriesFromMagento(platform);
    }
  } catch (err) {
    cb(err, null);
    throw err;
  }
};
