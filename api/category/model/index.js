const db = require("../../../service/db");
const category = db.collection("category");

exports.saveAll = async (ownerId, categories) => {
  try {
    await category.updateAsync(
      { owner: ownerId },
      { owner: ownerId, categories: categories },
      { upsert: true }
    );

    return categories;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.updateOne = async (ownerId, nameCategory, idCategory) => {
  try {
    let result = await category.findOneAndUpdateAsync(
      { owner: ownerId, "categories.id": idCategory },
      { $set: { "categories.$.name": nameCategory } },
      { returnOriginal: false }
    );

    return result.value;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.get = async (ownerId) => {
  try {
    let result = await category.findOneAsync({ owner: ownerId });
    return result ? result.categories : [];
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.del = async (ownerId, idCategory) => {
  try {
    let result = await category.updateAsync(
      { owner: ownerId },
      { $pull: { categories: { id: idCategory } } },
      { multi: false }
    );
    return "ok";
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};
