module.exports.del = require("./category.del");
module.exports.get = require("./category.get");
module.exports.sync = require("./category.sync");
module.exports.update = require("./category.update");
