const common = require("../common");

module.exports = async function (req, res) {
  const ownerId = req.decoded._id;
  common.sync(ownerId, cb);

  function cb(err, result) {
    if (err)
      return res.status(400).send({
        message: "dados da plataforma estão incorretos",
      });
    else if (result.length > 0)
      return res.json({
        message: "dados salvos com sucesso",
        categories: result,
      });
    else return res.json({ message: "Não há categorias" });
  }
};
