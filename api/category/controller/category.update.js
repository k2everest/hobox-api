const Category = require("../model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  const id = req.params.id;
  const name = req.body.name;

  let result = await Category.updateOne(ownerId, name, id);
  return result.error ? res.status(400).send(result) : res.json(result);
};
