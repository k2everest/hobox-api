const Category = require("../model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let id = req.params.id;

  let result = await Category.del(ownerId, id);
  return result.error ? res.status(400).send(result) : res.json(result);
};
