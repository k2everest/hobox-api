const common = require("../common");
const events = require("../../../service/event");

module.exports.run = function () {
  console.log("entrou no run");
  events.on("onAddNewMagentoPlatform", syncCategory);
};

function syncCategory(params) {
  console.log("o evento syncCategory disparou");
  common.sync(params.owner);
}
