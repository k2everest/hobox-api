const Attribute = require("../model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  const id = req.params.id;
  const name = req.body.name;

  let result = await Attribute.updateOne(ownerId, name, label);
  return result.error
    ? res.status(400).send({ message: result.message })
    : res.json(result);
};
