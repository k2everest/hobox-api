const Attribute = require("../model");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  let result = await Attribute.get(ownerId);
  if (result.error) return res.status(400).send({ message: result.message });
  else return res.json(result);
};
