const common = require("../common");

module.exports = async function (req, res) {
  const ownerId = req.decoded._id;
  common.sync(ownerId, cb);

  function cb(err, result) {
    if (err) res.status(400).send(err);
    else res.json(result);
  }
};
