const common = require("../common");
const events = require("../../../service/event");

module.exports.run = function () {
  console.log("entrou no run attribute");
  events.on("onAddNewMagentoPlatform", syncAttributes);
};

function syncAttributes(params) {
  console.log("o evento syncAttributes disparou");
  common.sync(params.owner);
}
