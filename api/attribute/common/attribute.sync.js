const logger = require("../../../service/winston");
const magento = require("../../../service/magento");
const db = require("../../../service/db");
const Attribute = require("../model");

module.exports = async function (ownerId, callback = function (err, res) {}) {
  let platform = await db
    .collection("plataform")
    .findOneAsync({ owner: ownerId, plataform: "magento" });

  magento(platform).xmlrpc.auto.integrator.product.listAllAttributes(
    async (err, result) => {
      if (err) callback({ message: "Problema ao conectar com Magento" }, null);
      else save(result);
    }
  );

  save = async (currentAttributes) => {
    let result = await Attribute.saveAll(ownerId, currentAttributes);
    if (result.error) {
      logger.error({
        message: result.message,
        meta: {
          resource: "attribute",
          origin: "magento",
          destiny: "hobox",
          owner: ownerId,
          action: "sync attributes",
        },
      });
    } else callback(null, "synced");
  };
};
