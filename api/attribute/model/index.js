const db = require("../../../service/db");
const ObjectId = require("mongodb").ObjectId;

exports.saveAll = async (ownerId, currentAttributes) => {
  try {
    const result = await db
      .collection("attribute")
      .updateAsync(
        { owner: ownerId },
        { $set: { attributes: currentAttributes } },
        { upsert: true }
      );
    return result;
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.get = async (ownerId) => {
  try {
    let result = await db
      .collection("attribute")
      .findOneAsync({ owner: ownerId });
    const attributeQty = {
      name: "qty",
      label: "Quantidade",
      options: null,
    };
    return result && result.attributes.push(attributeQty)
      ? result.attributes
      : [];
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.updateOne = async (ownerId, name, label) => {
  try {
    let result = await db
      .collection("attibute")
      .findOneAndUpdateAsync(
        { owner: ownerId, "attributes.name": name },
        { $set: { "attibute.$.label": label } },
        { returnOriginal: false }
      );
    return result.value;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};
