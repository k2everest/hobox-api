const express = require("express");
const router = express.Router();
const handlers = require("./controller");

/**
 *@swagger
 * definitions:
 *
 *  Attribute:
 *   type: object
 *   properties:
 *      label:
 *          type: string
 *      name:
 *          type: string
 *
 **/

/**
 *@swagger
 * /attributes/magento/sync:
 *   post:
 *     tags:
 *       - Attributes
 *     description: sync categories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of attributes synced
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Attribute'
 */

//get all attributes from magento plataform
router.post("/magento/sync", handlers.sync);

/**
 *@swagger
 * /attributes:
 *   get:
 *     tags:
 *       - Attributes
 *     description: List Attributes
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of attributes
 *         schema:
 *              type: array
 *              items:
 *                  $ref: '#/definitions/Attribute'
 */
//list all attributes
router.get("/", handlers.get);

module.exports = router;
