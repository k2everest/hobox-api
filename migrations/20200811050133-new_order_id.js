const { exist } = require("joi");

module.exports = {
  async up(db, client) {
    //return db.collection('orders').update({id:  {$exists:true}}, {$set: {id: id.toString()}});
    return db
      .collection("orders")
      .find({ id: { $exists: true } })
      .forEach(function (x) {
        db.collection("orders").updateMany(
          { _id: x._id },
          { $set: { id: x.id.toString(), packId: x.packId.toString() } }
        );
      });

    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: true}});
  },

  async down(db, client) {
    //return db.collection('orders').update({id:  {$exists:true}}, {$set: {id: id.toString()}});
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
