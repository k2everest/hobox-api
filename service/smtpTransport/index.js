const hbs = require("nodemailer-express-handlebars"),
  email = process.env.MAILER_EMAIL_ID || "rogeres19@gmail.com",
  pass = process.env.MAILER_PASSWORD || "123456",
  nodemailer = require("nodemailer");
path = require("path");

const smtpTransport = nodemailer.createTransport({
  host: "mail.k2everest.com.br",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: "rogeres@k2everest.com.br",
    pass: "rogeres007",
  },
  tls: { rejectUnauthorized: false },
});

var handlebarsOptions = {
  viewEngine: "handlebars",
  viewPath: path.resolve("./api/templates/"),
  extName: ".html",
};

smtpTransport.use("compile", hbs(handlebarsOptions));

module.exports = smtpTransport;
