var Promise = require("bluebird");
let magento = require("../../lib/magento");

Promise.promisifyAll(magento);
Object.keys(magento).forEach(function (key) {
  var value = magento[key];
  if (typeof value === "function") {
    Promise.promisifyAll(value);
    Promise.promisifyAll(value.prototype);
  }
});

const getConfig = function (platform) {
  let config = undefined;
  if (platform)
    config = {
      owner: platform.owner,
      host: platform.settings.url,
      port: 443,
      xmlrpc_path: platform.settings.url_api,
      login: platform.settings.username,
      pass: platform.settings.password,
    };
  else
    config = {
      host: "",
      port: 443,
      xmlrpc_path: "",
      login: "",
      pass: "",
    };

  return config;
};

const magentoConfigured = function (marketplace) {
  return magento(getConfig(marketplace));
};

module.exports = magentoConfigured;
