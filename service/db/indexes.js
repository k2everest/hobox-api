const db = require("./index");

module.exports.run = function () {
  console.log("indexando...");
  //db.collection('synclog').dropIndex( 'dateTime_-1' );
  db.collection("synclog").createIndex({ owner: "text", dateTime: -1, id: 1 });
  db.collection("orders").createIndex({ owner: "text", dateCreated: -1 });
  db.collection("product").createIndex({ owner: "text" });
  db.collection("plataform").createIndex({ owner: "text" });
  db.collection("user").createIndex({ owner: "hashed" });
  db.collection("category").createIndex({ owner: "text" });
  db.collection("attribute").createIndex({ owner: "text" });
};
