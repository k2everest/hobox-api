let mongo = require("mongoskin");
let Promise = require("bluebird");

const hostdb = process.env.HOST_DB ? process.env.HOST_DB : "localhost";

console.log("hostdb", hostdb);

//const USER = 'admin';
//const PWD = 'nomefacil';

Object.keys(mongo).forEach(function (key) {
  var value = mongo[key];
  if (typeof value === "function") {
    Promise.promisifyAll(value);
    Promise.promisifyAll(value.prototype);
  }
});
Promise.promisifyAll(mongo);

//const db = mongo.db("mongodb://"+ USER + ":" + PWD + "@" +"localhost:27017/api_hub", {safe:true, native_parser:true});
let db = mongo.db("mongodb://" + hostdb + ":27017/api_hub", {
  native_parser: true,
});

module.exports = db;
