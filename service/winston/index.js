const db = require("../db");
const winston = require("winston");
require("winston-mongodb").MongoDB;

const tsFormat = () =>
  new Date().toLocaleDateString() + " - " + new Date().toLocaleTimeString();
const hostdb = process.env.HOST_DB ? process.env.HOST_DB : "localhost";

console.log("hostwinston", hostdb);

const logger = winston.createLogger({
  level: "info",

  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.MongoDB({
      db: "mongodb://" + hostdb + ":27017/api_hub",
    }),
    new winston.transports.File({ filename: "logs/error.log", level: "error" }),
    new winston.transports.File({ filename: "logs/info.log", level: "info" }),
    new winston.transports.File({ filename: "logs/warn.log", level: "warn" }),
    new winston.transports.File({ filename: "logs/combined.log" }),
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
/* if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
} */

module.exports = logger;
