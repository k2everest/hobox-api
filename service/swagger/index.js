var swaggerJSDoc = require("swagger-jsdoc");

const protocol = process.env.PROTOCOL ? process.env.PROTOCOL : "https";
let host = process.env.HOST;

let swaggerDefinition = require("./config.json");

//set env values for swagger
swaggerDefinition["schemes"] = [protocol];
swaggerDefinition["host"] = host;

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  //apis: ['api/*/*.js'],
  apis: ["./service/swagger/docs/*.yaml"],
};

// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);
// view engine setup

module.exports = swaggerSpec;
