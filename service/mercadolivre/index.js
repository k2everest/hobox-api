const meli = require("mercadolibre-new");
let Promise = require("bluebird");

async function getMeli(accessToken = null, refreshToken = null) {
  let meliObject = new meli.Meli(
    process.env.MERCADOLIVRE_CLIENT_ID,
    process.env.MERCADOLIVRE_CLIENT_SECRET,
    accessToken,
    refreshToken
  );
  Promise.promisifyAll(meliObject);
  return meliObject;
}
module.exports.meli = getMeli;
