const Agenda = require("agenda");
const hostdb = process.env.HOST_DB ? process.env.HOST_DB : "localhost";

//agenda
const mongoConnectionString = "mongodb://" + hostdb + ":27017/agenda";
const agenda = new Agenda({ db: { address: mongoConnectionString } });

module.exports = agenda;
