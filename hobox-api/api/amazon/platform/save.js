const Joi = require("joi");
const syncConfig = require("./config/syncConfig");
const common = require("../../platform/common");

module.exports = async function (req, res) {
  const db = req.db;
  const ownerId = req.decoded._id;

  const schema = Joi.object().keys({
    sellerId: Joi.string().min(3).required(),
    accessKeyId: Joi.string().min(3).required(),
    secretKey: Joi.string().min(3).required(),
  });

  const platform = {
    owner: ownerId,
    platform: req.body.platform.platform,
    settings: req.body.platform.settings,
    mappedAttributes: [
      {
        marketplaceName: "teste",
        hoboxName: "teste",
        options: null,
      },
    ],
  };

  function validate(settings) {
    const result = Joi.validate(settings, schema);
    return result.error;
  }

  function setSyncConfigDefault(_idPlatform) {
    common.setSyncConfig(_idPlatform, syncConfig);
  }

  if (validate(platform.settings) == null) {
    try {
      let result = await db
        .collection("plataform")
        .updateOneAsync(
          { owner: ownerId, plataform: "amazon" },
          { $set: platform },
          { upsert: true }
        );
      if (result.result.upserted) {
        setSyncConfigDefault(result.result.upserted[0]._id);
      }
      return res.json({ message: "dados salvos" });
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  } else {
    res.status(400).json({
      status: "fail",
      message: "dados incompletos",
    });
  }
};
