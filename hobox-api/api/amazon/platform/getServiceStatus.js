"use strict";

//var accessKey = process.env.AWS_ACCESS_KEY_ID || 'AKIAI27AKUF6ATNZDRCA';
//var accessSecret = process.env.AWS_SECRET_ACCESS_KEY || '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';

var amazonMws = require("amazon-mws");
const updateStatus = require("./updateStatus");
//var fse = require('fs-extra');

module.exports = async (req, res) => {
  const db = req.db;
  const ownerId = req.decoded._id;
  let platform = await db
    .collection("plataform")
    .findOneAsync({ plataform: "amazon", owner: ownerId });
  const amazonObject = amazonMws(
    platform.settings.accessKeyId,
    platform.settings.secretKey
  );
  amazonObject.feeds.search(
    {
      Version: "2009-01-01",
      Action: "GetFeedSubmissionList",
      SellerId: platform.settings.sellerId,
      MWSAuthToken: "amzn.mws.d8af40d1-e5f2-8de7-2e86-cd52482685f4",
    },
    function (error, response) {
      if (error) {
        updateStatus(ownerId, false);
        return res.status(400).send(error);
      }
      updateStatus(ownerId, true);
      return res.json(response);
    }
  );
};
