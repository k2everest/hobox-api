const db = require("../../../service/db");

module.exports = async (ownerId, statusConn) => {
  let platform = await db
    .collection("plataform")
    .updateAsync(
      { owner: ownerId, plataform: "amazon" },
      { $set: { connected: statusConn } },
      { upsert: false }
    );
};
