var fs = require("fs");
const xml2js = require("xml2js");
var parser = new xml2js.Parser();
let Promise = require("bluebird");

let amazonBaseFileContent = null;

Promise.promisifyAll(fs);
Promise.promisifyAll(parser);

const getListAttributes = (jsonContent) => {
  let baseNode =
    jsonContent["xsd:schema"]["xsd:element"][0]["xsd:complexType"][0][
      "xsd:sequence"
    ][0]["xsd:element"];
  return baseNode;
};

const isSimpleTypeAttribute = (attribute) => {
  let basePathSimpleType = attribute["xsd:simpleType"];
  if (basePathSimpleType && basePathSimpleType[0]["xsd:restriction"][0])
    return attribute;
};

const isComplexTypeAttribute = (attribute) => {
  let basePathType = attribute["xsd:complexType"];
  if (basePathType && basePathType[0]["xsd:sequence"]) return attribute;
};

const isNativeTypeAttribute = (attribute) => {
  let basePathNativeType = attribute["$"]["type"];
  if (
    basePathNativeType &&
    (basePathNativeType.includes("xsd:") ||
      basePathNativeType.includes("String"))
  )
    return attribute;
};

const isBaseTypeAttributes = (attribute) => {
  let basePathNativeType = attribute["$"]["type"];
  if (basePathNativeType && !basePathNativeType.includes("xsd:"))
    return attribute;
};

const isRefAttribute = (attribute) => {
  let pathType = attribute["$"]["ref"];
  console.log(pathType);
  if (pathType) return attribute;
};

const mapOptions = (enumeration) => {
  return {
    label: enumeration["$"]["value"],
    value: enumeration["$"]["value"],
  };
};

const parseComplexTypeToArray = (att) => {
  let basePath = att["xsd:complexType"][0]["xsd:sequence"][0]["xsd:element"];
  let complexAttribute = basePath;
  let parsedAttributes = complexAttribute.map(handleAttributes);
  parsedAttributes = parsedAttributes.map((a) => {
    return a && a.name
      ? {
          name: att["$"]["name"] + "_" + a.name,
          label: a.name,
          options: a.options,
        }
      : a;
  });

  let result = [];
  parsedAttributes.map((a) =>
    Array.isArray(a) ? (result = result.concat(a)) : result.push(a)
  );

  return result;
};

const parseSimpleTypeEnumerationToCommon = (attribute) => {
  let attributeName = attribute["$"]["name"];
  let basePathSimpleType = attribute["xsd:simpleType"][0]["xsd:restriction"][0];
  let options = null;
  if (basePathSimpleType["xsd:enumeration"])
    options = basePathSimpleType["xsd:enumeration"].map(mapOptions);

  return {
    name: attributeName,
    label: attributeName,
    options: options,
  };
};

const parseRefTypeAttributeToCommon = (attribute) => {
  let elementBaseType = amazonBaseFileContent["xsd:schema"]["xsd:element"];
  if (
    !!elementBaseType.find((elem) => elem["$"]["name"] == attribute["$"]["ref"])
  )
    return handleAttributes(
      elementBaseType.find((elem) => elem["$"]["name"] == attribute["$"]["ref"])
    );
};

const parseBaseTypeAttributeToCommon = (attribute) => {
  let options = null;
  let elementBaseType = amazonBaseFileContent["xsd:schema"]["xsd:element"];
  let attributeType = amazonBaseFileContent["xsd:schema"][
    "xsd:simpleType"
  ].find((elem) => elem["$"]["name"] == attribute["$"]["type"]);
  let attributeTypeComplex = amazonBaseFileContent["xsd:schema"][
    "xsd:complexType"
  ].find((elem) => elem["$"]["name"] == attribute["$"]["type"]);
  if (attributeType) {
    if (attributeType["xsd:restriction"][0]["xsd:enumeration"])
      options = attributeType["xsd:restriction"][0]["xsd:enumeration"].map(
        mapOptions
      );

    return {
      name: attribute["$"]["name"],
      label: attribute["$"]["name"],
      options: options,
    };
  } else if (attributeTypeComplex) {
    if (attributeTypeComplex["xsd:sequence"]) {
      let temp = attributeTypeComplex["xsd:sequence"][0]["xsd:element"];
      return temp.map((t) => {
        return t["$"]["name"]
          ? {
              name: attribute["$"]["name"] + "_" + t["$"]["name"],
              label: attribute["$"]["name"] + "_" + t["$"]["name"],
              options: null,
            }
          : {
              name:
                attribute["$"]["name"] +
                "_" +
                handleAttributes(
                  elementBaseType.find(
                    (elem) => elem["$"]["name"] == t["$"]["ref"]
                  )
                ).name,
              label:
                attribute["$"]["name"] +
                "_" +
                handleAttributes(
                  elementBaseType.find(
                    (elem) => elem["$"]["name"] == t["$"]["ref"]
                  )
                ).label,
              options: handleAttributes(
                elementBaseType.find(
                  (elem) => elem["$"]["name"] == t["$"]["ref"]
                )
              ).options,
            };
      });
    }
    return {
      name: attribute["$"]["name"],
      label: attribute["$"]["name"],
      options: options,
    };
  } else return attribute;
};

const parseNativeTypeAttributesToCommon = (attribute) => {
  let attributeName = attribute["$"]["name"];
  return {
    name: attributeName,
    label: attributeName,
    options: null,
  };
};

const handleAttributes = (attribute) => {
  if (isNativeTypeAttribute(attribute))
    return parseNativeTypeAttributesToCommon(attribute);
  if (isSimpleTypeAttribute(attribute))
    return parseSimpleTypeEnumerationToCommon(attribute);
  if (isComplexTypeAttribute(attribute))
    return parseComplexTypeToArray(attribute);
  if (isBaseTypeAttributes(attribute))
    return parseBaseTypeAttributeToCommon(attribute);
  if (isRefAttribute(attribute))
    return parseRefTypeAttributeToCommon(attribute);
};

const readFileAsync = async (pathFile) => {
  let fileContent = await fs.readFileAsync(pathFile);
  let jsonContent = await parser.parseStringAsync(fileContent);
  return jsonContent;
};

const adjustAttributes = (attribute) => {
  const nameAttributesRemoved = [
    "RelatedProductID_Type",
    "StandardProductID_Type",
    "RelatedProductID_Value",
  ];
  if (attribute && attribute.name == "StandardProductID_Value")
    return {
      name: attribute.name,
      label: "EAN",
      options: null,
    };
  else if (attribute && !nameAttributesRemoved.includes(attribute.name))
    return attribute;
};

module.exports = async (req, res) => {
  const amazonBaseFilePath = "./api/amazon/data/Core/amzn-base.xsd";
  amazonBaseFileContent = await readFileAsync(amazonBaseFilePath);
  const productFilePath = "./api/amazon/data/Product/Product.xsd";
  let jsonContent = await readFileAsync(productFilePath);
  let attributes = getListAttributes(jsonContent);
  let parsedAttributes = await attributes.map(handleAttributes);
  let handledAttributes = [];
  parsedAttributes.map((a) =>
    Array.isArray(a)
      ? (handledAttributes = handledAttributes.concat(a))
      : handledAttributes.push(a)
  );
  handledAttributes = handledAttributes.map(adjustAttributes);
  console.log("teste", handledAttributes);
  return res.json(handledAttributes.filter((a) => a != null));

  return res.json(
    handledAttributes
      .filter((a) => a != null)
      .sort((a, b) => (a.name > b.name ? 1 : -1))
  );
};
