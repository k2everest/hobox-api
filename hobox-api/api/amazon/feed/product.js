const parser = require("./parser/product");
const Product = require("../../product/model");
const Platform = require("../../platform/model");
var amazonMws = require("../../../service/amazon").mws;
var fse = require("fs-extra");

module.exports = async (req, res) => {
  const ownerId = req.decoded._id;
  const selectedProductsSkus = ["252", "125", "119"];
  let platform = await Platform.get(ownerId, "amazon");
  if (!platform) return res.send("platform is not exist");
  const mappedCategories = platform.mappedCategories;
  const mappedAttributes = platform.mappedAttributes;
  let productsForSend = await Product.findByOwnerAndSkus(
    ownerId,
    selectedProductsSkus
  );
  const productsParsed = parser.itemToAmazon(
    productsForSend,
    mappedAttributes,
    mappedCategories
  );
  fse.writeFileSync("api/amazon/feed/sample.txt", productsParsed, "UTF-8");

  console.log(productsParsed);
  //return res.send(productsParsed);
  amazonMws.feeds.submit(
    {
      Version: "2009-01-01",
      Action: "SubmitFeed",
      FeedType: "_POST_PRODUCT_DATA_",
      FeedContent: productsParsed,
      SellerId: "A1OJQ1C2S6V5S4",
      MWSAuthToken: "amzn.mws.d8af40d1-e5f2-8de7-2e86-cd52482685f4",
    },
    function (error, response) {
      if (error) {
        return res.json(error);
      }
      return res.json(response);
    }
  );
};
