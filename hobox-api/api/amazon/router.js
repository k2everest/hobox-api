var express = require("express");
var router = express.Router();
const orderHandlers = require("./order/listOrders");

//FEED
const feedHandlers = require("./feed/submitFeed");
const feedTest = require("./feed/product");
const getFeedList = require("./feed/getFeedSubmissionList");
const getFeedResult = require("./feed/getFeedSubmissionResult");
const getReports = require("./reports/requestReport");
const getReportsId = require("./reports/getReport");
const getReportList = require("./reports/getReportList");

const getProduct = require("./product/getMatchingProduct");
const getCategories = require("./category/getCategories");
const platformSave = require("./platform/save");
const platformStatus = require("./platform/getServiceStatus");
const getAttributes = require("./attribute/getAttributes");

router.post("/platform", platformSave);
router.get("/platform/status", platformStatus);
router.post("/", orderHandlers);
router.post("/feed", feedHandlers);
router.post("/feed/test", feedTest);
router.get("/feed/:id", getFeedResult);
router.get("/feed", getFeedList);
router.get("/reports", getReports);
router.get("/reports/list", getReportList);
router.get("/reports/:id", getReportsId);
router.get("/product", getProduct);
router.get("/categories", getCategories);
router.get("/attributes", getAttributes);

module.exports = router;
