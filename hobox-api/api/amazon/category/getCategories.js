const amazonMws = require("../../../service/amazon");
const superagent = require("superagent");
var fs = require("fs");
const xml2js = require("xml2js");
var parser = new xml2js.Parser();
var util = require("util");
//var parser = require('xml2json');
let Promise = require("bluebird");

Promise.promisifyAll(fs);
Promise.promisifyAll(parser);

const ACCESS_ID = amazonMws.keys.AWS_ACCESS_KEY_ID;
const SECRET_KEY = amazonMws.keys.AWS_SECRET_ACCESS_KEY;

const CHILD_CAT_URL = () =>
  "https://browsenodes.com/amazon.com/browseNodeLookup/11260433011.json?awsId=" +
  ACCESS_ID +
  "&awsSecret=" +
  SECRET_KEY;

const url = () =>
  "https://browsenodes.com/amazon.com/explore.json?awsId=" +
  ACCESS_ID +
  "&awsSecret=" +
  SECRET_KEY;

const teste = "https://www.browsenodes.com/amazon.com/explore.json";

function clearCategories(categoryFile) {
  return categoryFile.replace(".xsd", "");
}

module.exports = async (req, res) => {
  var path = "./api/amazon/data/Category";

  function clear(categoryArray) {
    return categoryArray.map((a) =>
      a.$.ref ? a.$.ref : a.$.name ? a.$.name : a.$.value
    );
  }

  function formatMiscType(categoryArray) {
    return categoryArray.map((a) => a.$.value);
  }

  fs.readdir(path, async function (err, items) {
    //console.log(items);
    let objectComplete = [];
    for (var i = 0; i < items.length; i++) {
      let rootCategory = clearCategories(items[i]);

      //console.log(clearCategories(items[i]));
      let options = [];
      let data = await fs.readFileAsync(path + "/" + items[i]);
      let result = await parser.parseStringAsync(data);
      let categoryBase = result["xsd:schema"];
      let initialPath =
        categoryBase["xsd:element"][0]["xsd:complexType"][0]["xsd:sequence"][0][
          "xsd:element"
        ];
      let category = initialPath[0];

      for (let i = 0; i < initialPath.length; i++) {
        if (initialPath[i]["$"]["name"] == "ProductType") {
          category = initialPath[i];
          console.log("entrou", i);
          break;
        }
      }

      let isProductType = category["$"]["name"] == "ProductType";
      let isMiscType = category["$"]["type"] == "MiscType";

      if (isProductType == true) {
        if (!isMiscType)
          options = category["xsd:complexType"]
            ? category["xsd:complexType"][0]["xsd:choice"][0]["xsd:element"]
            : category["xsd:simpleType"][0]["xsd:restriction"][0][
                "xsd:enumeration"
              ];
        else
          options =
            categoryBase["xsd:simpleType"][0]["xsd:restriction"][0][
              "xsd:enumeration"
            ];
      }
      console.log(util.inspect(options));

      /*  fs.readFile(path + '/'+items[i], function(err, data) {
           parser.parseString(data, function (err, result) {
             let category = result['xsd:schema']['xsd:element'][0]['xsd:complexType'][0]['xsd:sequence'][0]['xsd:element'][0];
             let isProductType = category['$']['name']=='ProductType';
             let isMiscType = category['$']['type']=='MiscType';

             if(isProductType == true && !isMiscType){
              options = category['xsd:complexType']? category['xsd:complexType'][0]['xsd:choice'][0]['xsd:element'] : category['xsd:simpleType'][0]['xsd:restriction'][0]['xsd:enumeration'];
              console.log(clear(options));
             }
             console.log('produt',isProductType);
            console.log(util.inspect(result['xsd:schema']['xsd:element'][0]['xsd:complexType'][0]['xsd:sequence'][0]['xsd:element'][0], false, null));
            console.log('Done');
          });
        
        }); */
      //let subcategories = clear(options);
      let subcategories =
        isProductType == true && isMiscType
          ? formatMiscType(options)
          : clear(options);

      objectComplete.push({ text: rootCategory, children: subcategories });
    }
    return res.json(objectComplete);
  });

  /*     console.log(CHILD_CAT_URL());
    try {
        const result = await superagent
          .get(CHILD_CAT_URL());
        res.json(JSON.parse(result.text));
        } catch(err) {
        console.log(err);
      } */
};
