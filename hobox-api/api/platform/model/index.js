const Joi = require("joi");
const ObjectId = require("mongodb").ObjectId;
const db = require("../../../service/db");
const Platform = db.collection("plataform");

exports.get = async (ownerId, platforName) => {
  try {
    let result = await Platform.findOneAsync({
      owner: ownerId,
      plataform: platforName,
    });
    return result;
  } catch (err) {
    return {
      error: true,
      message: err.message,
    };
  }
};

exports.findAll = async (ownerId, filter, sortBy, limit, page) => {
  try {
    const count = await db
      .collection("orders")
      .find({ owner: ownerId, $and: filter })
      .countAsync();
    const tpages = Math.ceil(count / limit);
    const orders = await db
      .collection("orders")
      .find({ owner: ownerId, $and: filter })
      .sort(sortBy)
      .limit(limit)
      .skip(page)
      .toArrayAsync();
    return { orders: orders, total: count, pages: tpages };
  } catch (err) {
    return { error: true, message: err.message };
  }
};

exports.findNotSyncedMagento = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      $or: [
        { sync: { $exists: false } },
        { sync: "error" },
        { sync: "pending" },
      ],
    })
    .toArrayAsync();

  return orders;
};

exports.updateSyncStatus = async (
  orderId,
  currentSync,
  hoboxCode,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId },
    {
      $set: { sync: currentSync, code: hoboxCode },
      $push: {
        logs: {
          dateCreated: new Date(),
          from: "hobox",
          to: "magento",
          message: currentMessage,
        },
      },
    }
  );
};

exports.updateInvoiceStatus = async (
  orderId,
  currentInvoice,
  currentMessage
) => {
  db.collection("orders").updateAsync(
    { _id: orderId },
    {
      $set: { invoiced: currentInvoice },
      $push: {
        logs: {
          dateCreated: new Date(),
          from: "hobox",
          to: "magento",
          message: currentMessage,
        },
      },
    }
  );
};

exports.findForInvoice = async (ownerId) => {
  const orders = await db
    .collection("orders")
    .find({
      owner: ownerId,
      "status.value": "processing",
      sync: "ok",
      $or: [{ invoiced: false }, { invoiced: { $exists: false } }],
    })
    .toArrayAsync();
  return orders;
};

exports.updateOrder = (ownerId, order, cb) => {
  db.collection("orders").update(
    { id: order.id, owner: ownerId },
    { $set: order, $setOnInsert: { sync: "pending" } },
    { upsert: true },
    cb
  );
};
