exports.hoboxStatus = [
  {
    value: "pending_payment",
    label: "Pagamento pendente",
  },
  {
    value: "payment_review",
    label: "Pagamento em análise",
  },
  {
    value: "processing",
    label: "Processando",
  },
  {
    value: "canceled",
    label: "Cancelado",
  },
  {
    value: "complete",
    label: "Completo",
  },
];
