//check token
var jwt = require("jsonwebtoken");

const checkAuth = function (app) {
  app.use(function (req, res, next) {
    let token =
      req.body.token ||
      req.query.token ||
      req.headers["x-access-token"] ||
      req.headers["token"];
    console.log(req.headers);
    if (token) {
      return jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
          return res.status(401).json({
            success: false,
            message: "Falha ao tentar autenticar o token!",
          });
        } else {
          //se tudo correr bem, salve a requisição para o uso em outras rotas
          req.decoded = decoded;
          console.log("aqui - ", req.decoded);
          return next();
        }
      });
    } else {
      // se não tiver o token, retornar a mensagem
      return res.json({ success: false, message: "Não há token!" });
    }
  });
};

module.exports = checkAuth;
