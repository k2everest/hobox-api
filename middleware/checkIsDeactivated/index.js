"use strict";
const User = require("../../api/user/userModel");

const checkIsDeactivate = async function (app) {
  console.log("teste especifico");
  app.use(async function (req, res, next) {
    const _id = req.decoded._id;
    let result = await User.checkIsDeleted(_id);
    console.log(result);
    if (result.status == true)
      return res.status(405).send({ success: false, message: result.data });
    else if (result.type && result.type == false)
      return res.status(500).send({ success: false, message: result.data });
    else return next();
  });
};

module.exports = checkIsDeactivate;
